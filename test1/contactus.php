<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
                        }
                        .column-1
                        {
	                        margin-top:-20px;
                        }
					</style>
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Contact </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 8359825066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
            <div >
                <img class=column-1 src="images/contactus.jpg" alt="" height="290px" width="1050px" title="">
            <div>  
                          
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
            
			<div class="column two-third">

              
                <h3> Send us a message </h3>
                <div class="message"></div>
                <form action="php/sendmail.php" method="get" id="contact-form">
                    <p class="column one-half">
                        <input name="name" type="text" placeholder="Your Name" required>
                    </p>
                    
                    <p class="column one-half last">
                        <input name="email" type="email" placeholder="Your Email" required>
                    </p>
                    
                    <p class="clear">
                        <textarea name="comment" placeholder="Your Message" cols="5" rows="3" required></textarea>
                    </p>
                    
                    <p>
                        <input name="submit" type="submit" value="Comment">
                    </p>            
                </form>
            </div>  
            
            <div class="column one-third last">
            	<h3> Contact Details </h3>
                <div class="contact-details">
                	<p> D/61  Chouhan Town Bhilai, Chhattisgarh, India </p>
                    <p> <span class="icon-phone"> </span> <strong>Phone</strong> : (+91) 8359825066 </p>
                    <p> <span class="icon-envelope-alt"> </span> <strong>Email</strong> : <a href="mailto:info@divyaltech.com"> info@divyaltech.com </a> </p>
                    <p> <span class="icon-globe"> </span> <strong>Website</strong> : <a href="" title=""> www.divyaltech.com </a> </p> 
                    <h4> Working Hours </h4>
                    <p class="working-hours"> <span class="icon-time"> </span> <strong>Mon - Fri : 10am - 7pm</strong> <br> 
                    <strong>&amp; Sat : 10am - 3pm  </strong> </p>
                </div>
            </div>
			<div class="clear"> </div>
            <div class="hr-invisible"> </div> 
			<div class="column two-third">
            	<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
				<div style='overflow:hidden;height:400px;width:520px;'>
					<div id='gmap_canvas' style='height:400px;width:520px;'></div>
					
				</div> <a href='https://embedmap.org/'>maps generator</a>
				<script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=bdc1f62481dfd0c35b66820a0fa69755541ce147'></script>
				<script type='text/javascript'>
					function init_map() {
						var myOptions = {
							zoom: 14,
							center: new google.maps.LatLng(21.2134099, 81.3080281),
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
						marker = new google.maps.Marker({
							map: map,
							position: new google.maps.LatLng(21.2134099, 81.3080281)
						});
						infowindow = new google.maps.InfoWindow({
							content: '<strong>Divyal Technology</strong><br>D/61 Chouhan Town Bhilai, C.G.,India'
						});
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.open(map, marker);
						});
						infowindow.open(map, marker);
					}
					google.maps.event.addDomListener(window, 'load', init_map);
				</script>
            </div>  
              
        
        	<?php /*?><div class="column two-third">
            	<div id="map"> </div>
            </div>  
            
            <div class="column one-third last">
            	<h3> Contact Details </h3>
                <div class="contact-details">
                	<p> Aditya Nagar, Durg, Chhattisgarh, India </p>
                    <p> <span class="icon-phone"> </span> <strong>Phone</strong> : (+91) 788-2225066 </p>
                    <p> <span class="icon-envelope-alt"> </span> <strong>Email</strong> : <a href="mailto:info@divyaltech.com"> info@divyaltech.com </a> </p>
                    <p> <span class="icon-globe"> </span> <strong>Website</strong> : <a href="" title=""> www.divyaltech.com </a> </p> 
                    <h4> Working Hours </h4>
                    <p class="working-hours"> <span class="icon-time"> </span> <strong>Mon - Fri : 10am - 7pm</strong> <br> 
                    <strong>&amp; Sat : 10am - 3pm  </strong> </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div> 
            
            <h3> Send us a message </h3>
            <div class="message"></div>
            <form action="php/sendmail.php" method="get" id="contact-form">
                <p class="column one-half">
                    <input name="name" type="text" placeholder="Your Name" required>
                </p>
                
                <p class="column one-half last">
                    <input name="email" type="email" placeholder="Your Email" required>
                </p>
                
                <p class="clear">
                    <textarea name="comment" placeholder="Your Message" cols="5" rows="3" required></textarea>
                </p>
                
                <p>
                    <input name="submit" type="submit" value="Comment">
                </p>            
            </form>
        <?php */?>
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>
<script src="js/clickme.js"></script>
</body>
</html>
