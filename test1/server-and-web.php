<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1>Server and website support</h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong>Server and website support</strong></span></h6>
                   <p>Divyaltech give server observing services. Your servers will be observed by our Engineers 24x7. Our 
                     checking framework involves both operator and specialist less bundles. You will get email alarms or reboot 
                     demands from our administrators amid your server having downtime issues. On the off chance that you have a 
                     Proactive server administration arrangement with observing , then our techs will naturally deal with your 
                     administration downtime issues. You will get the month to month or week after week reports upon 
                     solicitation. Planning a proactive methodology gives our customers the points of interest by protection the 
                     sites and servers to stay online by attempting to address issues before they debilitate the security of the 
                     customer's facilitating surroundings.
                  </p>
              
            </div>
            <br><br>
           <div class="da-cantent"> 
           <h6><span style="font-size: 27px;color: #464646;">
               <strong> Server Management</strong></span></h6>
               <p>Server Management includes 24x7 checking of your server, administration of OS, outsider programming projects 
                  and Security fixing. Best Server Support team will screen your servers 24x7, as to which all administrations 
                  are running in the server furthermore take are of outsider programming establishments.
               </p>
               <p>At Best Server Support, we see how significant your Time and Money are. Those two things are at the very 
               center of our business. We realize that you have a site to run, a business to run, and an existence to live. That 
               is the reason Best Server Support offers Full Server Management, to make running your server a smooth affair. 
               We're here to ensure everything works immaculately.
               </p>
               
             <h6><span style="font-size: 27px;color: #464646;">
               <strong> Support Services</strong></span></h6>
               <ul>
				<li>Editing, revising, or updating page and product content including text, images, and SEO-related content</li>
				<li>New web page and email templates based on existing design</li>
				<li>Editing, revising, or updating templates on existing website pages</li>
				<li>Modifications and minor enhancements to existing functions, forms, and features on the existing website</li>
				<li>Google Analytics and Webmaster Tools basic configuration and support</li>
			 </ul>   
          <!-- MENU-LOCATION=NONE --><br><br>
           <h6><span style=" font-size: 27px;color: #464646;">Website Updates: </span></h6>
           <h6><span style=" font-size: 20px;color: #464646;">Keep your website's content up-to-date</span></h6>
               <p>Your site is an extraordinary instrument for keeping clients educated about the most recent changes in your 
                  business, so it's essential to ensure what guests see on your site is present and right.
               </p>
              <h6><span style=" font-size: 20px;color: #464646;">Update text content on your website</span></h6>
               <p>Content substance on your site should be accurate,free of spelling or syntactic mistakes and mirror the 
                 present conditions identifying with your items, administrations and your business general. On the off chance 
                 that the content substance on your site is outdated, you ought to take some an opportunity to assemble new 
                 substance.We can help you upgrade site content.
               </p> 
               
              <h6><span style=" font-size: 20px;color: #464646;">Update images & videos on your website</span></h6>
               <p>Pictures and videos can be extraordinary for advertising items and services to your site's guests, so you 
                 should guarantee that the pictures that show up on your site are present and applicable. In the event that 
                 picture content on your site is outdated, you should take some time to review your website's images. We can help                 you update website pictures.
               </p>                  
            </div>
           <br>
           <br>
<!--           <div class="service">
           <h6><span style=" font-size: 27px;color: #464646;">Desktop Development Services Provided by Us: </span></h6>
             <ul>
                 <li>Desktop software management</li>
                 <li>Desktop to web software migration</li>
                 <li>Desktop application developers</li>
                 <li>Desktop and web application integration</li>
                 <li>Software services outsourcing</li>
            </ul>
           
           </div>-->
      </div> 
          <div class="rightside">
           <?php include 'technology.php';?>
             <div class="services" style="margin-top:0px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Our technical support is useful to you:</span> </h6>
                  <ul> 
                    <li>Installation of new software and/or software upgrading</li>
                    <li>Issue related to Control Panel</li>
                  	<li>Security checking and ensuring the enhancement in security level as needed</li>
					<li>Remotely monitoring your server</li>
                    <li>Hardware testing</li>
                    <li>To run stress tests</li>
		         </ul> 
               </div>
               
             <div class="services" style="margin-top:10px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Website Maintenance Services:</span> </h6>
                 <p>Below are some common website maintenance services that could enhance your website.</p>
                  <ul> 
                    <li>Facebook Like Button</li>
                    <li>Facebook Social Plugins</li>
                    <li>Update WordPress Website</li>
                  	<li>WordPress conversions</li>
                    <li>Articles</li>
                  	<li>Blogs</li>
					<li>Online Appointments</li>
					<li>Online Bookings</li>
					<li>Online payments</li>
                  	<li>Google Analytics</li>
					<li>Live Chat</li>
                    <li>Payment gateways</li>
                  	<li>PayPal</li>
					<li>News</li>                 
					<li>Maps</li>
		         </ul> 
               </div>
               
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
