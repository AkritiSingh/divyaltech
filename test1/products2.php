<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />    
    <link href="css/jquery-ui.css" rel="stylesheet" media="all" />
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Products List Type 2 </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary">     
        	
            <ul class="products type2 gallery">
                <li>
                    <div class="product-thumb">
                        <a href="" title="">
                            <span class="onsale"> Sale! <span> </span> </span>
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
                <li class="last">
                    <div class="product-thumb">
                        <a href="" title="">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a href="" title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
                <li>
                    <div class="product-thumb">
                        <a href="" title="">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a href="" title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
                <li class="last">
                    <div class="product-thumb">
                        <a href="" title="">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a href="" title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
                <li>
                    <div class="product-thumb">
                        <a href="" title="">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a href="" title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
                <li class="last">
                    <div class="product-thumb">
                        <a href="" title="">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">                
                        </a>
                        <div class="product-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title=""> <span class="icon-search"> </span> </a>
                        </div>
                    </div> 
                    <h4> Lorem ipsum dolor sit amet </h4>    
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum ultricies orci in luctus. Ut quis ante tellus, eget euismod nibh. Curabitur blandit placerat tellus sed vehicula. </p>
                    <div class="details">  
                        <span class="price"> $720,000.00 </span>
                        <a href="" title="" class="button small green"> Add to cart </a>
                    </div>
                </li>
            </ul>
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>   
            
            <div class="pagination"> 
            	<ul>
                    <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                	<li> <a href="" title=""> 1 </a> </li>
                    <li class="active-page"> 2 </li>
                    <li> <a href="" title=""> 3 </a> </li>
                    <li> <a href="" title=""> 4 </a> </li>
                    <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                </ul>
            </div>
        
        </section><!-- **Primary Section** -->      
        
        <!-- **Secondary Section - End** -->    
        <section id="secondary">
        
        	<h2 class="filtering"> <span> Filtering </span> </h2>
        
            <aside class="widget woocommerce">
                <h3 class="widgettitle"> <a href="" title=""> Search our Products </a> </h3>
                <form action="#" id="searchform" method="get">
                    <input type="text" placeholder="Search our site" name="s" id="s">
                    <input type="submit" name="submit" value="Go">
                </form>
            </aside>   
        
            <aside class="widget woocommerce">
                <h3 class="widgettitle"> <a href="" title=""> Filter by price </a> </h3>
                <div id="slider-range"></div>
                <p class="price-range">
                    <label for="amount">Price range:</label>
                    <input type="text" id="amount" />
                    <a href="" title="" class="button small green"> Filter </a>
                </p>                
            </aside>  
            
            <aside class="widget woocommerce widget_product_categories">
                <h3 class="widgettitle"> <a href="" title=""> Filter by Categories </a> </h3>
                <ul>
                    <li> <a href="" title=""> Apartment <span> 16 </span> </a> </li>
                    <li> <a href="" title=""> Villa <span> 5 </span> </a> </li>
                    <li> <a href="" title=""> Single Family <span> 11 </span> </a> </li>
                    <li> <a href="" title=""> Studio <span> 20 </span> </a> </li>
                    <li> <a href="" title=""> Land <span> 7 </span> </a> </li>
                    <li> <a href="" title=""> Office space for lease <span> 3 </span> </a> </li>
            	</ul>
            </aside> 
            
            <aside class="widget woocommerce">
                <h3 class="widgettitle"> <a href="" title=""> Filter by Top Rated properties </a> </h3>
                <ul class="product_list_widget">
                    <li> 
                        <a href="" title=""> 
                            <img src="http://placehold.it/100x80.jpg" alt="" title="">                                 
                        </a>
                        Integer luctus portto pur nec ornare metus
                        <span class="amount"> $170,000 </span>
                    </li>
                    <li> 
                        <a href="" title=""> 
                            <img src="http://placehold.it/100x80.jpg" alt="" title="">                                 
                        </a>
                        Integer luctus portto pur nec ornare metus
                        <span class="amount"> $170,000 </span>
                    </li>
                    <li> 
                        <a href="" title=""> 
                            <img src="http://placehold.it/100x80.jpg" alt="" title="">                                 
                        </a>
                        Integer luctus portto pur nec ornare metus
                        <span class="amount"> $170,000 </span>
                    </li>
                </ul>
            </aside> 
            
        </section><!-- **Secondary Section -End** -->    
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<script src="js/jquery-ui.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
