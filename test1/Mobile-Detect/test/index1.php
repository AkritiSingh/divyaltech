<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content=" Divyal Technology is a web development company and offer best solution for web and mobile application development, php, coldfusion, wordpress and HTML & CSS. We are located in Chhattisgarh, India ">
	<meta name="author" content="">
    <meta name="google-site-verification" content="DTPEKOcwPsuvzEPSSZQugE7EZuf0Zk5fWIFKiLnpXNw" />
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="newstyle.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/skyblue/style.css" rel="stylesheet" media="all" /> 
	<!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" /> 	
	<!-- bootstrap v3.3.6 css -->
	<link rel="stylesheet" href="css/slider/bootstrap.min.css">
	<!-- owl.carousel css -->
	<link rel="stylesheet" href="css/slider/owl.carousel.css">
	<link rel="stylesheet" href="css/slider/owl.transitions.css">
	<!-- meanmenu css -->
	<link rel="stylesheet" href="css/slider/meanmenu.min.css">
	
	<!-- magnific css -->
	<link rel="stylesheet" href="css/slider/magnific.min.css">
	<!-- venobox css -->
	<link rel="stylesheet" href="css/slider/venobox.css">
	<!-- style css -->
	<link rel="stylesheet" href="css/slider/style1.css">
	<link rel="stylesheet" href="css/slider/font-awesome.min.css">
	<!-- responsive css 
	<link rel="stylesheet" href="css/responsive.css">
-->
	<!-- modernizr css -->
	<script src="javascript/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
    <![endif]-->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<header class="header-one">
            <!-- Start top bar -->
            <div class="topbar-area fix hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class=" col-md-6 col-sm-6">
                            <div class="topbar-left">
                                <ul>
                                    <li><a href="#"><i class="fa fa-envelope"></i>info@divyaltech.com</a></li>
                                    <li><a href="#"><i class="fa fa-phone"></i> (+91) 788-2225066</a></li>
                                </ul>  
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="topbar-right">                                
                                <div class="top-social">
                                    <ul>
										<li><a href="#"><i class="fa fa-skype"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
										<li><a href="#"><i class="fa fa-google"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									</ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End top bar -->
            <!-- header-area start -->
            <div id="sticker" class="header-area hidden-xs">
                <div class="container">
                    <div class="row">
                        <!-- logo start -->
                        <div class="col-md-3 col-sm-3">
                            <div class="logo">
                                <!-- Brand -->
                                <a class="navbar-brand page-scroll sticky-logo" href="index.html">
                                    <img src="img/logo/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- logo end -->
                        <div class="col-md-9 col-sm-9">
                            <div class="header-right-link">
                                <!-- search option start -->
                                <form action="#">
                                    <div class="search-option">
                                        <input type="text" placeholder="Search...">
                                        <button class="button" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                    <a class="main-search" href="#"><i class="fa fa-search"></i></a>
                                </form>
                                <!-- search option end -->
                            </div>
                            <!-- mainmenu start -->
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <div class="main-menu">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li><a class="pagess" href="index.html">Home</a></li>
											<li><a class="pagess" href="#">About us</a></li>
											<li><a class="pagess" href="#">Services</a>
												<ul class="sub-menu">
													<li><a href="services.html">Services</a></li>
													<li><a href="single-service.html">Single Services</a></li>
												</ul>
											</li>
											<li><a class="pagess" href="#">Portfolio</a>
											</li>											
											<li><a href="contact.html">Contact us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- mainmenu end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-area end -->
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu-bar">
                                <div class="logo">
                                    <a href="index.html"><img src="img/logo/logo.png" class="mobile-logo" alt="" /></a>
                                </div>
                                <nav id="dropdown">
                                    <ul>
                                        <li><a class="pagess" href="index.html">Home</a></li>
                                        <li><a class="pagess" href="#">About us</a></li>
                                        <li><a class="pagess" href="#">Services</a>
                                            <ul class="sub-menu">
                                                <li><a href="services.html">Services</a></li>
                                                <li><a href="single-service.html">Single Services</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="pagess" href="#">Portfolio</a></li>
                                        <li><a href="contact.html">Contact us</a></li>
                                    </ul>
                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end -->		
        </header>
        <!-- header end -->
        <!-- Start Slider Area -->
        <div class="intro-area">
           <div class="main-overly"></div>
            <div class="intro-carousel">
                <div class="intro-content">
                    <div class="slider-images">
                        <img src="img/slider/h2.jpg" alt="">
                    </div>
                    <div class="slider-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- layer 1 -->
                                            <div class="layer-1-2">
                                                <h1 class="title2">The Most Promising <span class="color">ColdFusion Web Development Company In India</span></h1>
                                            </div>
                                            <!-- layer 2 -->
                                            <div class="layer-1-1 ">
                                                <p>A Web development company having ColdFusion devlopment experts that provides Effective and Creative solution for all types of your buisness.</p>
                                            </div>
                                            <!-- layer 3 -->
                                            <div class="layer-1-3">
                                                <a href="#" class="ready-btn left-btn" >Our Services</a>
                                                <a href="#" class="ready-btn right-btn" >Contact us</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="intro-content">
                    <div class="slider-images">
                        <img src="img/slider/h2a.jpg" alt="">
                    </div>
                    <div class="slider-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- layer 1 -->
                                            <div class="layer-1-2">
                                                <h1 class="title2">World digital resolutions with <span class="color">Mobile App Development</span></h1>
                                            </div>
                                            <!-- layer 2 -->
                                            <div class="layer-1-1">
                                                <p>We make innovative mobile apps to make digital and accessible to the world.</p>
                                            </div>
                                            <!-- layer 3 -->
                                            <div class="layer-1-3">
                                                <a href="#" class="ready-btn left-btn" >Our Services</a>
                                                <a href="#" class="ready-btn right-btn" >Contact us</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Slider Area -->
    
    <!-- **Main** -->
    <div id="main">
	
        <!-- **Container** -->
        <div class="container-main">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        	<div class="intro-text type1">
            	<!--h4> Welcome to our <span class="highlight"> Divyal Technology</span> - Software and Web Application Development Company</h4-->
                <h6 style="text-transform: capitalize; font-size: 16px;">A True Coldfusion Development Company Which Takes Care of All Your Online Needs!</h6>
            </div>
            
            <div class="hr-invisible-small"> </div>
			
            <!--<div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span><img src="images/sociable/cflogo.png" alt="coldfusion development" style="padding-top: 12px;padding-left: 8px; "> </span> </div>
                    <div style="text-align:center"><h5><a href="services.php" title="">Coldfusion Development </a> </h5></div>
                    <div style="text-align:justify"><p>At Divyaltech, we have been working in ColdFusion. We are committed to providing our clients with the best solutions, quality services and superior support.</p></div>
                </div>
            </div>
			
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span><img src="images/sociable/settings-2.png" alt="website maintenance"  style=" padding-top: 22px; "> </span>  </div>
                    <div style="text-align:center"><h5> <a href="services.php" title="">Website Support and Maintainance </a> </h5></div>
                    <div style="text-align:justify"><p> We offer website maintenance and management that is expert, economical and takes care of all your web support needs. </p></div>
                </div>
            </div>
            
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span><img src="images/sociable/multiple-devices.png" alt="web development" style=" padding-top: 22px; "> </span>  </div>
                    <div style="text-align:center"><h5> <a href="services.php" title=""> Web Development</a> </h5></div>
                    <div style="text-align:justify"><p>Divyal Technology is a chief web application development organization and we offer web solutions to our clients which are custom-made to achieve their respective goals, vision and mission. </p></div>
                </div>
            </div>
			
			<div class="column one-fourth last">
                <div class="ico-content type1">
                	<div class="icon"> <span><img src="images/sociable/technology.png" alt="mobile app development"  style=" padding-top: 22px; "> </span>  </div>
                    <div style="text-align:center"><h5> <a href="services.php" title=""> Mobile App Development </a></h5></div>
                    <div style="text-align:justify"><p>Divyal Technology provide you PhoneGap Development services to build your app to by expertise PhoneGap Developer. </p></div>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div> -->
            
			<!-- Start Service area -->
		<div class="services-area area-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
					        <h6 class="small-title">What we offer</h6>
						    <h3>Our Services</h3>
						    <p>Our consultants opt in to the projects they genuinely want to work on, committing wholeheartedly to delivering.</p>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="services-all">
                    	<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
						   <div class="single-services">
								<a class="service-images" href="#"><img src="img/service/cf.png" alt="coldfusion development"></a>
								<div class="service-content">
									<h4><a href="#">COLDFUSION DEVELOPMENT</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
					    <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><i class="fa fa-mobile"></i></a>
								<div class="service-content">
									<h4><a href="#">MOBILE APP DEVELOPMENT</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
							   <a class="service-images" href="#"><img src="img/service/java.jpg" alt="coldfusion development"></a>
								<div class="service-content">
									<h4><a href="#">Java Application Development</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><img src="img/service/php.png" alt="coldfusion development"></a>
								<div class="service-content">
									<h4><a href="#">PHP Web Development</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><i class="fa fa-desktop"></i></a>
								<div class="service-content">
									<h4><a href="#">Desktop Application Development</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><i class="fa fa-gear"></i></a>
								<div class="service-content">
									<h4><a href="#">Website Support and Maintenance</a></h4>
									<p>Our independent consultants, free from the internal demands of traditional firms, can focus.</p>
								</div>
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
		<!-- End Service area -->
			
            <div class="border-title"> <h2> Latest from Portfolio <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">                   
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/socialsense/socialsense1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                               
                                <a href="images/portfolio/socialsense/socialsense6.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="link"> <span class="icon-fullscreen"> </span> </a>								
								<a href="images/portfolio/socialsense/socialsense2.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense4.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense5.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/socialsense/socialsense1.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                               
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Social Sense</a> </h5>
                            <p> www.socialsense.outerdata.com </p>
                        </div>
                    </li>
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/elite-dealers/elite1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/portfolio/elite-dealers/elite2.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/elite-dealers/elite3.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>								
								<a href="images/portfolio/elite-dealers/elite4.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/elite-dealers/Responsive-elite.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/elite-dealers/elite1.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Elite Dealers</a> </h5>
                            <p> www.elite-dealers.com </p>
                        </div>
                    </li>
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/seaweedhealthnews/seasweet1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                                
								<a href="images/portfolio/seaweedhealthnews/seasweet2.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/seasweet4.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                  <a href="images/portfolio/seaweedhealthnews/seasweet6.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/Responsive-seaweed.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/seaweedhealthnews/seasweet1.png" data-gal="prettyPhoto6[]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                              
                                							
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Sea Weed Health News</a> </h5>
                            <p> www.seaweedhealthnews.com </p>
                        </div>
                    </li>                      
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!--**Portfolio Carousel Wrapper - End** -->         
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> FAQ's <span> </span> </h2> </div>
                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set faq">                  
                    <h5 class="toggle-accordion"> <a href="#" title="">Why we choose you/ why should we work with you?</a> </h5>
                    <div class="toggle-content">
                        <p> We are the team of very good experienced developers, designers. We take care of all your
                            requirements, your business need and provide best solutions. We follow the process of
                            understanding your issues and requirements first and then code.
                        </p>
                    </div>
                 
                    <h5 class="toggle-accordion"> <a href="#" title="">What is the quality of people you employ?</a> </h5>
                    <div class="toggle-content">
                        <p> Our all peoples are well educated and post graduated in Commuters and engineering. Well
                            experienced.</p>
                            <ul>
                              <li>20+ Man-Years Experience in ColdFusion and web technologies.</li>
                              <li>Our people are flexible working at your working hours if your need.</li>
                              <li>All people have experience on off-shore projects with US/European clients.</li>
                              <li>All the projects QA are done by highly experienced Team Leads before deliver to you.</li>
                            </ul>
                        
                    </div>
                   
                   <h5 class="toggle-accordion"> <a href="#" title=""> What’s you rates and payment terms?</a> </h5>
                    <div class="toggle-content">
                        <p>We follow different payment models, we work fixed price as well as hourly and monthly bases.</p><br>
                        <p><strong>Fixed Price- </strong>It’s based on project requirement and size of the project. So please                           write us for quote with your requirements.
                        </p>
                           <ul>
                              <li>We take 20% of project cost on start of project.</li>
                              <li>40% on first release.</li>
                              <li>40% on final release and confirmation.</li>
                           </ul>
                           
                     <p><strong>Hourly Rates-</strong>–Our hourly charges are between $10-$15 based on technical resource you
                      need and project complexity. Our Resource based charges are as below for your idea -
                     </p>
                           <ul>
                              <li>$15/ hour for 5+ year’s senior developer, Team Leads.</li>
                              <li>$13/ hour for 3+ year’s developers or designers.</li>
                              <li>$10/ hour for junior developers/designers.</li>
                           </ul>  
                           
                           <p>
                           <strong>Monthly Rates – </strong>Our monthly rates are starts from $1000 to $2000 per month depending                            on availability your need and resources experience level or project complexity.
                           </p>
                    </div>
               
                     <h5 class="toggle-accordion"> <a href="#" title="">How you provide support remotely if we 
                         need it instantly?</a> </h5>
                    <div class="toggle-content">
                        <p>We use skype for instant support and discussion. You can contact us via email or call in skype. Our
                           offices are open 15 hours a day.<br>
                           If you need support at off hours or on weekends you will have our phone numbers to call our
                           support team ask for instant support with details. Support team will engage a developer for you to
                           get your issue resolve or job done within half an hour.
                        </p>
                    </div>                     
                </div> <!-- **Toggle Frame Set - End** --> 
                
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Testimonials </h2> </div>
                
                <div class="testimonial">
                    <div class="author">
                        <img src="images/text2image.jpg" alt="divyal Technology website" title="">
                    </div>                       
                    <blockquote>
                    	<q> I tested Divyal Technologies and every time I had issues, I got almost immediate responses.  With a 12 hour time difference, I was truly skeptical.  I don’t know how it’s done, but I get responses within minutes, not hours, relieving my stress.I am so pleased with the work.


</q>
                        <cite> Marc L.<br>
                            Online Technologies </cite>
                    </blockquote>
                </div>
                <div class="testimonial">
                    <div class="author">
                        <img src="images/testimonial.jpg" alt="divyaltech website development" title="">
						
                    </div>                       
                    <blockquote>
                    	<q> Manoj has helped me with many web projects over several years. He has always been highly professional, communicative and able to deliver projects on time and on budget. Highly recommended. </q>
                        <cite> John Carratt<br>
						 SeeInside.co.uk
						</cite>
                    </blockquote>
                </div>
            </div>
			
			<!-- Start testimonials Area -->
			<div class="testimonial-area area-padding">
				<div class="test-overly"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="section-headline text-center">
								<h6 class="small-title">what client say</h6>
								<h3>Testimonial</h3>
								<p>Our consultants opt in to the projects they genuinely want to work on, committing wholeheartedly to delivering.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="Reviews-content">
								<!-- start testimonial carousel -->
								<div class="testimonial-carousel item-indicator">
									<div class="single-testi">
										<div class="testi-img ">
											<img src="img/review/1.jpg" alt="" style="max-width:80px;">
											<div class="texti-name">
												<h4>Maxwell</h4>
												<span class="guest-rev"><a href="#">Genarel customer</a></span>
											</div>
										</div>
										<div class="client-rating">
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
										</div>
										<div class="testi-text">
											<p>Designer have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.</p>
										</div>
									</div>	
									<!-- End single item -->
									<div class="single-testi">
										<div class="testi-img ">
											<img src="img/review/3.jpg" alt="" style="max-width:80px;">
											<div class="texti-name">
												<h4>Roderick</h4>
												<span class="guest-rev"><a href="#">Genarel customer</a></span>
											</div>
										</div>
										<div class="client-rating">
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
										</div>
										<div class="testi-text">
											<p>Designer have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.</p>
										</div>
									</div>
									<!-- End single item -->
									<div class="single-testi">
										<div class="testi-img ">
											<img src="img/review/4.jpg" alt="" style="max-width:80px;">
											<div class="texti-name">
												<h4>Zachariah</h4>
												<span class="guest-rev"><a href="#">Genarel customer</a></span>
											</div>
										</div>
										<div class="client-rating">
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
										</div>
										<div class="testi-text">
											<p>Designer have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.</p>
										</div>
									</div>
									<!-- End single item -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End testimonials end -->         
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->




<!-- all js here -->
<!-- jquery latest version -->
<script src="javascript/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap js -->
<script src="javascript/bootstrap.min.js"></script>
<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- <script src="js/jquery.js"></script> -->
<script src="js/jquery.mobilemenu.js"></script>
<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
<!-- owl.carousel js -->
<script src="javascript/owl.carousel.min.js"></script>
<!-- Counter js -->
<script src="javascript/jquery.counterup.min.js"></script>
<!-- waypoint js -->
<script src="javascript/waypoints.js"></script>
<!-- isotope js -->
<script src="javascript/isotope.pkgd.min.js"></script>
<!-- stellar js -->
<script src="javascript/jquery.stellar.min.js"></script>
<!-- magnific js -->
<script src="javascript/magnific.min.js"></script>
<!-- venobox js -->
<script src="javascript/venobox.min.js"></script>
<!-- meanmenu js -->
<script src="javascript/jquery.meanmenu.js"></script>
<!-- Form validator js -->
<script src="javascript/form-validator.min.js"></script>
<!-- plugins js -->
<script src="javascript/plugins.js"></script>
<!-- main js -->
<script src="javascript/main.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

<!-- Layer Slider -->
<script src="js/jquery-easing-1.3.js"></script>
<script src="js/jquery-transit-modified.js"></script>




</body>
</html>
