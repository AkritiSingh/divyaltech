<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Desktop Application Development </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong> Desktop Application Development</strong> <!--<img style="max-width:30%;" title="App Development" alt="App Development" src="images/desktop1.jpg">--></span></h6>
                   <p>Desktop stands for outwardly rich, exceptionally quick and responsive programming. In our team, we support 
                      best practices in desktop application advancement keeping in mind the end goal to dependably pull off that 
                      additional mile which transforms a strong desktop application thought into a business sector sensation.
                  </p>
              
            </div>
            <br><br>
           <div class="da-cantent"> 
             <h6><span style="font-size: 27px;color: #464646;">
                 <strong> Hire Desktop Application Developers,On Demand</strong></span></h6>
            <p>We, at <strong>Divyaltech</strong> have the best Desktop Application developers and teams.Our group of desktop 
               developers constructs extensive scale standalone products. Divyaltech the best in Desktop Application Development 
               Company resolved to give top quality, gainful and brief arrangements, with the main objective of meeting and 
               exceeding customer desires.We offer desktop programming application advancement administrations to our regarded 
               customers at extremely reasonable cost.
            </p>       
          <!-- MENU-LOCATION=NONE --><br><br>
           <h6><span style=" font-size: 27px;color: #464646;">Our Ability in Desktop Application Development: </span></h6>
           <p>We have involvement in every significant stage for application advancement like Microsoft.NET, WPF, JAVA and so 
             forth. All products and arrangements are advertised under our customers' names or their association's names are 
             welcomed by their clients around the world. We give windows and desktop applications development in.</p>  
             <ul>
                 <li>C#</li>
                 <li>Visual C++</li>
                 <li>.Net (Dot Net)</li>
                 <li>Java</li>
                 <li>C, C++</li>
                 <li>Visual Basic</li>
                 <li>SQL Server</li>
             </ul>
                
         </div>
           <br>
           <br>
           <div class="service">
           <h6><span style=" font-size: 27px;color: #464646;">Desktop Development Services Provided by Us: </span></h6>
             <ul>
                 <li>Desktop software management</li>
                 <li>Desktop to web software migration</li>
                 <li>Desktop application developers</li>
                 <li>Desktop and web application integration</li>
                 <li>Software services outsourcing</li>
            </ul>
           
           </div>
      </div> 
          <div class="rightside">
           <?php include 'technology.php';?>
           <img style="max-width:30%; margin-top: 45px;" title="Web Development" alt="Web Development" src="images/desktop2.png">
             <div class="type" style="margin-top:18px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Desktop Application Types We Worked On:</span> </h6>
                  <ul> 
                    <li>Standalone business applications</li>
                    <li>Client-server applications</li>
                  	<li>Collaborative applications</li>
					<li>Desktop games</li>
                    <li>Utilities and plug-ins</li>
                    <li>System apps and services</li>
		         </ul> 
               </div>
               
             <!--<div class="tech" style="margin-top:65px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Technologies We Use:</span> </h6>
                  <ul> <strong>Front End</strong>
                    <li>HTML,CSS3</li>
                  	<li>JavaScript</li>
					<li>AJAX</li>
		         </ul> 
                   <ul> <strong>Back End</strong>
                    <li>Cold Fusion,PHP</li>
                    <li>MYSQL,MSSQL,Oracle,SQL</li>
			     </ul>      
               </div>-->
               
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
