<footer id="footer">
    	<div class="container">
        
			<div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Divyal Technology </a> </h3>
                    <ul>
                        <li> <a href="" title=""> Web Application Development </a> </li>
                        <li> <a href="" title=""> Mobile Appliacation Development </a> </li>
                        <li> <a href="" title=""> PHP Development</a> </li>
                        <li> <a href="" title=""> Internet Marketing </a> </li>
                        <li> <a href="" title=""> ColdFusion Development </a> </li>        
                        <li> <a href="" title=""> WordPress Development </a> </li>
                    </ul>
                </aside>   
			</div>
            
          <!--  <div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Featured Products </a> </h3>
                    <ul class="product_list_widget">
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                    </ul>
                </aside>   
			</div> -->
        
        
        	<div class="column one-fourth">
                <aside class="widget tweetbox"> 
                    <h3 class="widgettitle"> <a href="" title=""> Twitter Feeds </a> </h3>
                    <div class="tweets"> </div>
                </aside>   
            </div>  
            
            <div class="column one-fourth last">
            	<aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Contact Info </a> </h3>
                	<p> Aditya Nagar, Durg, Chhattisgarh, India </p>
                    <p> <span class="icon-phone"> </span> Phone : (+91) 788-2225066 </p>
                    <p> <span class="icon-envelope-alt"> </span> Email : <a href="mailto:info@divyaltech.com"> info@divyaltech.com </a> </p>
                    <p> <span class="icon-globe"> </span> Website : <a href="" title=""> www.divyaltech.com </a> </p> 
                    <ul class="social-icons">
                        <li>
                            <a href="https://www.facebook.com/Divyal-Technologies-572881792864790/" title="facebook"> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="facebook">
                                <img src="images/sociable/facebook.png" alt="" title="facebook">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/u/0/108727414415824265010" title="google+"> 
                                <img src="images/sociable/hover/google.png" alt="" title="google+">
                                <img src="images/sociable/google.png" alt="" title="google+">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/DivyalTech" title="twitter"> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="twitter">
                                <img src="images/sociable/twitter.png" alt="" title="twitter">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://dribbble.com/divyaltech" title="dribbble"> 
                                <img src="images/sociable/hover/dribble.png" alt="" title="dribbble">
                                <img src="images/sociable/dribble.png" alt="" title="dribbble">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </aside>	
            </div>
            
        </div>
        
        <div class="copyright">        	
        	<div class="container">
                <p> Copyright &copy; 2015 Divyal Technology All Rights Reserved. | <a href="" title=""> Design Themes </a> </p>        	
            </div>
        </div>
    </footer>
	