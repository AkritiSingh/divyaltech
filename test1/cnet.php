<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> C# Development </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong> C# Development</strong></span></h6>
                   <p>Divyaltech is one of the top Microsoft DotNet (.Net) website development company. Our group of expert 
                      developers has limitless experience on Dot Net development. Divyaltech has encountered and skill in an 
                      expansive scope of technologies and languages such as VB.NET, ASP.NET, C#.Net, C, C++, MS SQL, XML AJAX 
                      and JSON web Services.
                    </p>
                     <p>ASP.Net Framework offers quantities of points of interest over other comparative web development 
                       technologies. ASP.Net Framework is in fact more progressed and offers quantities of different advantages 
                       with the backing of Microsoft and its Server and Database Technologies. In addition, it's anything but 
                       difficult to start creating custom web application on ASP.Net Web Framework, it's also saves development 
                       times utilizing much effective ASP.Net Framework and it's tools.
                   </p>
                   
                   <h6><span style="font-size: 27px;color: #464646;">
                <strong> Why Divyaltech for .Net Development?</strong></span></h6>
                <p>
                Divyaltech is a software development company, giving an expansive size of top of the line programming 
                administrations to the customers over the circle. At outsourcing, seaward .NET improvement administrations, we 
                cater top line software solutions in different modern markets and spaces, including account, IT, medicinal 
                services, visits and ventures, protection and others.
                </p> 
            </div>
            <br><br>
           <div class="da-cantent"> 
                <h6><span style="font-size: 20px;color: #464646;">
                <strong>Our .Net technology and web development services are:</strong></span></h6>
                  <ul> 
                    <li>JCRM (Customer Relationship Management) Development using Dot Net Framework</li>
                    <li>Custom Web Application based on ASP.Net & MS SQL</li>
                    <li>Customize Dot Net Development</li>
                    <li>Dot Net Website Development</li>
                    <li>Dot Net Responsive Website Development</li>
					<li>Dot Net Web Portal Development</li>
                    <li>Custom .Net Web Services/APIs development</li>
                    <li>eCommerce Development using Dot Net Framework</li>
                    <li>Database Application Development</li>
                    <li>Content Management System using Dot Net Framework</li>
                    <li>Dot Net Website/Portal Support & Maintenance</li>
                    <li>Dot Net Migration</li>
                    <li>Higher Individual Dot net Developers/Programmers or Team</li>
		         </ul> 
            
         </div>
           <br>
           <br>
           <div class="type" style="margin-top:0px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Main Benefits of .Net development:</span> </h6>
                  <ul> 
                    <li>It significantly reduces the amount of code vital in immense scale web applications which are made in .NET structure.</li>
                    <li>It offers server controls and blueprints with the force of movable and automatic operation.</li>
                    <li>Web application development in .Net framework is highly secure and reliable.</li>
                    <li>Source code and HTML code are isolated, so editing  is entirely simple.</li>
                 </ul> 
           </div>
      </div> 
          <div class="rightside">
           <?php include 'technology.php';?>
           <img style="max-width:30%;" title="App Development" alt="App Development" src="images/cnet.PNG">
                            
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
</div><!-- **Wrapper - End** -->
<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
