<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />    
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Portfolio Single Layout 2 </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<!-- **Portfolio Detail** -->
        	<div class="portfolio-single">
            
                <div class="portfolio-slider-container">
                    <ul class="portfolio-slider">
                        <li> <img src="http://placehold.it/1060x475.jpg" alt="" title=""> </li>
                        <li> <img src="http://placehold.it/1060x475.jpg" alt="" title=""> </li>
                        <li> <img src="http://placehold.it/1060x475.jpg" alt="" title=""> </li>
                        <li> <img src="http://placehold.it/1060x475.jpg" alt="" title=""> </li>
                    </ul>
                    <div class="slider-arrows">
                        <a href="" title="" class="prev"> </a>
                        <a href="" title="" class="next"> </a>
                    </div>
                </div>
            
                <h3> Lorem ipsum dolor sit amet, consect </h3>
                <h6> Pellentesque habitant </h6>
                <p>  Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec sit amet facilisis urna. Cras placerat ipsum nulla. Vestibulum quam diam, auctor vitae pretium quis, interdum quis nulla. Curabitur vulputate massa lorem. Proin nec ante porttitor sapien dapibus volutpat. Quisque quis ante dui, sed iaculis dui. Morbi vitae rhoncus erat. Duis nec leo massa. </p>
                
                <h5> Project Details </h5>                    
                <p> <span class="icon-user"> </span> <strong> Client Name : </strong> designthemes </p>
                <p> <span class="icon-map-marker"> </span> <strong> Location : </strong> Melbourne, Australia </p>
                <p> <span class="icon-link"> </span> <strong> Website : </strong> <a href="" title=""> http://designthemes.com </a> </p>
                <div class="portfolio-share"> <img src="images/post-images/portfolio-share.jpg" alt="" title=""> </div>
                
                <div class="post-nav-container">
                    <div class="post-prev-link"><a rel="prev" href=""> <i class="icon-circle-arrow-left"> </i> Vestibulum congue <span>(Prev Entry)</span></a> </div>
                    <div class="post-next-link"><a rel="next" href=""> <span>(Next Entry)</span> Maecenas ut sem neque  <i class="icon-circle-arrow-right"> </i> </a></div>
                </div>    
                            
            </div><!-- **Portfolio Detail End** -->
            
            <div class="hr-invisible-small"> </div>
            
            <div class="border-title"> <h2> Related Projects <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!-- **Portfolio Carousel Wrapper - End** -->
            
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.bxslider.min.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
