<header id="header">
    
    	<!-- **Top Bar** -->
    	<div id="top-bar">
        	<div class="container">
            	<p class="phone-no"> (+91) 8359825066 <a href="" title=""> info@divyaltech.com </a> </p>
                <ul class="social-icons">
                    <li>
                    <a href="https://twitter.com/DivyalTech" class="icon-button twitter" title="twitter"><i class="icon-twitter"></i><span></span></a>
                    </li>
                    <li>
                    <a href="https://www.facebook.com/Divyal-Technology-572881792864790/" class="icon-button facebook" title="facebook" ><i class="icon-facebook"></i><span></span></a>
                    </li>
                    <li>
                    <a href="" class="icon-button google-plus" title="google+"><i class="icon-google-plus"></i><span></span></a>
                    </li>
                    <li>
                    <a href="" class="icon-button linkedin " title="linkedin"><i class="icon-linkedin"></i><span></span></a>
                    </li>
                  
                </ul>
            </div>
        </div><!-- **Top Bar - End** -->
        
        <div class="container">
        	<!-- **Logo - End** -->
            <div id="logo">
            	<a href="index.php" title=""><img src="images/logo2.png" alt="Divyal Technology"></a>
            </div><!-- **Logo - End** -->
            
            <!-- **Navigation** -->
            <nav id="main-menu">
            	<ul>
		    <li <?php if (ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Index'){?>class="current_page_item"<?php }?> >
			 <a href="index.php" title="">  Home </a> <span> </span>     
                    </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'],PATHINFO_FILENAME))=='Aboutus'){?> class="current_page_item" <?php }?>>
					 <a href="aboutus.php" title=""> About </a> <span> </span> </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Services'){ ?> class="current_page_item"  <?php }?>>
					 <a href="#" title=""> Services </a> <span> </span>      
                     <ul>
                      
                        <li class=""><a href="coldfusion-development.php">Coldfusion Development</a></li>
                       <!-- <li class=""><a href="php-development.php">PHP Development</a></li>
                        <li class=""><a href="cnet.php">C# Developer</a></li> -->
                        <li class=""><a href="web-application.php">Web Application Development</a></li>
                        <li class=""><a href="mobile-development.php">Mobile Application Development</a></li>
                        <li class=""><a href="desktop-application.php">Desktop Application Development</a></li>                  
                        <li class=""><a href="server-and-web.php">Server and Website Support</a></li>
                                               
                     </ul>               	
                    </li>                 
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Portfolio'){ ?>class="current_page_item"<?php }?>> <a href="portfolio.php" title=""> Portfolio </a> <span> </span>                     
                    </li>                   
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Contactus'){ ?>class="current_page_item"<?php }?>> <a href="contactus.php" title=""> Contact Us </a> <span> </span>                                           
                    </li>
                </ul>
            </nav><!-- **Navigation - End** -->      
        </div>
    	
    </header>