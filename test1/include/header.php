<header id="header">
    
    	<!-- **Top Bar** -->
    	<div id="top-bar">
        	<div class="contain">
                
            	<p class="phone-no" style=""> (+91) 8359825066 <a  href="" title=""> info@divyaltech.com </a> </p>
                <ul class="social-icons">
                    <li>
                    <a href="https://www.facebook.com/Divyal-Technology-572881792864790/" title="facebook"> 
                            <img src="images/sociable/facebook1.png" alt="" title="facebook">
                            <img src="images/sociable/hover/facebook.png" alt="" title="facebook">                        
                        </a>
                    </li>
                    <li>
                        <a href="" title="google+"> 
                            <img src="images/sociable/google1.png" alt="" title="google+">
                            <img src="images/sociable/hover/google.png" alt="" title="google+">                        
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/DivyalTech" title="twitter"> 
                            <img src="images/sociable/twitter1.png" alt="" title="twitter">
                            <img src="images/sociable/hover/twitter.png" alt="" title="twitter">                        
                        </a>
                    </li>
                    <li>
                        <a href="https://dribbble.com/divyaltech" title="dribbble"> 
                            <img src="images/sociable/dribble1.png" alt="" title="dribbble">
                            <img src="images/sociable/hover/dribble.png" alt="" title="dribbble">                        
                        </a>
                    </li>
                    <li>
                        <a href="" title="linkedin"> 
                            <img src="images/sociable/linkedin1.png" alt="" title="linkedin">
                            <img src="images/sociable/hover/linkedin.png" alt="" title="linkedin">                        
                        </a>
                    </li>
                </ul>
            </div>
        </div><!-- **Top Bar - End** -->
        
        <div class="contain">
        	<!-- **Logo - End** -->
            <div id="logo">
            	<a href="index.php" title=""><img src="images/logo2.png" alt="Divyal Technology"></a>
            </div><!-- **Logo - End** -->
            
            <!-- **Navigation** -->
            <nav id="main-menu">
            	<ul>
		    <li <?php if (ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Index'){?>class="current_page_item"<?php }?> >
			 <a href="index.php" title="">  Home </a> <span> </span>     
                    </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'],PATHINFO_FILENAME))=='Aboutus'){?> class="current_page_item" <?php }?>>
					 <a href="aboutus.php" title=""> About </a> <span> </span> </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Services'){ ?> class="current_page_item"  <?php }?>>
					 <a href="#" title=""> Services </a> <span> </span>      
                     <ul>
                      
                        <li class=""><a href="coldfusion-development.php">Coldfusion Development</a></li>
                       <!-- <li class=""><a href="php-development.php">PHP Development</a></li>
                        <li class=""><a href="cnet.php">C# Developer</a></li> -->
                        <li class=""><a href="web-application.php">Web Application Development</a></li>
                        <li class=""><a href="mobile-development.php">Mobile Application Development</a></li>
                        <li class=""><a href="desktop-application.php">Desktop Application Development</a></li>                  
                        <li class=""><a href="server-and-web.php">Server and Website Support</a></li>
                                               
                     </ul>               	
                    </li>                 
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Portfolio'){ ?>class="current_page_item"<?php }?>> <a href="portfolio.php" title=""> Portfolio </a> <span> </span>                     
                    </li>                   
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Contactus'){ ?>class="current_page_item"<?php }?>> <a href="contactus.php" title=""> Contact Us </a> <span> </span>                                           
                    </li>
                </ul>
            </nav><!-- **Navigation - End** -->      
        </div>
    	
    </header>