﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="Divyal Technology is Software and IT company provide IT solution for web and mobile. We create responsive web design suitable for seo and smm">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> About Us </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<!--div class="page-slider-container">
                <ul class="page-slider">
                    <li> <img src="images/about1.jpg" alt="divyaltech web development" title="" style="width:1060px;height:360px;" > </li>
                    <li> <img src="images/about2.jpg" alt="divyaltech php web development" title="" style="width:1060px;height:360px;"> </li>
                    <li> <img src="images/about3.png" alt="divyaltech social media marketing" title="" style="width:1060px;height:360px;"> </li>
                </ul>                
            </div-->
        
            <div class="clear"> </div>
            <!--div class="hr-invisible-small"> </div-->
            
			<div class="column one-half">
                <div class="border-title"> <h2> About Us  </h2> </div>                
                <!-- **Toggle Frame Set** -->  

				<p>
				<strong>Divyal Technologies</strong> is a web development company started by a group of experienced developers after working in<strong> MNC's software development companies</strong> and work experience as part time freelancers for couple years in multiple technologies. 
				</p>
				
				<p>
				All developers have good experience of working in multiple projects and technologies as offshores and for direct clients from abroad most of from USA and European countries. Our <strong>working hours are flexible as needed to clients</strong>. 
				</p>
				
				<p>
				We located in <strong>India, Chhattisgarh</strong> and provide our services remotely. You can outsource your work to us in very low rates. As we know nature of IT industry and impacts of your business if any failure, our highly experienced and dedicated team ready to provide you <strong>100% support and maintenance services</strong> in your working hours.
				</p>	
                <p>
				<strong>We provide our service mainly in</strong> <strong style="color:red;">ColdFusion</strong>. We are able serve maintenance service to large medium and small sizes ERP projects as well as 100% server and database support.  We work on other technologies as well like <strong>PHP, C# and java tools.</strong>
				</p>	
                <p>
                We have expert designers for front end designs. We use <strong>MySQL/MSSQL/Oracle Database servers.</strong>
                </p>
            </div>
			
            <div class="column one-half last">
                <div class="border-title"> <h2> Why Choose us? </h2> </div>                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set">
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title="">A group of experienced developers </a> </h5>
                        <div class="toggle-content">
                            <p> We have experienced developers to get your job done with quality work. We use latest technologies
                                to make it perfect and user friendly. We make small apps, manage medium size app as well as
                                complex ERP web applications and providing dedicated support and maintenance. 
                           </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Awesome support and maintenance service</a> </h5>
                        <div class="toggle-content">
                            <p>We understand that this is the most important thing to manage long term relationship. We respect
                              your time and business need. Our support base is Awesome not because of large number but
                              dedicated employees. Our team work in flexible hours as required to client. So any type of server
                              down or other issue shouldn’t impact your business. We know that our working hours may differ
                              with clients but we manage our working hours as required to you and not a single problems to do
                              late hours if required. We always open to hear your issues and get these resolve ASAP. </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                      <h5 class="toggle-accordion"> <a href="#" title="">Save money by outsource your work in low rates</a></h5>
                        <div class="toggle-content">
                            <p> Our rates are too low on behalf of what we are committing. Not because of Quality but the living
                                cost of place where we live. Our company believe more to do great things then making profit.                                 Profit is the last thing we consider. Our employees work for themselves and for clients.
                            </p>
                        </div>
                    </div>
                	<div class="toggle-frame">                    
                        <h5 class="toggle-accordion"> <a href="#" title="">Contact us if you have any other concerns</a></h5>
                        <div class="toggle-content">
                            <p> Still you have any questions in mind? Chat with us or use contact us form and write your 
                            questions.We will answer all your concerns and questions honestly. If anything we are not comfortable                             with your work or we don’t have resource we will not do any false commitment. </p>
                        </div>
                    </div>
                </div> <!-- **Toggle Frame Set - End** -->                 
            </div>
            <!--
            <div class="column one-half last">
                <div class="border-title"> <h2> Our Skills <span> </span> </h2> </div>
                
                <div class="progress progress-striped">
                	<div data-value="90" class="bar">
                    	<div class="bar-text"> HTML5 &amp; CSS3 <span> 90% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped ">
                	<div data-value="70" class="bar">
                    	<div class="bar-text"> Wordpress <span> 70% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="80" class="bar">
                    	<div class="bar-text"> PHP <span> 80% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="60" class="bar">
                    	<div class="bar-text"> Photoshop <span> 60% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="50" class="bar">
                    	<div class="bar-text"> jQuery <span> 50% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="75" class="bar">
                    	<div class="bar-text"> Web Design <span> 75% </span> </div>
                    </div>
                </div>
            </div>
            -->
            
			<div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Services <span> </span> </h2> </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-cogs"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title=""> PHP Development </a> </h5>
                    <p> We at Divyal  Technology develops sites considering the user’s perspective to offer complete satisfaction. . The experienced team of PHP developers at Divyal Technology have the ability to bring the best to the table while dealing with latest technologies, user interface and methodologies </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-beaker"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title="">Web Application Development </a> </h5>
                    <p> We convey unparalleled web development solutions for fit each of our clients' special business requirements and strategies. Our group of specialists use new innovations, improved systems and mature methodologies to create applications that encourage business change and development </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magic"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title="">Mobile Application Development </a> </h5>
                    <p> Our team of mobile app developers is creative and knowledgeable to achieve your individual demands according your business needs. Our mobile apps developers makes highly customized mobile applications for clients needs and enterprises using advanced tools and technology. </p>
                </div>
            </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magnet"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title=""> HTML5 &amp; CSS3 </a> </h5>
                    <p> Divyal technology  delivers the best HTML 5 Development solutions by using a mix of skills and expertise. Our experienced and  professionals provides the best results for HTML5 application development services </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-dashboard"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title="">  </a>Hire Developers</h5>
                    <p> Hiring dedicated, skilled, experienced, expert software developer  from Divyal Technology for your project work offers lot of advantage in terms of Cost Saving, Getting Experts and Shorter startup Time </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5> <a href="http://divyaltech.com/services.php" title=""> SEO &amp; SMM  </a> </h5>
                    <p>At Divyal Technology,  we know how to build up your brand presence across online channels according to your business. Our SEO-Specific services influence this insight to deliver significant SEO process improvements, actionable SEO strategies for rank improvement, cost management and growth </p>
                </div>
            </div>
            
           <!-- <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Partners <span> </span> </h2> </div>-->
            
            <!-- **Partner Carousel Wrapper** -->
         <!--   <div class="partner-carousel-wrapper">
                <ul class="partner-carousel">
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="codeigniter web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="divyal technology website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="divyaltech website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="html and css web development" title=""> </a> </li>
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="wordpress web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="social media marketing and seo" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="cold fusion development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="website development in php" title=""> </a> </li>
                </ul>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div>--><!-- **Partner Carousel Wrapper - End** -->
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>

<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
<script src="js/jquery.viewport.js"></script>
<script src="js/twitter/jquery.tweet.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/clickme.js"></script>
</body>
</html>
