<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content=" Divyal Technology is a web development company and offer best solution for web and mobile application development, php, coldfusion, wordpress and HTML & CSS. We are located in Chhattisgarh, India ">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />  
    <link rel="stylesheet" href="css/layerslider.css" type="text/css">
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
    <![endif]-->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    	<!-- **Slider Section** -->
    	<section id="slider">
        
            <div id="layerslider-container-fw">    
                <div id="layerslider" style="width:100%; height:400px; margin:0px auto; ">
                        
                  <div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; transition3d:all;">      
                  
                      <img alt="website design in php" src="images/slider/page1-img8.jpg" class="ls-bg">
                      <img alt="web app development" class="ls-s-1"  src="images/slider/mac.png"  style="position:absolute; top:79px; left:468px; slidedirection:top;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                      <img alt="wepsite development in php" class="ls-s-1"  src="images/slider/ipad-slider-stage.png"  style="position:absolute; top:184.00001525878906px; left:634px; slidedirection:right;   durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                     <img alt="website design" class="ls-s-1" src="images/slider/ipad-slider.png" style="position: absolute; top: 110px; left: 250px; width: 207px; height: 307px; padding: 0px; border-width: 0px; opacity: 1; transform: rotate(0deg) scale(1, 1); margin-left: 0px; margin-top: 0px; display: block; visibility: visible;">
                      <img alt="divyal technology design" class="ls-s-1"  src="images/slider/iphone-features.png"  style="position:absolute; top:245.00001525878906px; left:452px; slidedirection:left;   durationin:3000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <h2 class="ls-s-1" style="position:absolute; top:50px; left:-4px; slidedirection:left;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;">Web Application Development</h2>
                        <div class="ls-s-1 button-text2"  style="position:absolute; top:283.00001525878906px; left: 65px; display:block; slidedirection:bottom;  durationin:4000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0">
                        <a style=""> Grab it now </a>
                        </div>  
                    </div>        
                    
                    <div class="ls-layer"  style="slidedirection:right; slidedelay:6000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition3d:all;">
                    
                        <img alt="mobile app development" src="images/slider/tuuli_media.jpg" class="ls-bg">
                        <img alt="web application development" class="ls-s-1"  src="images/slider/seo-banner.png"  style="position:absolute; top:20px; left:590px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeOutBack; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <p class="ls-s-1" style="position:absolute; top:141px; left:20px; slidedirection:fade;  durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;  width:420px; height:228px; background:#ffffff;  white-space:nowrap;">&nbsp; </p>
                        <h3 class="ls-s-1" style="position:absolute; top:158px; left:42px; slidedirection:top;  durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> SEO</h3>
                        <p class="ls-s-1 text1" style="position:absolute; top:200.00001525878906px; left:43px; slidedirection:right;  durationin:2500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; width:380px; ">At Divyaltech we understand how essential online visibility is for the growth of companies in today’s tough economic situation. Lets Divyaltech help your biz grow and aid you with all your search engine ranking needs. </p>
                        
                        <div class="ls-s-1 button-text"  style="position:absolute; top:321.00001525878906px; left:43px; display:block; slidedirection:bottom;  durationin:3000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0">
                        <a style=""> Buy Now </a>
                        </div>        
                    </div>
                    
                    <div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; timeshift:0; transition2d:all; ">
                    
                        <img alt="divyal technology development" src="images/slider/service_slider.jpg" class="ls-bg">
                        <img alt="website development in coldfusion" class="ls-s-1"  src="images/slider/pic3.png"  style="position:absolute; top:25px; left:75px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <h2 class="ls-s-1" style="position:absolute; top:147.00001525878906px; left:585px; slidedirection:top;  durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;">HTML5 App Development</h2>
                        <h4 class="ls-s-1" style="position:absolute; top:196.00001525878906px; left:585px; slidedirection:right;  durationin:2500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> With a Clean & Modern design </h4>
                        <p class="ls-s-1 text2" style="position:absolute; top:238.00001525878906px; left:585px; slidedirection:bottom;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> Want to Developing Rich Immersive native applications  using HTML 5/CSS 3 on mobile & desktop browsers?Try Html5 Development.Divyal Technology has highly experience that will create high performing, feature-packed HTML5-based web and mobile experiences. </p>                     
                    </div>
                  
                    <div class="ls-layer"  style="slidedirection:right; slidedelay:4000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition3d:all; ">
                    
                        <img alt="divyaltech web development" src="images/slider/page2-img6_small.jpg" class="ls-bg">
                        <img alt="php web development" class="ls-s-1"  src="images/slider/css.png"  style="position:absolute; top:168px; left:17px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <img alt="coldfusion web development" class="ls-s-1"  src="images/slider/html.png"  style="position:absolute; top:169px; left:232px; slidedirection:right;   durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <img alt="divyal Technology development" class="ls-s-1"  src="images/slider/prog-lang.png"  style="position:absolute; top:5px; left:447px; slidedirection:bottom;   durationin:4000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <!--img alt="layer slider" class="ls-s-1"  src="http://placehold.it/328x27.png/000000"  style="position:absolute; top:46px; left:61px; slidedirection:top;   durationin:3000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; "-->
                        <h5 class="ls-s-1" style="position:absolute; top:85px; left:114px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> WANT TO HIRE DEDICATED DEVELOPERS</h5>
						<!--<div style="position: absolute; top: 85px; width: auto; line-height: 29px; padding: 0px; border-width: 0px; transform: rotate(0deg) scale(1, 1); opacity: 1; visibility: visible; margin-left: 0px; margin-top: 0px;height: 154px; left: 46.5px;" class="ls-s-1"> 
						  <h5 class="ls-s-1" style="color:green;padding:12px;">WANT TO HIRE DEDICATED DEVELOPERS</h5>
						  <p style="color:black;background-color:white; margin-left: 15px;padding: 13px;width: 89%">Test fgdfg dfgdfg dfgdfgdfgdfdfg dfgdf dfg </p>
						</div>	         -->           
                    </div>
                        
                        
                </div>
            </div>
        </section><!-- **Slider Section - End** -->
        
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        	<div class="intro-text type1">
            	<h4> Welcome to our <span class="highlight"> Divyal Technology</span> - Software and Web Application Development Company</h4>
                <h6> We offer you the best Secure & Scalable IT Solutions on time and are well proficient with the latest technologies to breathe in the modern business space </h6>
            </div>
            
            <div class="hr-invisible-small"> </div>
            
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-cogs"> </span> </div>
                    <h5> <a href="" title=""> Great Support </a> </h5>
                    <p>Our technical support team is available for 24/7/365 and provides services to enhance your productivity </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-leaf"> </span> </div>
                    <h5> <a href="" title="">Digital Marketing </a> </h5>
                    <p> Digital marketing has approached business intelligence to discover the right strategy and we act as a proficient digital marketing agency and provide our clients better structural digital marketing platform </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5> <a href="" title="">Web Development </a> </h5>
                    <p> We offer web solutions to our clients which are custom-made to achieve their respective goals, vision and mission </p>
                </div>
            </div>
            <div class="column one-fourth last">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-laptop"> </span> </div>
                    <h5> <a href="" title=""> Fully Responsive </a> </h5>
                    <p>At Divyaltech, We take your business to next level by focusing on both desktop & mobile traffic with responsive web design for a better user experience </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>
            
            <div class="border-title"> <h2> Latest from Portfolio <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/add1.JPG" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/add1.JPG" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/add2.JPG" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/contact.JPG" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/addictive.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet2.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/add5.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Addictive Polls</a> </h5>
                            <p> www.addictivepolls.com </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/mobile2.png" alt="wordpress development" title="">
                            <div class="image-overlay">
                                <a href="images/econo.JPG" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/localcars.jpg" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/reservation.jpg" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/localcars.jpg" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/mobile2.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab3.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab2.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Econo Auto Rental </a> </h5>
                            <p> www.temp.econoautorent.com </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="coldfusion website application" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="website development in php" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="web app development" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="php web development" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!--**Portfolio Carousel Wrapper - End** -->
            
            <!-- <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> From the blog <span> </span> </h2> </div>
            
            <div class="column one-half">
                <!-- **Blog Entry** -->
               <!-- <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="divyal Technology website" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            /*<div class="categories"> <span class="icon-pushpin"> </span> <a href="" title=""> Business</a>, <a href="" title=""> Outdoors </a> </div>-->
                       <!-- </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
         <!--   </div>
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
          <!--      <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="web app development" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
            <!--</div> -->
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> FAQ's <span> </span> </h2> </div>
                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set faq">
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to center align a button? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to enable different types of homepage from backend? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to insert an image in the blog? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to set up slider? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to disable comments on pages? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                </div> <!-- **Toggle Frame Set - End** --> 
                
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Testimonials <span> </span> </h2> </div>
                
                <div class="testimonial">
                    <div class="author">
                        <img src="images/text2image.jpg" alt="divyal Technology website" title="">
                    </div>                       
                    <blockquote>
                    	<q> I tested Divyal Technologies and every time I had issues, I got almost immediate responses.  With a 12 hour time difference, I was truly skeptical.  I don’t know how it’s done, but I get responses within minutes, not hours, relieving my stress.I am so pleased with the work.


</q>
                        <cite> Marc L.<br>
                            Online Technologies </cite>
                    </blockquote>
                </div>
                <div class="testimonial">
                    <div class="author">
                        <img src="http://placehold.it/118x118.jpg" alt="divyaltech website development" title="">
						
                    </div>                       
                    <blockquote>
                    	<q> Nunc at pretium est curabitur commodo leac est venenatis egestas sed aliquet auguevelit </q>
                        <cite> - Nullam dignissim </cite>
                    </blockquote>
                </div>
            </div>
            
            <!--<div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Partners <span> </span> </h2> </div>
            
            <!-- **Partner Carousel Wrapper** -->
            <!--<div class="partner-carousel-wrapper">
                <ul class="partner-carousel">
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="Divyaltech web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="seo and social media marketing" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="ecommerce website design" title=""> </a> </li>
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="wordpress web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="cold fusion website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="website development in codeigniter" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php and coldfusion web development" title=""> </a> </li>
                </ul>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div><!-- **Partner Carousel Wrapper - End** -->
            
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

<!-- Layer Slider -->
<script src="js/jquery-easing-1.3.js"></script>
<script src="js/jquery-transit-modified.js"></script>
<script src="js/layerslider.transitions.js"></script>
<script src="js/layerslider.kreaturamedia.jquery.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery('#layerslider').layerSlider({
			skinsPath : 'layerslider-skins/',
			skin : 'fullwidthdark',
			thumbnailNavigation : 'hover',
			hoverPrevNext : false,
			responsive : false,
			responsiveUnder : 1120,
			sublayerContainer : 1060,
			width : '100%',
			height : '500px'
		});
	});		
</script>		



</body>
</html>
