<div class="technology">
    <div class="title"><strong>Technologies We Work On</strong></div>
    <ul class="custom-menu" style="list-style-type:none;padding-top:5px;padding-left:5px;">
        <li class=""><a href="coldfusion-development.php">Coldfusion Development</a></li>
        <li class=""><a href="php-development.php">PHP Development</a></li>
        <li class=""><a href="cnet.php">C# Developer</a></li>  
        <li class=""><a href="codeignitor-development.php">Codeignitor Development</a></li>
        <li class=""><a href="mobile-development.php">Mobile Application Development</a></li>
        <li class=""><a href="desktop-application.php">Desktop Application development</a></li>
        <li class=""><a href="web-application.php">Web Application Development</a></li>
        <li class=""><a href="server-and-web.php">Server and website support</a></li>
        <li class=""><a href="wordpress-development.php">WordPress Development</a></li>
        <li class=""><a href="joomla-development.php">joomla Development</a></li>
    </ul>
</div>