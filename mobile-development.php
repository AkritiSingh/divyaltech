<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Mobile Development </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong>Mobile Application Development</strong></span></h6>
                <p>Mobile Application is no more an option, however has turned into a <b>NECESSITY</b>.It is completely clear 
                  that the interest of mobile applications has raised in all organizations.To adapt to that, we turn into your 
                  quickening agent to design and develop splendid mobile applications.<b>Divyaltech</b> is a first mobile 
                  application advancement company offering portable application improvement administrations for iPhone, iPad and 
                  Android.
                   <img style="max-width:30%;" title="App Development" alt="App Development" src="images/mobaap3.jpg">
               </p>
              <p><strong>PhoneGap</strong> is an open source development framework for building mobile applications across 
                multiple devices.Looking at the rising market of cross mobile compatible applications, we started providing same 
                services to our clients to save their cost and time. Cross platform development tools are progressively more 
                becoming popular each day.</p>
			 <p>One of the most admired of these cross platform framework is Phonegap. It is used by many cross platform app 
              development experts and gives huge results. However, along with the advantages, it also come with its own share of 
              limitations.
             </p>
          </div>
           <div class="da-cantent"> 
            <p><strong>Divyal Technology</strong> provide you PhoneGap Development services to build your app to by expertise 
            PhoneGap Developer.Our team of mobile application developers is innovative and educated to perform your individual 
            requests and your business needs. With advanced tools and technology our portable applications engineers can make 
            very redid mobile applications for consumer needs and ventures. We offers portable application development services 
            for iPhone, iPad and Android.
            </p>         
            <!-- MENU-LOCATION=NONE --><br><br>
           <h6><span style=" font-size: 27px;color: #464646;"> Why Do You Choose Us? </span></h6>
            <ul>
					<li>	Profound knowledge of frameworks and mobile technologies.</li>
                    <li>	Reputed and Recognized for developing robust mobile apps</li>
					<li>	Time bound delivery and cost effective services</li>
                    <li>	Provide dynamic services to various platforms</li>
                  	<li>	High Quality app development process</li>
				    <li>	Experienced Personnel</li>
					
           	</ul>        
         </div>
           <br>
           <br>
            <div>
             <h6><span style=" font-size: 27px;color: #464646;"> Mobile Apps Development Services We Offer: </span></h6>
					<!--img src="images/cldfsn.png" alt="cold fusion"--> <br>
             
               <h6><span style=" font-size: 20px;color: #464646;">Android App Development: </span></h6>
                   <P>Be it social media, restaurants, health care, sports, education sector,and so on., we outline and build up 
                     each kind of Android Apps for cell phones and tablets. Our Android developers are energetic and proficient 
                     in making the most astounding quality mobile application for you.
                   </P>
                   
             <h6><span style=" font-size: 20px;color: #464646;"> iPhone Application Development: </span></h6>
                   <P>With the approach of iOS technological progressions, we build rich and drawing in iPhone applications that 
                      won't just make your business lucrative additionally execution situated. Our expert iPhone application 
                      developers dependably plan and create easy to use and simple to-use mobile applications.
                    </P> 
                    
              <h6><span style=" font-size: 20px;color: #464646;"> iPad App Development: </span></h6>
                   <P>With a perspective to make your iPad applications high-yielding, our experts conveys the unrivaled nature 
                      of uses. We will make your iPad applications thought best the outline by executing it in like manner. We 
                      display our tirelessness by making totally without bug iPad applications.
                   </P>     
              
                            
              <h6><span style=" font-size: 20px;color: #464646;">Windows Mobile Application Development: </span></h6>
                   <P>We turn into your configuration and improvement partners for your windows mobile application. From 
                      configuration to format, UI to UX, we conceptualize the mobile application you covet. We endeavor to 
                      manufacture wonderful applications for our customers through a streamlined application development process.
                   </P>      
              <br><br>
              
		   </div>  
          </div> 
           <div class="rightside">
            <?php include 'technology.php';?>
             <img style="max-width:30%; margin-top: 45px;" title="App Development" alt="App Development" src="images/mobapp.jpg">
             <img style="max-width:30%;margin-left: 0px;" title="App Development" alt="App Development" src="images/mobapp1.jpg">
               <div class="tech" style="margin-top:65px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Technologies We Use:</span> </h6>
                  <ul>
                    <li>HTML5, CSS3, JavaScript</li>
                    <li>jQuery Mobile</li>
					<li>Android development</li>
					<li>iOS development</li>
					<li>Web-based mobile development</li>
				  </ul>      
               </div>
               <div class="category" style="margin-top:70px;">
                <h6><span style=" font-size: 27px;color: #464646;">Categories in which our team has developed mobile applications: </span></h6>   
              <ul>
                <li>Health & Fitness Mobile Apps</li>
                <li>Music & Audio Mobile Apps</li>
                <li>News & Magazines Mobile Apps</li>
                <li>Books & Reference Mobile Apps</li>
                <li>Business Mobile Apps</li>
                <li>Education Mobile Apps</li>
                <li>Entertainment Mobile Apps</li>
                <li>Finance Mobile Apps</li>
                <li>Media & Video Mobile Apps</li>
              </ul>    
              </div>
             </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
