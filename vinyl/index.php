<?php $current = 'home';
include('navigation.php');?>
<div class="middle_banner">               
           
<div class="featured_slider">
      	<!-- begin: sliding featured banner -->
         <div id="featured_border" class="jcarousel-container">
            <div id="featured_wrapper" class="jcarousel-clip">
               <ul id="featured_images" class="jcarousel-list">
                  <li><img src="images/slider.png" width="965" height="280" alt="" /></li>
                  <li><img src="images/slider2.png" width="965" height="280" alt="" /></li>
                  <li><img src="images/slider_photo2.jpg" width="965" height="280" alt="" /></li>
               </ul>
            </div>
            <div id="featured_positioner_desc" class="jcarousel-container">
               <div id="featured_wrapper_desc" class="jcarousel-clip">
                  <ul id="featured_desc" class="jcarousel-list">                 
                     <li>
                        <div>
                           <p>Vinyl Acetate
                           </p>
                        </div>
                     </li> 
                     <li>
                        <div>
                           <p>Vinyl Acetate Polymer
                           </p>
                        </div>
                     </li> 
                     
                     
                  </ul>
               </div>

            </div>
            <ul id="featured_buttons" class="clear_fix">
               <li>1</li>
               <li>2</li>
               <li>3</li>
                            
            </ul>
         </div>
         <!-- end: sliding featured banner -->
  </div>
 </div>
<div class="center_content">   
        <div class="home_section_left">
            <img src="images/icon1.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <a href="about.php" id="about"><h2 class="home_title">About Us</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                <img src="images/home_section_thumb1.jpg" alt="" title="" border="0">
                </div>
                <p><span>About Vinyl Acetate</span><br>
                
Find out more about vinyl acetate, products, etc.
 
                <br> <br>
                <span>About the Vinyl Acetate Council</span><br>
Find out more about the VAC and its mission and members.                </p>
                <a href="about.php" class="more"><img src="images/more.gif" alt="" title="" border="0"></a>
        <div class="clear"></div>
        </div>
        
        
        <div class="home_section_left">
            <img src="images/icon2.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <a href="health.php" id="about"><h2 class="home_title">Health & Safety</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                <img src="images/home_section_thumb2.jpg" alt="" title="" border="0">
                </div>
                <p><span>Health
                </span><br>
                Find out more about…
                <br> <br>
                <span>Safety</span><br>
                Request the Safe Handling Guide. 
                <br /><br />
                </p>
                <a href="health.php" class="more"><img src="images/more.gif" alt="" title="" border="0"></a>
                <div class="clear"></div>
        </div>
        
        <div class="home_section_left">
            <img src="images/icon3.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <a href="regulatory.php" id="about"><h2 class="home_title">Regulatory Activity</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                <img src="images/home_section_thumb3.jpg" alt="" title="" border="0">
                </div>
                <p><span>North America
                 </span><br>
                 Find out more about vinyl acetate, products, etc.
                <br> <br>
                <span>Europe, Asia, etc.</span><br> 
                Find out more about the VAC and its mission and members.
                </p>
                <a href="regulatory.php" class="more"><img src="images/more.gif" alt="" title="" border="0"></a>
          <div class="clear"></div>
          </div>
           <div class="clear"></div>
             <div class="home_section_leftn">
                                      
                <a href="global.php" id="about"><h2 class="home_title">Global</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                <img src="images/p1.jpg" alt="" title="" border="0">
                </div>
                <div class="clear"></div>
              </div>
              
              <div class="home_section_leftn">
                                      
                <a href="library.php" id="about"><h2 class="home_title">Library</h2></a>
                <div class="home_subtitle"></div>
               <div class="home_section_thumb">
                <img src="images/p2.jpg" alt="" title="" border="0">
                </div>
               
               <div class="clear"></div>
              </div>
              
              <div class="home_section_leftn">
                                    
                <a href="links.php" id="about"><h2 class="home_title">Links</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                <img src="images/p3.jpg" alt="" title="" border="0">
                </div>
               <div class="clear"></div>
              </div>
              
               <div class="home_section_leftn">
                                      
                <a href="contact.php" id="about"><h2 class="home_title">Contact us</h2></a>
                <div class="home_subtitle"></div>
    
                <div class="home_section_thumb">
                  <h3 style="border: 1px solid black;padding: 5px;">join our mailing list</h3>
                  <a href="contact.php" id="about"><h2 class="home_title">Sing Up</h2></a>
                </div>
               <div class="clear"></div>
              </div>
              
<?php include('footer.php');?>
