<?php
$current = 'library';
include('navigation.php');?>
	
	<!--- Page text begins here --->
	<div class="center_content_pages">
      
        <div class="left_content" style="width:500px">
        
        	<div class="left_block">
            	<h2>Library</h2>
            </div>
      
				<div class="block_wide" style="width:462px">
					<h2>Reports</h2>
					<h2>The Vinyl Acetate Safe Handling Guide </h2>
					<p>
					The Vinyl Acetate Council developed the Vinyl Acetate Safe Handling Guide to promote the safe and responsible use of vinyl acetate. The Guide was developed based on procedures recommended by VAC members and reflects best practices on the safe handling, storage and transport of vinyl acetate. The Guide is intended for properly trained workers who may come in contact with the vinyl acetate monomer, including individuals responsible for plant safety as well as other safety and health professionals. Always consult your supplier's safety data sheet, product label and other safe handling instructions for the most current recommendations.
					<br><br>

					To promote the use of best practices around the globe, the Vinyl Acetate Safe Handling Guide is available in the following languages: .
					</p>
			 
					
				</div>
            
           </div>
		   <div class="right_block" style="margin-top: 95px;padding-right: 12px; white-space: normal; width: 435px; word-wrap: break-word;">
		   <h2>Vinyl Acetate and the Toxics Release Inventory (TRI) </h2>
					<p>
           The Toxics Release Inventory (TRI) is an annual survey that the U.S. EPA conducts on the release and disposal of chemicals.     
           Vinyl Acetate is one of the many substances listed on the TRI list and both manufacturers and users of vinyl acetate must report           TRI information to U.S. EPA.

            Not every pound reported under TRI is the same.  TRI captures pounds that are both released into the environmental and also            sent to waste management.  It’s important to understand this distinction when reviewing TRI information about a chemical or            facility. 

             The goal of the Toxics Release Inventory program is to provide communities with information about toxic chemical releases and             waste management activities and to support informed decision making at all levels by industry, government, non-governmental             organizations, and the public.
<br><br>

				
					</p><br><br>
		   </div>
       	<!--- Page text ends here --->

	</div>
		
		<?php include('footer.php');?>
	</div>
</div>
</body>
</html>


