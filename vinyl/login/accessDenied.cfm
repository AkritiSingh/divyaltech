<!--- BEGIN Site Header --->
<cfset pagetitle = "Access Denied">
<cfset whichsection = "MO">
<cfinclude template="../navigation.cfm">
<!--- END Site Header --->

<link type="text/css" rel="stylesheet" href="/css/login.css" />

<h3>Access Denied</h3>

<p>We're sorry, but your login does not have access to the page you requested.</p>

<cfinclude template="../footer.cfm" />