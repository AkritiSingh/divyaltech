<?php 
$current = 'links';
include('navigation.php');?>
	
	<!--- Page text begins here --->
	<div class="center_content_pages">
      
        <div class="left_content" style="float: left;padding: 2px 2px 2px 20px;width: 98%;">
        <H2>Links</H2>
		        
		<B style="color: #b52025;">Additional Sources of Information on Vinyl Acetate</B><P>
		
		<UL>
			<a href="http://hazmat.dot.gov/pubs/erg/erg2008_eng.pdf" target="_blank">2008 Emergency Response Guidebook</a><p>

			<a href="http://www.epa.gov/oppt/aegl/pubs/rest143.htm" target="_blank">Acute Exposure Guideline Levels Proposed by EPA for Vinyl Acetate</a><p>

			<a href="http://www.atsdr.cdc.gov/tfacts59.html" target="_blank">Agency for Toxic Substances and Disease Registry (ATSDR) ToxFAQs<SUP>&#153;</SUP> for Vinyl Acetate</a><P>

			<a href="http://www.petrochemistry.net/acetyls.html" target="_blank">CEFIC Acetyls Sector Group (ASG)</a><P>

			<a href="http://www.epa.gov/iris/" target="_blank">EPA's Integrated Risk Information System</a><BR> 
			NOTE: EPA is in the process of updating its IRIS file on vinyl acetate.  A revised version is expected.<P>

			<a href="http://www.epa.gov/emergencies/index.htm" target="_blank">EPA Emergency Management</a><p>

		<a href="http://ecb.jrc.it/" target="_blank">European Chemicals Bureau</a><p> 

		<a href="http://www.hc-sc.gc.ca/hecs-sesc/whmis/index.htm" target="_blank">Health Canada's Workplace Hazardous Materials Information System (WHMIS)</a><P>

			<a href="http://www.iarc.fr/" target="_blank">International Agency for Research on Cancer</a><P>
			
		</UL><P><BR>
		<B style="color: #b52025;">Allied Associations</B><P>
		<ul>
			<a href="https://plastics.americanchemistry.com/" target="_blank">The American Plastics Council</a><p>
			<a href="http://www.apme.org/" target="_blank">Association of Plastic Manufacturers in Europe (APME)</a><p>
			<a href="http://www.ccpa.ca/" target="_blank">Canadian Chemical Producers Association</a><p>
			<a href="http://www.ccpa.ca/" target="_blank">Canadian Plastics Industry Association</a><p>
			<a href="http://www.cspa.org/" target="_blank">Consumer Specialty Products Association</a><p>
			<a href="http://styrene.org/" target="_blank">SIRC</a><p>
			<a href="http://www.plasticsindustry.org/" target="_blank">The Society of the Plastics Industry</a><p>
			<a href="http://youknowstyrene.org/" target="_blank">You Know Styrene</a><p>
		</ul><br><br>
        
            <B style="color: #b52025;">Other</B><P>	
                <ul>
                    <a href="http://www.inda.org/" target="_blank">Association of the Nonwoven Fabrics Industry (INDA)                   </a><p>
                    <a href="http://www.bpf.co.uk/" target="_blank">British Plastics Federation</a><p>
                    <a href="http://www.accctquebec.ca/" target="_blank">Canadian Association of Textile Colourists and                     Chemists (CATCC)</a><p>
                    <a href="http://cpda.com/" target="_blank">Chemical Producers & Distributors Association (CPDA)</a>                    <p>
                    <a href="http://www.cler.com/" target="_blank">Council for LAB/LAS Environmental Research (CLER)</a>                     <p>
                </u>
                
                <br><br>
                
        <B style="color: #b52025;">Vinyl Acetate Council Member Companies</B><P>
		<img src="images/celance.JPG" alt="" title="" border="0" class="logo"width="110px" height="40px"/>
	    <img src="images/dow.jpg" alt="" title="" border="0" class="logo"/>
		<img src="images/dupont.jpg" alt="" title="" border="0" class="logo" width="120px" height="30px"/>
	    <img src="images/lyondell.jpg" alt="" title="" border="0" class="logo"/>
	    
		<br><br>
		<B style="color: #b52025;">Associate Member Companies</B><P><BR>
		<img src="images/wacker.JPG" alt="" title="" border="0" class="logo" width="120px" height="30px"/>
		<br><br>
       	<!--- Page text ends here --->

	</div>
		
		<?php include('footer.php');?>
	</div>
</div>
</body>
</html>


