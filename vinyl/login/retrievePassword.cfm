<cfparam name="form.email" default="" />

<cfset request.errorMessage = "" />
<cfset request.successMessage = "" />

<cfif len(form.email)>
	<cfset request.loginRecord = application.loginService.getLoginByEmail(form.email) />
	
	<cfif request.loginRecord.recordcount EQ 1>
		<cfset request.successMessage = "Your login and password have been sent to the email addresss you provided." />
	<cfelse>
		<cfset request.errorMessage = "We could not locate your record.  Please contact us for assistance." />
	</cfif>
</cfif>

<!--- BEGIN Site Header --->
<cfset pagetitle = "Member Login">
<cfset whichsection = "MO">
<cfinclude template="../navigation.cfm">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
<!--- END Site Header --->

<link type="text/css" rel="stylesheet" href="/css/login.css" />

<h3>Password Retrieval</h3>

<cfif len(request.errorMessage)>
	<div class="ui-state-error ui-corner-all messageBox">
		<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span>
		<cfoutput>#request.errorMessage#</cfoutput>
	</div>
</cfif>

<cfif len(request.successMessage)>
	
	<div class="ui-state-highlight ui-corner-all messageBox">
		<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>
		<cfoutput>#request.successMessage#</cfoutput>
	</div>
	
	<cfmail from="#applciation.sitesettings.adminEmail#" to="#form.email#" subject="Login Information" type="html">
		<p style="font-family:Arial, Helvetica, sans-serif; font-size:12px">
			Below is the login information you requested.
			<br />
			<br /><strong>Username:</strong> #request.loginRecord.login#
			<br /><strong>Password:</strong> #request.loginRecord.password#
			<br />
			<br />Thank you
		</p>
	</cfmail>
<cfelse>

	<p>To retrieve your password, enter the primary email address associated with your company record.  We will send your login and password to the email address.</p>
	<p>If you cannot remember your email address, please contact us for assistance.</p>
	
	<div id="loginForm">
		<form method="post">	
			<div id="loginFormInnerContainer">
				<div class="loginFormLabel"><label for="login">Email Address:</label></div>
				<div class="loginFormInput"><input id="email" name="email" type="text"></div>
							
				<div class="loginFormLabel">&nbsp;</div>
				<div class="loginFormInput"><input type="submit" value="Retrieve" /></div>
				
				<div class="clear"></div>
			</div>
		</form>	
		<div class="clear"></div>
	</div>
</cfif>
	
</td></tr></table></td>

<cfinclude template="../footer.cfm">

