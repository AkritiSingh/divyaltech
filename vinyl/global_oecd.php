<?php include('navigation.php');?>

<IMG SRC="images/hdr-gu.jpg" WIDTH=382 HEIGHT=63 ALT="Global Update">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->




<h3>Global Update</h3>

<h4>Organization for Economic Co-operation and Development</h4>

<i>OECD Investigation of Existing Chemicals</i>

<blockquote>
Germany and the United States are co-sponsoring vinyl acetate for review under the OECD Co-operation on the Investigation of Existing Chemicals.
<p>
In February 2003, Germany offered its preliminary draft EU risk assessment for comment to the OECD member countries; among others, the United States offered comments.
<p>
<a href="oecd_vam_22Feb05.pdf" target="_blank">Click here for the latest official status of vinyl acetate under the OECD program</a>, available from the <a href="http://cs3-hq.oecd.org/scripts/hpv/" target="_blank">OECD HPV database site</a>.
<p>
VAC and ASG intend to actively participate in the OECD review of vinyl acetate.

</blockquote>

	<!--- Page text ends here --->

</td></tr></table></td>

<?php include('footer.php');?>
