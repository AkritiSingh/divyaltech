<cfcomponent>
	
	<cfset this.sessionManagement  = true />
	<cfset this.clientManagement   = false />
	<cfset this.setClientCookies   = true />
	<cfset this.sessionTimeout     = createTimeSpan(0,4,0,0) />
	<cfset this.applicationTimeout = createTimeSpan(0,4,0,0) />
	<cfset this.name               = "VINYLACETATE_MAIN_SITE" />
    
    <cffunction name="onApplicationStart" returntype="boolean" output="false">
		
		<!--- Determine the path to the folder containing the site root htdocs folder (i.e. one up from root) --->
		
		<cfset currentTemplatePath = getCurrentTemplatePath() />
		
		<cfset oneUpFromRoot = "" />

		<cfloop list="#currentTemplatePath#" index="subFolderName" delimiters="\/">
			<cfif subFolderName NEQ "htdocs">
				<cfset oneUpFromRoot &= subFolderName & "\" />
			<cfelse>
				<cfbreak />
			</cfif>
		</cfloop>
					
		<!--- Instantiate Services --->
		
		<cfscript>
		
			// Site Settings
			application.siteSettings = structNew();
			structInsert(application.siteSettings, "rootFolder",                "#oneUpFromRoot#htdocs");
			structInsert(application.siteSettings, "baseFolder",                "#oneUpFromRoot#");
			structInsert(application.siteSettings, "devFilepath",               "#oneUpFromRoot#logfiles_dev");
			structInsert(application.siteSettings, "prodFilepath",              "#oneUpFromRoot#logfiles_prod");
			structInsert(application.siteSettings, "documentUploadDir",  		"#oneUpFromRoot#secureDocuments");
			structInsert(application.siteSettings, "adminEmail",                "info@vinylacetate.org");
			structInsert(application.siteSettings, "devEmail",  				"podidev@podi.com");
			structInsert(application.siteSettings, "customerHTMLEmailTemplate", "/emailTemplates/customerHTMLEmail.cfm");
			structInsert(application.siteSettings, "customerTextEmailTemplate", "/emailTemplates/customerTextEmail.cfm");
			structInsert(application.siteSettings, "clientHTMLEmailTemplate",   "/emailTemplates/clientHTMLEmail.cfm");
			structInsert(application.siteSettings, "clientTextEmailTemplate",   "/emailTemplates/clientTextEmail.cfm");
			structInsert(application.siteSettings, "membershipDSN",             "DSN_FOR_SITE_MEMBERS");
			structInsert(application.siteSettings, "documentsDSN",              "DSN_FOR_SITE_DOCS");
			structInsert(application.siteSettings, "devFormLogsDSN",            "vinylacetate_FORM_LOGS_TEST");
			structInsert(application.siteSettings, "prodFormLogsDSN",           "vinylacetate_FORM_LOGS");
			//structInsert(application.siteSettings, "fuseGuardDSN", "YOUR_SITE_FUSEGUARD", true);

			// set date/time of last successful application load/restart
			structInsert(application.siteSettings, "lastApplicationLoad",  		"#dateFormat(Now(),"mm/dd/yyyy")# #timeFormat(Now(),"hh:mm:ss tt")#",	false);
			
			//For Activity Folder
			StructInsert(application.siteSettings, "siteDomain", "mysite.org");
			
			//set up restricted access
			userNameStruct = structNew();
			//structInsert(userNameStruct,   "siteStaff", "siteStaffPassword", true);
			//structInsert(userNameStruct,   "siteStaff2", "siteStaff2Password", true);
			structInsert(userNameStruct,   "blosey", 		"vinyl2012", true);
			structInsert(userNameStruct,   "smeuller", 		"vinyl2012", true);
			structInsert(userNameStruct,   "vinylstaff",	"vinyl2012", true);
			structInsert(userNameStruct,   "vinylguest",	"vinyl2012", true);
			
			userAccessStruct = structNew();
			//structInsert(userAccessStruct, "siteStaff",	"members", "No");
			//structInsert(userAccessStruct, "siteStaff2", "members,update", "No");
			structInsert(userAccessStruct, "blosey",	"members,update", "No");
			structInsert(userAccessStruct, "smeuller",	"members,update", "No");
			structInsert(userAccessStruct, "vinylstaff","members,update", "No");
			structInsert(userAccessStruct, "vinylguest","members", "No");
		
			structInsert(application.siteSettings, "userNameStruct",   userNameStruct, true);			
			structInsert(application.siteSettings, "userAccessStruct",  userAccessStruct, true);		
			
			
			// Load Portcullis Settings
			noscrubList = "";
			portcullisSettings = StructNew();
			structInsert(portcullisSettings , "exceptionFields", noscrubList, false);			
			structInsert(application.siteSettings, "portcullisSettings",   portcullisSettings, true);	
			
			// Create Parent Bean Factory
			application.parentBeanFactory = createObject("component", "coldspring.beans.DefaultXmlBeanFactory").init(StructNew(),application.siteSettings);
			
			// Load Podi Form Tools
			application.parentBeanFactory.loadBeans("#oneUpFromRoot#\config\podi_tools_config.xml");
			application.toolsFactory = application.parentBeanFactory.getBean("ToolsFactory");
			
			// Load Miva Integration Service
			//application.parentbeanFactory.loadBeans("#oneUpFromRoot#\config\miva_config.xml");
			//application.mivaService = application.parentBeanFactory.getBean("mivaService");	
			
			// Load Site Configuration
			application.parentBeanFactory.loadBeans("#oneUpFromRoot#\config\vinylacetate_config.xml");	
			
			//Load Secure Document Service
			//application.uploadService = application.parentBeanFactory.getBean("documentService");				
			
			//Load File Upload Service
			application.documentService = application.parentBeanFactory.getBean("uploadService");
			
			// Login Service
			application.loginService = application.parentBeanFactory.getBean("loginService");
			
			//Instantiate Site Utilities
			application.siteUtilities = application.parentBeanFactory.getBean('siteUtilities');
			
			//Load Portcullis (WAF)
			application.portcullis = application.parentBeanFactory.getBean("portcullis");

			// Load FuseGuard
			//application.parentBeanFactory.loadBeans('#oneUpFromRoot#config\fuseGuard_config.xml');
			//application.fuseguard = application.parentBeanFactory.getBean("fuseGuard");
			
			//application.fuseGuard_podiAuthenticator = application.parentBeanFactory.getBean("fuseGuard_podiAuthenticator"); // Custom authentication for FuseGuard manager
			//application.fuseguard.setAuthenticator(application.fuseGuard_podiAuthenticator);
			
		</cfscript>
		
		<cfreturn true />
		
    </cffunction>
	
    
    <cffunction name="onSessionStart" returntype="boolean" output="false">
	
    	<cfscript>
		
			// Intialize debug mode
			session.debugMode = false;
		
		</cfscript>
        
		<cfreturn true />
		
    </cffunction>
	
    
    <cffunction name="onRequestStart" returntype="void" output="false">

    	<!--- Check for debug mode override key --->	
		<cfif isDefined("URL.podiSetDebug") AND isBoolean(URL.podiSetDebug)>
			<cfset session.debugMode = URL.podiSetDebug />
		</cfif>
		
		<!--- Setup error handlers (request handler must be first) --->				
		<cfif isDefined("session.debugMode") AND  NOT session.debugMode>
			<cferror type="request" template="errorHandlers/error.cfm" />
		</cfif>
				
		<cferror type="exception" template="errorHandlers/exception.cfm" exception="any" />
		
		<!--- Check for application reload key --->		
		<cfif isDefined("URL.podiResetApp") AND (URL.podiResetApp EQ "true")>
			<cflock scope="application" type="exclusive" timeout="10">
				<cfset this.onApplicationStart() />
			</cflock>
		</cfif>
		
		<!--- Reset session --->		
		<cfif isDefined("URL.logout") AND (URL.logout EQ "true")>        
        	<cfset application.loginService.killLoginSession() />
			<cfset session = JavaCast( "null", 0 ) />
			<cfset this.onSessionStart() />
			<cflocation url="/" addtoken="no" />
		</cfif>	
		
		<!--- Dump portcullis block log --->
		<cfif isDefined("URL.podiViewPortCullis") AND URL.podiViewPortCullis EQ "true">
			<cfdump var="#application.portcullis.getLog()#" label="Portcullis block Log" />
		</cfif>
		
		<!--- Allow for reseting IP address block from URL for testing only --->		
		<cfif isDefined("URL.podiResetPortCullis") AND (URL.podiResetPortCullis EQ "true")>
			<cfset application.portcullis.removeIPFromLog(cgi.remote_addr) />
		</cfif> 
		
		<!--- Use Portcullis to scan CF scopes containing user-supplied data --->
			
		<cfif isDefined("url")>
			<cfset application.portcullis.scan(url, "url", cgi.remote_addr) />
		</cfif>
		
		<cfif isDefined("form")>
			<cfset application.portcullis.scan(form, "form", cgi.remote_addr) />
		</cfif>
		
		<cfif isDefined("cookie")>
			<cfset application.portcullis.scan(cookie, "cookie", cgi.remote_addr) />
		</cfif>
		
		<!--- If we have blocking on, call Portcullis to determine if a request should be blocked --->		
		 <cfif application.portcullis.isBlocked(cgi.remote_addr) AND NOT session.debugMode>
			<cfthrow type="portcullis.blocked" message="Request blocked" detail="This request was terminated due to an IP block in the application firewall." />
		</cfif>
		
		<!--- FuseGuard Web Application Firewall (alternative to Portcullis) --->
		<!---<cfset application.fuseguard.processRequest(abort=true) />--->
		
	</cffunction>
	
</cfcomponent>