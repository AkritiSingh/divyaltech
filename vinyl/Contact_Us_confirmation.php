 <?php 
 $current = 'confirm';
 include('navigation.php');?>
        
	<div class="center_content_pages">
        
         <div class="financial-application-form">
				<h2>Contact Us Confirmation</h2>

	<?php
	date_default_timezone_set('Asia/Calcutta');
	?>
	
				<h3>Thank You for Your Inquiry</h3>				
				<p>Thank you for submitting a Contact Us form.<br/>				
	 			You will receive a detailed email confirmation shortly. Please find the summary of your request below:</p>
		</div>
	
	<div class="form">
		<table border="0" cellpadding="5" width=100%><tr>
			<td><strong>Name:</strong></td>
			<td><?php echo $_REQUEST['firstName'];?></td>
		</tr><tr>
			<td><strong>Email:</strong></td>
			<td><?php echo $_REQUEST['contactEmail'];?></td>
		</tr><tr>
			<td><strong>Your phone:</strong></td>
			<td><?php echo $_REQUEST['telephone'];?></td>
		</tr><tr>
			<td><strong>Website:</strong></td>
			<td><?php echo $_REQUEST['website'];?></td>
		</tr>
		<tr>
			<td><strong>Address:</strong></td>
			<td><?php echo $_REQUEST['address'];?></td>
		</tr>
		
	
		</table>	
	
		<p style="margin-top:20px">
			<em>Date Submitted: <?php echo date('Y-m-d H:i:s');?> </em>
		</p>
	
	<!--- Page text ends here --->

</td></tr></table></td>
	</div>

	  <div class="clear"></div>
  
<?php
		 $to = $_REQUEST['contactEmail'];
         $subject = "Email confirmation";
         $message = "<b>Thank you for submitting a Contact Us form.</b>";
		 $header = "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
		 $retval = mail ($to,$subject,$message,$header);
         
         if( $retval == true ) {
            echo "<h2>Message sent successfully...</h2><br>";
         }else {
            echo "<h2>Message could not be sent...</h2><br>";
         }
?>
	</div>

	 <?php include('footer.php');?>
      
      
    
    </div>
</div>
</body>
</html>
