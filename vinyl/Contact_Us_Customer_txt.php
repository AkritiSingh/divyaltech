<?php 
$CRLF = Chr(13) & Chr(10);

echo "Thank You" .$CRLF $CRLF.
"We have received your email.  A copy of the information you sent is supplied below for verification and your records". $CRLF $CRLF
"If you have any questions or concerns please email info@vinylacetate.org." .$CRLF $CRLF.
"Name: " $attributes.firstName $attributes.lastName .$CRLF.
"Email: " $attributes.contactEmail .$CRLF.
"Title: " $attributes.title .$CRLF.
"Company: " $attributes.company .$CRLF.
"Address: " $attributes.address .$CRLF.
"City: " $attributes.city .$CRLF.
"State: " $attributes.state .$CRLF.
"Zip Code: " $attributes.zipcode .$CRLF.
"Country: " $attributes.country .$CRLF.
"Telephone: " $attributes.telephone .$CRLF.
"Fax: " $attributes.fax .$CRLF.
"Participate in Future Web Briefings: " $attributes.webbriefings .$CRLF.
"Notify me when new information is posted: " $attributes.newinfo .$CRLF.
"Add me to Canadian news distribution list: " $attributes.canadiannews .$CRLF.
"Comments or Suggestions: " $attributes.comments .$CRLF;

?>		 