<cfsilent>
<!------------------------- FILE HEADER BLOCK ---------------------------------
  ---
  --- File Name:		SITE::downloadSecureDocument.cfm
  ---
  --- Author:				Dave Hunsberger
  ---
  --- Created:			03.25.2011
  ---
  --- Purpose:			outputs a file from above the docRoot
  ---
  --- Dependencies:		
  ---
  --- Notes:			parameter for either download only, or view in browser
  ---
  --- Copyright:		2011 Potomac Digitek
  ---
  --- Revisions:
------------------------------------------------------------------------------>
</cfsilent>
<cfparam name="ot" default="view" /><!--- can also be dl --->
<cfparam name="docID" default=0 />

<cfif isNumeric(docID) AND len(docID) AND docID GT 0>
	
	<!--- get infor about file to output --->
	<cfset docInfo = application.documentService.getSingleDocument(docID) />
	
	<!--- if ONE file found --->
	<cfif docInfo.recordcount EQ 1>		
		<cfset basePath = application.siteSettings.documentUploadDir> 
		<cfset baseFileName = "#docInfo.file_Name#.#docInfo.fileExt#" />
		<cfset downloadFileName = "#docInfo.userFileName#.#docInfo.fileExt#" />
		<cfset fileSize = createObject("java","java.io.File").init("#basePath##baseFileName#").length()>
		<cfset MIMEtype = getPageContext().getServletContext().getMimeType("#basePath##baseFileName#") />
		
		<!---<cfset fileExtension = listLast(docInfo.filePath, ".")>--->
		<!---<cfset fileName = listLast(docInfo.filePath, "/")>--->
		
		<cfset dlCount = application.documentService.updateDownloadCount(docID) />
					
		<cfif ot EQ "view">		
			<!--- output file to browser --->	
			<cfheader name="Content-Disposition" value="inline; filename=#downloadFileName#">
			<cfheader name="Content-Length"  value="#fileSize#"  />
			<cfcontent type="#MIMEtype#" file="#basePath##baseFileName#" deletefile="No" reset="Yes">
			<cfabort />			
		<cfelseif ot EQ "dl">	
			<!--- force file download --->
			<cfheader name="Content-Disposition" value="attachment; filename=#downloadFileName#">
			<cfheader name="Content-Length"  value="#fileSize#"  />
			<cfcontent type="#MIMEtype#" file="#basePath##baseFileName#" deletefile="No" reset="Yes">
			<cfabort />		
		<cfelse>
			<cfthrow detail="Invalid output type" message="Invalid output type" errorcode="DOCUMENT_DOWNLOAD_ERROR" />
			<cfabort />
		</cfif>		
	<cfelse>
		<cfthrow detail="Invalid unique file query results" message="Invalid secure query results" errorcode="DOCUMENT_DOWNLOAD_ERROR" />
		<cfabort />
	</cfif>		
<cfelse>
	<cfthrow detail="Invalid docID" message="Invalid docID" errorcode="DOCUMENT_DOWNLOAD_ERROR" />
	<cfabort />
</cfif>