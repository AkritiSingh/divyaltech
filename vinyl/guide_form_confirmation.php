<!--cfparam name="URL.id"       default="" />
<cfparam name="URL.formCode" default="" />
<cfparam name="session.formCode" default="">

<cfif len(URL.id) AND len(URL.formCode)>

	<cfset submissionDetails = application.toolsFactory.getLogEntry(URL.formCode, URL.id) />
	
	<cfif len(submissionDetails.logAbsolutePath) AND fileExists(submissionDetails.logAbsolutePath)>
		<cffile action="read" file="#submissionDetails.logAbsolutePath#" variable="logFileText" />
		
		<cfif len(logFileText)>
			<cfwddx action="wddx2cfml" input="#logFileText#" output="logFileStruct" />
		</cfif>
	</cfif>
<cfelse>
	
	<cfthrow type="invalidParams" />
	
</cfif>

<cfif isQuery(submissionDetails) AND submissionDetails.recordcount EQ 1-->

<?php include('navigation.php');
date_default_timezone_set('Asia/Calcutta');
?>
<?php if(isset($_REQUEST['firstname'])) { ?>
<IMG SRC="images/hdr-vas.jpg" WIDTH=382 HEIGHT=63 ALT="Safe Handling Guide">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
		<h4>Thank You for Your Request</h4>

		<?php /*cfswitch expression="#URL.formCode#">
	
			<cfcase value="guideform"*/ ?>
				
				<p>Thank you for submitting a VAM Safe Handling Guide order form.</p>
	 			<p>You will receive a detailed email confirmation shortly. Please find the summary of your request below:</p>

		<table border="0" cellpadding="5"><tr>
			<td><strong>Name:</strong></td>
			<td><?php echo $_REQUEST['firstname'];?></td>
		</tr><tr>
			<td><strong>Email:</strong></td>
			<td><?php echo $_REQUEST['email'];?></td>
		</tr><tr>
			<td><strong>Company:</strong></td>
			<td><?php echo $_REQUEST['company'];?></td>					
		</tr><tr>
			<td><strong>Address:</strong></td>
			<td><?php echo $_REQUEST['address'];?></td>
		</tr><tr>
			<td><strong>City:</strong></td>
			<td><?php echo $_REQUEST['city'];?></td>
		</tr><tr>
			<td><strong>State:</strong></td>
			<td><?php echo $_REQUEST['state'];?></td>
		</tr><tr>
			<td><strong>Zip Code:</strong></td>
			<td><?php echo $_REQUEST['zipcode'];?></td>		
		</tr><tr>
			<td><strong>Telephone:</strong></td>
			<td><?php echo $_REQUEST['telephone'];?></td>
		</tr><tr>
			<td><strong>Fax:</strong></td>
			<td><?php echo $_REQUEST['fax'];?></td>
		</tr><tr>
			<td valign="top"><strong>How is your company associated with VAM:</strong></td>
			<td valign="top">
				<?php 
					if(isset($_REQUEST['associated'])){
						foreach($_REQUEST['associated'] as $val)
						echo $val."<br/>";
					}
					else 
						echo"not selected";
				?>
			</td>			
		</tr>
	
		</table>	
	
		<p style="margin-top:20px">
			<em>Date Submitted: <?php echo date('Y-m-d H:i:s');?></em>
		</p>
	
	<!--- Page text ends here --->

</td></tr></table></td>
<?php
		 $to = $_REQUEST['email'];
         $subject = "Email confirmation";
         
         $message = "<b>Thank you for submitting a Contact Us form.</b>";
		 $header = "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
		 $retval = mail ($to,$subject,$message,$header);
         
         if( $retval == true ) {
            echo "Message sent successfully...<br>";
         }else {
            echo "Message could not be sent...<br>";
         }

 } 
else
{
echo "There was an error processing your submission.  Please contact us for assistance.<br>";	
}
?>
<?php include('footer.php');?>
	
