<?php include('navigation.php');?>

<IMG SRC="images/hdr-vas.jpg" WIDTH=382 HEIGHT=63 ALT="Safe Handling Guide">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->



<h3>New Vinyl Acetate Safe Handling Guide - April 2010</h3>

<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
		<table><tr>
			<td width="169" valign="top"><a href="guide_preview.doc" target="_blank"><IMG SRC="/images/shg10.jpg" WIDTH=135 HEIGHT=175 border=0 hspace=5></a><br /><font size="1"><a href="guide_preview.doc" target="_blank">Click here</a> for a preview<br />of the guide.</font></td>
		</tr></table></td>
			
	<td width="100%" valign="top">The  VAC is pleased to announce that a new edition (April 2010) of the Vinyl Acetate  Safe Handling Guide is now available.&nbsp;  This new edition includes a number of updates and improvements and supersedes  all previous safe handling guides that the VAC has prepared. 
<P>
  The Vinl Acetate Council developed  the Vinyl Acetate Safe Handling Guide to promote the safe and responsible use  of vinyl acetate. The Guide was developed based on procedures recommended by  VAC members and reflects best practices on the safe handling, storage and  transport of vinyl acetate. 
  <P>
  The Guide is intended for properly  trained workers that may come in contact with vinyl acetate monomer, including  individuals responsible for plant safety as well as safety and health  professionals. <em>Always Consult Your Supplier's Safety Data Sheet, Product  Label and Other Safe Handling Instructions for the Most Current  Recommendations.</em> </td>
		</tr><tr>
			<td valign="top" colspan="2"><br /><br /><strong>How to Get the Guide</strong><P>

To obtain a full copy of the Guide please contact one of the following VAC members or request a copy from the VAC  (<a href="guideform.php">request form</a>):</td>
</tr></table>

<P>

		
		<table cellpadding=7 cellspacing=7 width=75%><tr>
			<td valign=center align=right><IMG SRC="images/celance.jpg"></td>
			<td valign=center>Celanese  Corporation<br>
Transportation  Emergency: 800-424-9300<br>
Product  Information: 800-835-5235<br>
<a href="http://www.celanese.com">www.celanese.com</a><br>
<a href="http://www.chemvip.com/index/products_index/all_products/all_products_acetyls/product-vinyl-acetate.htm">www.chemvip.com/index/products_index/all_products/all_products_acetyls/product-vinyl-acetate.htm</a></td>
		</tr><tr>
			<td valign=center align=right><IMG SRC="images/dow.jpg"></td>
			<td valign=center>The  Dow Chemical Company<br>
  North  America: 800-447-4369<br>
  Europe:  +800-3694-6367 <br>
  Asia  Pacific (except China): +800-7776-7776<br>
  China:  +800-600-0015<br>
  Other  Global Areas: 989-832-1560<br>
  <a href="http://www.dow.com/">www.dow.com</a><br>
<a href="http://www.dow.com/vam">www.dow.com/vam</a></td>
		</tr><tr>
			<td valign=center align=right><IMG SRC="images/dupont.jpg"></td>
			<td valign=center>DuPont  Packaging and Industrial Polymers<br>
  Medical Emergency: 800-441-3637<br>
  Technical Information and VAM Safe Handling Guide Access:  800-628-6208, ext 6 <br>
<a href="http://www.dupont.com/">www.dupont.com/</a></td>
		</tr><tr>
			<td valign=center align=right><IMG SRC="images/lyondell.jpg"></td>	
			<td valign=center>LyondellBasell Acetyls, LLC<br>
Contact:  713-209-7000 <br>
24-hour  Emergency Number: 800-245-4532 <br>
<a href="http://www.lyondellbasell.com/">www.lyondellbasell.com</a></td>
		</tr><tr>
			<td valign=center align=right><IMG SRC="images/wacker.jpg"></td>
			<td valign=center>Wacker Chemie AG<br>
Wacker Polymers<br>
Contact:  +49-89-6279-1646<br>
International  Emergency Information: +49-621-60-43333 <br>
 <a href="http://www.wacker.com">www.wacker.com</a></td>
		</tr></table>
	
<p>	<strong>Translations</strong><P>
To promote the use of best practices around the globe, VAC previously translated earlier editions of the Safe Handling Guide into several different languages. Translated versions of the new Vinyl Acetate Safe Handling Guide (April 2010) are being developed for Chinese, French, German, Italian, Spanish (Latin American) and Portuguese (Brazilian).  These translations should be available soon, please check this page again soon for updates.<p>

	<!--- Page text ends here --->

</td></tr></table></td>

<?php include('footer.php');?>