<cfcomponent extends="applicationproxy">

	<cffunction name="onRequestStart">
		
		<!--- Set the access code required to reach pages in this folder --->
		<cfset request.requiredAccessCode = "members" />
		
		<!--- Call the main application.cfc's onRequestMethod --->
		<cfscript>
			SUPER.onRequestStart();
		</cfscript>
		
		<!--- Verify user is logged in and has access to this event --->
		
		<cfif NOT application.loginService.isLoggedIn()>
			<cflocation url="/login/index.cfm?destURL=#urlEncodedFormat(cgi.script_name)#&destQuery=#urlEncodedFormat(cgi.query_string)#" addtoken="false" /> 	
		</cfif>
				
		<cfif NOT application.loginService.checkAccess(request.requiredAccessCode)>
			<cflocation url="/login/accessDenied.cfm" addtoken="false" />
		</cfif>
	</cffunction>
</cfcomponent>