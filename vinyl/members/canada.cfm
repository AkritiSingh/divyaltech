<cfinclude template="../navigation.cfm">

<IMG SRC="/images/hdr-mo.jpg" WIDTH=382 HEIGHT=63 ALT="Members Only">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->

<H3>Canada's Batch Two</H3>



<strong>Presentations</strong>

<UL>
	<LI><a href="presentations/060508.pdf"><strong>June 5, 2008 VAC webcast</strong></a> - provides overview of May 2008 draft assessment and raises issues VAC may focus on as it develops comments for submission by the July 16 deadline.  There is emphasis on exposure issues in this presentation, which was given to a large audience of downstream VAM users.<P>


	<LI><a href="presentations/060308pottenger.pdf"><strong>June 3, 2008</strong></a> presentation by Lynn Pottenger (Dow) on behalf of the Canadian Industry Coordinating Group (ICG) at Canadian Chemical Producers Association (CCPA) meeting with Canadian authorities.
</UL><P>


<strong>Materials Circulated to Organizations In Response to May 2008 Notice, Draft Screening Assessment and Risk Management Scope Document</strong>	
<UL>
	<LI>May 28, 2008 &mdash; <A HREF="VAC_Canada_Notice2_052908.pdf">memo</A> regarding Canada materials released May 17 and announcement of June 5 web cast <P>
	<LI>May 16, 2008 &mdash; update <A HREF="VAC_Canada_Update_Uses.pdf">memo</A> on uses expected to be identified as concerns in May 17 Screening Assessment<P>
	<LI>May 14, 2008 &mdash; <A HREF="VAC_Canada_Notice1.pdf">memo</A> and <A HREF="VAC_Canada_Notice1_Transmittal.pdf">transmittal</A> announcing anticipated May 17 release of information 
</UL><P>

 
<strong>Comments Submitted to Canada</strong>
<UL>
	<LI>July 16, 2008 &mdash; draft in progress<P>
	
	<LI>November 13, 2007 &mdash; <A HREF="VAC_Batch2_Comments.pdf">Voluntary Submission</A> in Response to May 17, 2007 Gazette Notice and draft Substance Profile <P>
</UL><P>
 
<strong>Related Media</strong>
<UL>
	<LI>May 28, 2008. <em>The Sault Star.</em> <A HREF="Sault_Star.pdf">We'll have to coin a word for our newly found fear of plastics</A>.<P>

	<LI>May 24, 2008. <em>Montreal Gazette (via Canada.com).</em>  <A HREF="Canada_Chew.pdf">Vinyl acetate toxic? Just chew on the facts</A>.<P>	 

	<LI>May 21, 2008. <em>Chemical & Engineering News.</em> <A HREF="Canada_Moves.pdf">Canada Moves Against Four Chemicals &ndash; Proposed regulation is based on toxicity concerns</A>.<P>	 

	<LI>May 19, 2008. <em>Cosmeticsdesign.com.</em> <A HREF="Cosmetics.pdf">Canada proposes personal care ingredients for toxic list</A>.<P>	 

	<LI>May 17, 2008. <em>The Canadian Press.</em>  <A HREF="Ottawa.pdf">Ottawa wants silicone fluids used in breast implants deemed toxic</A>.<P>	 

	<LI>May 16, 2008. <em>Canada.com.</em> <A HREF="Ottawa2.pdf">Ottawa prepared to slap toxic label on widely used chemicals</A>.<P> 	 

	<LI>May 15, 2008. <em>CNW Group (via Newswire.ca/en).</em> <A HREF="newswire.pdf">Federal Government Announcement on Chemicals used in Chewing Gum, Dyes, Personal Care Products</A>.<P>	 

	<LI>May 14, 2008. <em>Montreal Gazette (via Canada.com).</em> <A HREF="Health_Canada.pdf">Health Canada may declare silicone breast implants toxic</A>.<P>	 

	<LI>May 14, 2008. <em>Kentucky Environmental Matters.</em> <A HREF="Kentucky.pdf">Canada may ban vinyl acetate from chewing gum base</A>.<P>	 

	<LI>May 13, 2008. <em>Ottawa Citizen.</em> <A HREF="Ottawa3.pdf">National: Gum substance possibly toxic</A>.<P>	 

	<LI>May 13, 2008. <em>Greenpeace Canada Weblogs.</em>  <A HREF="Greenpeace.pdf">99% of British Columbians want the right to know!</A><P>	 

	<LI>May 13, 2008. <em>The Globe and Mail.</em> <A HREF="Ottawa4.pdf">Ottawa may classify gum ingredient as toxic</A>.<P>	 

	<LI>May 12, 2008. <em>The Canadian Press.</em> <A HREF="Ottawa5.pdf">Ottawa could move to label substance used to make chewing gum as toxic</A>.<P>	 

	<LI>May 12, 2008. <em>TheStar.com.</em> <A HREF="Ottawa6.pdf">Chemical in gum toxic? Maybe, Ottawa says</A>.<P>	 

	<LI>May 12, 2008. <em>Montreal Gazette (via Canada.com).</em> <A HREF="Montreal.pdf">Substance found in chewing gum could be labeled toxic</A>.<P>	 

	<LI>May 12, 2008. <em>Edmonton Sun.</em> <A HREF="Edmonton.pdf">Feds could label chewing gum ingredient as toxic</A>.<P> 	 
</UL>


 

	<!--- Page text ends here --->

</td></tr></table></td>

<cfinclude template="../footer.cfm">

