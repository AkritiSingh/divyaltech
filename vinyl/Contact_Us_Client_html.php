
	<table width="100%" cellpadding="5" cellspacing="0" style="font-family:Verdana,Arial,Helvetica;font-size: 10pt;"><tr>
		<td colspan="2">
			<h3>Vinyl Acetate Council Contact Us Form</h3>
			<p>The following submission was received from a contact us form:</p>
		</td>
	</tr><tr>
		<td valign="top"><strong>Name:</strong></td>
		<td valign="top"><?php echo $_REQUEST['firstName'] $attributes.lastName ?></td>
	</tr><tr>
		<td valign="top"><strong>Email:</strong></td>
		<td valign="top"><?php echo $attributes.contactEmail ?></td>
	</tr><tr>
		<td valign="top"><strong>Title:</strong></td>
		<td valign="top"><?php echo $attributes.title ?></td>		
	</tr><tr>
		<td valign="top"><strong>Company:</strong></td>
		<td valign="top"><?php echo $attributes.company ?></td>		
	</tr><tr>
		<td valign="top"><strong>Address:</strong></td>
		<td valign="top"><?php echo $attributes.address ?></td>
	</tr><tr>
		<td valign="top"><strong>City:</strong></td>
		<td valign="top"><?php echo $attributes.city ?></td>
	</tr><tr>
		<td valign="top"><strong>State:</strong></td>
		<td valign="top"><?php echo $attributes.state ?></td>
	</tr><tr>
		<td valign="top"><strong>Zip Code:</strong></td>
		<td valign="top"><?php echo $attributes.zipcode ?></td>		
	</tr><tr>
		<td valign="top"><strong>Country:</strong></td>
		<td valign="top"><?php echo $attributes.country ?></td>		
	</tr><tr>
		<td valign="top"><strong>Telephone:</strong></td>
		<td valign="top"><?php echo $attributes.telephone ?></td>
	</tr><tr>
		<td valign="top"><strong>Fax:</strong></td>
		<td valign="top"><?php echo $attributes.fax ?></td>	
	</tr><tr>
		<td valign="top"><strong>Participate in Future Web Briefings:</strong></td>
		<td valign="top"><?php echo $attributes.webbriefings ?></td>
	</tr><tr>
		<td valign="top"><strong>Notify me when new information is posted:</strong></td>
		<td valign="top"><?php echo $attributes.newinfo ?></td>		
	</tr><tr>
		<td valign="top"><strong>Add me to Canadian news distribution list:</strong></td>
		<td valign="top"><?php echo $attributes.canadiannews ?></td>																		
	</tr><tr>
		<td valign="top"><strong>Comments or Suggestions:</strong></td>
		<td valign="top"><?php echo $attributes.comments ?></td>
	</tr>		
	</table>
