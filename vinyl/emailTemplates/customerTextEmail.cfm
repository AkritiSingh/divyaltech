<!--- Set default values for all possible form fields into the ATTRIBUTES scope --->
<cfset request.emailTemplateFormFieldList = "firstName,lastName,email,title,company,Street_Address,city,state,Zip_Code,telephone,FaxComments" />

<cfloop list="#request.emailTemplateFormFieldList#" index="emailTemplateFormFieldName">
	 <cfparam name="attributes.#emailTemplateFormFieldName#" default="">        
</cfloop>

<cfset CRLF = Chr(13) & Chr(10) />
<cfoutput>
Thank You #CRLF# #CRLF#
We have received your email.  A copy of the information you sent is supplied below for verification and your records. #CRLF# #CRLF#
Name:  #attributes.firstName# #attributes.lastName# #CRLF#
Email:  #attributes.email# #CRLF#
Title:  #attributes.title# #CRLF#
Company:  #attributes.company# #CRLF#
Street Address:  #attributes.Street_Address# #CRLF#
City:  #attributes.city# #CRLF#
State:  #attributes.state# #CRLF#
Zip:  #attributes.Zip_Code# #CRLF#
Telephone:  #attributes.telephone# #CRLF#
Fax:  #attributes.Fax# #CRLF#
Comments:  #attributes.Comments#
</cfoutput>	