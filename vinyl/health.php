<?php 
 $current = 'health';
include('navigation.php');?>

	
	<!--- Page text begins here --->
<div class="center_content_pages">
        
          <div class="left_content" style="width:48%;">
        
        	     <div class="left_block" style="float: left;width:95%;">
            	<h2>Health and Environmental Effects</h2>
                  <h4 style="color: #b52025;">Health</h4>
                     <p>
                    As with all chemical substances, the health effects associated with exposure to vinyl acetate depend on the exposure level and duration.  Vinyl acetate is not considered to be highly toxic.  In rats, the median lethal dose (LD50) by the oral route is 2,920 mg/kg and the inhalation four-hour lethal concentration is 4,000 ppm.  Vinyl acetate can penetrate the skin, but does not do so readily; it is considered to be moderately toxic by skin absorption in rabbits with a dermal LD50 of 2,335 mg/kg.  If vinyl acetate contacts the eyes, it may cause severe irritation, including corneal burns, redness and swelling.  Vinyl acetate vapors have been reported to be irritating to the eyes at 21 ppm but not at 10 ppm.  The odor detection threshold is reported to be about 0.5 ppm.
                     <p>
                    <p>
                    Repeat dose, subchronic studies in animals have documented irritation to the nose, throat and respiratory system.  The exposure concentrations in repeat dose studies associated with no adverse effects were 50 ppm in mice and 200 ppm in rats.  Acute high level exposure in animals has been shown to result in pulmonary edema.  
                    <p>
                    Laboratory animal studies have found that long-term exposure to vinyl acetate can cause a carcinogenic response.  Tumors observed in these studies occurred only at vinyl acetate concentrations well above recommended exposure levels.  Also, tumors arose in animal tissues that were directly in contact with vinyl acetate (i.e., the nose and upper respiratory tract if inhaled or in the mouth, esophagus and stomach if ingested).  Through mechanistic research, tumor development is believed to arise following the metabolism of vinyl acetate by enzymes (carboxylesterases) normally found in tissues of the respiratory tract or upper digestive tract first contacted by vinyl acetate.   These enzymes convert vinyl acetate to acetic acid and acetaldehyde.  At a sufficiently high inhaled or ingested vinyl acetate concentration, the amounts of acetaldehyde and acetic acid can directly produce genetic mutation in respiratory or digestive tract tissues in animals and trigger the cellular proliferation necessary for tumor development.  The tumors observed in laboratory animals at very high exposure concentrations for their lifetimes are therefore not considered to be of relevance to humans exposed to low concentrations under typical use conditions.  
                    <p>
                    <h4 style="color: #b52025;">Environmental</h4>
                     <p>
                    Vinyl acetate is considered to be slightly to moderately toxic to aquatic organisms.  When released into the environment, vinyl acetate will partition to air where it is rapidly degraded by photochemical pathways involving hydroxyl radicals or ozone.  Volatilization also occurs following releases of vinyl acetate to soil or water.  Vinyl acetate is readily biodegraded by either anaerobic or aerobic mechanisms.  Additionally, bioaccumulation, the increase in chemical concentration under steady state conditions in an organism relative to its environmental concentration, is unlikely. 
                     <p>
                    <p>
                    The VAC continues to support programs to understand the potential risk associated with low-level exposure.
                    <p>
            </div>
          </div>
        
            <div class="right_block" style="float: right;padding: 20px 20px 10px 0;width: 48%">
            	<h2>Safety</h2>
                <h4 style="color: #b52025;">Safe Handling Guide</h4>
                 <p>
                 The Vinyl Acetate Council developed the Vinyl Acetate Safe Handling Guide to promote the safe and responsible use of vinyl                  acetate. The Guide was developed based on procedures recommended by VAC members and reflects best practices on the safe                  handling, storage and transport of vinyl acetate. The Guide is intended for properly trained workers who may come in                  contact with vinyl acetate monomer, including individuals responsible for plant safety as well as other safety and health                  professionals. Always consult your supplier's safety data sheet, product label and other safe handling instructions for                  the most current recommendations.
                 </p>
                 <p>
                 The VAC Safe Handling Guide has been translated into the following languages: Chinese, French, German, Italian, Spanish (Latin American) and Portuguese (Brazilian).
                 </p> 
                </p> <br/><br/>   
                    
  	
            </div>
   </div>
	<!--- Page text ends here --->


<?php include('footer.php');?>

