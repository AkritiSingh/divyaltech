<?php 
$CRLF = Chr(13) & Chr(10);
echo "VAM Safe Handling Guide Order Form" .$CRLF $CRLF.
"The following submission was received from a VAM Safe Handling Guide order form: " .$CRLF $CRLF.
"Name: " $attributes.firstName $attributes.lastName .$CRLF.
"Email: " $attributes.contactEmail .$CRLF.
"Company: " $attributes.company .$CRLF.
"Address: " $attributes.address .$CRLF.
"City: " $attributes.city .$CRLF.
"State: " $attributes.state .$CRLF.
"Zip Code: " $attributes.zipcode .$CRLF.
"Telephone: " $attributes.telephone .$CRLF.
"Fax: " $attributes.fax .$CRLF.
"How is your comapny associated with VAM: " $attributes.associated .$CRLF;


?>			 
 