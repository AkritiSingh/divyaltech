<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Web Application Development </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong>Web Application Development</strong></span></h6>
                <p>Custom web application developments helps you manage and develop your business and organization.With an 
                  in-house programming improvement group, our capacity to create, adjust, and completely coordinate web 
                  applications around your requirements is the thing that separates us.
                 
               </p>
               <img style="max-width:30%;" title="App Development" alt="App Development" src="images/webapp1.png">
            </div>
           <div class="da-cantent"> 
            <p>Our web developers have wide involvement in site improvement and web designing . They can manufacture and plan 
            your site as indicated by your necessities and requirements. Our web designers can convey your project on time and on
            spending plan.
            </p>       
            <p>
                At <b>Divyaltech</b> Web development concentrates on the diverse approaches, procedures and tools that are the   
                establishment of any development procedure. It can be actualized through legitimate arranging, outlining, 
                advancement,development and their definitive usage.
            </p>  
            <!-- MENU-LOCATION=NONE --><br><br>
           <h6><span style=" font-size: 27px;color: #464646;">The Benefit of Custom Web Applications: </span></h6>
            <ul>
				<li>Ability to create custom reports, mail consolidations, and interchanges utilizing constant information.</li>
              <li>Computerize your procedures and gather data through a consolidated application.</li>
       		  <li>No required permitting or software installation for various PCs/clients.</li>
              <li>Set up particular access and authorizations to keep up integrity and security.</li>   	
                <li>Coordinate devices consistently with other online applications and/or legacy databases</li>
                <li>Guarantee convenience through intuitive interface design.</li>		
           	</ul>        
         </div>
           <br>
           <br>
           <div class="service">
           <h6><span style=" font-size: 27px;color: #464646;">WebSite Development Services Provided by Us: </span></h6>
             <ul>
                 <li>HTML/CSS Design</li>
                 <li>Theme customization</li>
                 <li>Bootstrap / Responsive designs</li>
                 <li>Creating a Custom Website Design</li>
                 <li>Creating a Website from a Template</li>
                 <li>Updating Your Existing Website</li>
                 <li>Domain Name Registration & Website Hosting</li>
             </ul>
           
           </div>
      </div> 
           <div class="rightside">
            <?php include 'technology.php';?>
            <img style="max-width:30%; margin-top:25px;margin-left: 40px;" title="Web Development" alt="Web Development" src="images/webapp3.png">
             <div class="services" style="margin-top:15px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">Technologies We Use:</span> </h6>
                   <h4>Front End</h4>
				<ul>
                    <li>HTML,CSS3</li>
                  	<li>JavaScript</li>
					<li>AJAX</li>
		         </ul> 
                    <h4>Back End</h4>
				 <ul>
                    <li>Cold Fusion,PHP</li>
                    <li>MYSQL,MSSQL,Oracle,SQL</li>
			     </ul>      
               </div>
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
