<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1>Wordpress </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong> WordPress Development</strong></span></h6>
                   <p>WordPress is an online, open source website creation tool written in PHP. WordPress is one of the most 
                      popular open source platforms used for creating a website or a blog.Everybody anticipates a site that is 
                      simple, rich and exceptionally useful. WordPress sites have every one of the components that you are 
                      searching for with customization option as well.

                    </p>
                     <p>Our Web App development services cover a wide range – from essential site outline and development to 
                       customized PHP web to E-business solutions utilizing Magento, Zen truck. We likewise give Open Source CMS 
                       Development utilizing WordPress, Joomla and others. Aside from Bespoke, CMS and E-trade arrangements, we 
                       additionally offer Open Source CRM and and e-learning solutions.
                   </p>
            </div>
            <br><br>
           <div class="type" style="margin-top:0px;">
               <br><h6><span style=" font-size: 27px;color: #464646;">We Work On WordPress Theme Development:</span> </h6>
                <p>We develop WordPress themes for our client using two approaches.</p>
                  <p><strong>Customized Themes:</strong>In the customized approach, you can give us word press theme of your 
                    decision which are promptly accessible in the business sector and we will customize it for you to suit your 
                    prerequisite. We adjust the theme so it suits your site necessity.
                 </p> 
                 <p><strong>Themes from Scratch:</strong>In this approach, we build up the themes right starting with no outside 
                   help in view of your prerequisites.The custom theme creation helps to remove the unwanted codes and elements 
                   and improves the performance of your website to achieve the greatest results.
                </p> 
                <br><h6><span style=" font-size: 27px;color: #464646;">We Work On WordPress Plugin Development:</span> </h6>
                <p>Creating WordPress plugins that can be consistently incorporated to your site is a area we have practical 
                 experience in.If the plugin you need is accessible in the WordPress Plugin vaults, we integrate them to your 
                 site. If the plugin requires modification to suit your requirement, we customize them to suit your requirement 
                 and integrate them to your website. If the plugin is not accessible,we create them starting with no outside 
                 help and incorporate them to your site. </p>
                 <h6><span style=" font-size: 20px;color: #464646;">Our services include:</span></h6>
                 <ul>
              	    <li>Plug-in integration and customization.</li>
                     <li>Plug-in upgrade, enhancement and modifications.</li>
                     <li>Plug-in development and installation.</li>
                     <li>WordPress CMS development and coding</li>
                 </ul>
              
               
               <h6><span style=" font-size: 27px;color: #464646;">Our Mision:</span></h6>
                 <p>With mission to add to the customer's execution in their business, our organization is genuinely working 
                   harder for demonstrating benefit situated results to their clients all over the World.
                 </p>  
           </div>
           <br><br>
        
      </div> 
          <div class="rightside">
           <?php include 'technology.php';?>
           <img style="max-width:30%; margin-top:50px;" title="App Development" alt="App Development" src="images/wordpress.jpg">
             <div class="tech" style="margin-top:65px;">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong>Our WordPress Areas of Expertise:</strong></span></h6>
                    <ul>
                    <li>WordPress Installation and; Setup</li>
                    <li>Custom CMS Development</li>
                    <li>WordPress custom plugin/widget development</li>
                    <li>WordPress Theme Development</li>
                    <li>WordPress Template Design</li>
                    <li>Corporate Blogging Solutions</li>
                    <li>Mobile Template Design</li>
                    <li>WordPress Data Migration</li>
                    <li>WordPress SEO Services</li>
                    <li>WordPress Security Audits</li>
                    <li>WordPress Troubleshooting and Support</li>
                    <li>WordPress Maintenance and Upgrades</li>
                  </ul>
               </div>
               
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
