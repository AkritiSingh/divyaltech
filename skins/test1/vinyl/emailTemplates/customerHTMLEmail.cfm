<!--- Set default values for all possible form fields into the ATTRIBUTES scope --->
<cfset request.emailTemplateFormFieldList = "firstName,lastName,email,title,company,Street_Address,city,state,Zip_Code,telephone,FaxComments" />

<cfloop list="#request.emailTemplateFormFieldList#" index="emailTemplateFormFieldName">
	 <cfparam name="attributes.#emailTemplateFormFieldName#" default="">        
</cfloop>

<cfoutput>
<table width="100%" cellpadding="5" cellspacing="0" style="font-family:Verdana,Arial,Helvetica;font-size: 10pt;">
	<tr>
		<td colspan="2">
			<h3>Thank You</h3>
			<p>We have received your email.  A copy of the information you sent is supplied below for verification and your records.</p>
		</td>
	</tr>
	<tr>
		<td valign="top"><strong>Name:</strong></td>
		<td valign="top">#attributes.firstName# #attributes.lastName#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Title:</strong></td>
		<td valign="top">#attributes.title#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Company:</strong></td>
		<td valign="top">#attributes.company#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Street Address:</strong></td>
		<td valign="top">#attributes.Street_Address#</td>
	</tr>
	<tr>
		<td valign="top"><strong>City:</strong></td>
		<td valign="top">#attributes.city#</td>
	</tr>
	<tr>
		<td valign="top"><strong>State:</strong></td>
		<td valign="top">#attributes.state#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Zip:</strong></td>
		<td valign="top">#attributes.Zip_Code#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Telephone:</strong></td>
		<td valign="top">#attributes.telephone#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Fax:</strong></td>
		<td valign="top">#attributes.Fax#</td>
	</tr>
	<tr>
		<td valign="top"><strong>Comments:</strong></td>
		<td valign="top">#attributes.Comments#</td>
	</tr>
</table>
</cfoutput>