<cfinclude template="../navigation.cfm">

<IMG SRC="/images/hdr-mo.jpg" WIDTH=382 HEIGHT=63 ALT="Members Only">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->

 <H3>Active Issues of Interest</H3>

<blockquote>
<h4>AEGL</h4>

<a href="vinylacetate_tsd_draft0904.pdf" target="_blank">Vinyl Acetate Technical Support Document (TSD) Developed by Oak Ridge National Laboratory (ORNL) & Environmental Protection Agency</a> (Draft) &#151; September 2004
<p>
<a href="acetaldehydedraft_tsdforseptemberaeglmeeting.pdf" target="_blank">Acetaldehyde Technical Support Document (TSD) Developed by RIVM & Environmental Protection Agency</a> (Draft) &#151; September 2004
<p>
<a href="va_reversibilityltr.pdf" target="_blank">DuPont letter to C. Troxel (ORNL) regarding AEGL-2 Point of Departure &#151; December 1, 2004</a> (Bogdanffy et al. 1997.  Four-week Inhalation Cell Proliferation Study of the Effects of Vinyl Acetate on Rat Nasal Epithelium.  <i>Inhalation Toxicology</i> 9:331-350)
<p>
VAC <a href="finalltr_epava_aegl.pdf" target="_blank">Cover Letter</a> and <a href="vam_literature_sources_forconsequenceanalyses.pdf" target="_blank">Summary Spreadsheet</a> Submission to EPA on VAM Literature Review and AEGL &#151; November 12, 2003<p>


<i>Federal Register</i> Notices<br>
<ul>
<li><a href="120604aeglfr.pdf" target="_blank">Announcement of December 13-15, 2004 Meeting</a> - December 6, 2004 (VAM to be discussed)<br>
<li><a href="aegl_vam_acetaldehyde090304fr.pdf" target="_blank">Announcement of September 21-23, 2004 Meeting</a> - September 3, 2004 (VAM and acetaldehyde to be discussed)<br>
<li><a href="112503aeglfr.pdf" target="_blank">Announcement of December 10-12, 2003 Meeting</a> - November 25, 2003 (VAM to be discussed)<br>
<li><a href="053102aeglfr.pdf" target="_blank">Announcement of VAM on list of priority chemicals for AEGL development</a> - May 31, 2002</ul>

<h4>EU RISK ASSESSMENT - VAM</h4><p>

<a href="riskassessment_may2005.pdf" target="_blank">Latest Draft EU Risk Assessment &#150; Environmental</a> &#151; May 2005
<p>

<a href="lv_059_tm203_hh.pdf" target="_blank">Latest Draft EU Risk Assessment &#150; Human Health</a> &#151; April 2003
<p>

<a href="minutes.pdf" target="_blank">EU TM IV '02 Minutes on Vinyl Acetate</a> (Draft) 
<p>

<h4>ENDOCRINE</h4>
<i>Information coming soon</i></blockquote>



 

	<!--- Page text ends here --->

</td></tr></table></td>

<cfinclude template="../footer.cfm">

