<?php include('navigation.php');?>

<IMG SRC="images/hdr-gu.jpg" WIDTH=382 HEIGHT=63 ALT="Global Update">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->



<h3>Global Update</h3>

<h4>European Union (EU)</h4>

<i>European Council Regulation (EEC) 793/93</i>

<blockquote>
Vinyl acetate was listed on the First Priority List developed under European Council Regulation (EEC) 793/93. Germany volunteered to serve as the rapporteur and is in the process of developing the environmental and human health risk assessment documents for vinyl acetate.  
<p>
The <a href="http://www.petrochemistry.net/acetyls.html" target="_blank">Acetyls Sector Group (ASG)</a> has taken the lead in communicating with Germany and providing industry data. VAC is providing technical support to ASG.  
<p>
The European Chemicals Bureau (ECB) has been the focal point for the collection of data and the assessment review. From the <a href="http://ecb.jrc.it/" target="_blank">ECB website</a>, additional information can be found on European Council Regulation (EEC) 793/93, as well as a complete schedule of events remaining for 2008 (see page 21 of <a href="http://ecb.jrc.it/newsletter/newsletter200703.pdf" target="_blank">ECB Newsletter 2007, Issue No. 3</a>).
<!--- <p>
The European Chemicals Bureau (ECB) is the focal point for the collection of data and the assessment review. From the <a href="http://ecb.jrc.it/" target="_blank">ECB website</a>, additional information can be found on European Council Regulation (EEC) 793/93, as well as a complete schedule of 2005 events. ---> 
<p>
Currently there are a total of four priority lists developed under (EEC) 793/93, each of which identify substances that require review according to the EU.  For each chemical on a priority list, an EU member state is identified as the rapporteur.


<p>
From the ECB site, VAC retrieved a profile for vinyl acetate from ESIS (<a href="esis_vam_22Feb05.pdf" target="_blank">click here to access</a>), the European chemical Substance Information System (February 2005).
</blockquote>

	<!--- Page text ends here --->

</td></tr></table></td>

<?php include('footer.php');?>

