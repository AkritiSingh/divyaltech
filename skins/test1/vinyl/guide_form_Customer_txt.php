<?php 
$CRLF = Chr(13) & Chr(10) ;
echo "Thank You".$CRLF $CRLF.
"We have received your email.  A copy of the information you sent is supplied below for verification and your records. ".$CRLF $CRLF.
"If you have any questions or concerns please email info@vinylacetate.org.".$CRLF $CRLF.
"Name: ".$attributes.firstName $attributes.lastName $CRLF.
"Email: ".$attributes.contactEmail $CRLF.
"Company: ".$attributes.company $CRLF.
"Address: " .$attributes.address $CRLF.
"City: " .$attributes.city $CRLF.
"State: " .$attributes.state $CRLF.
"Zip Code: " .$attributes.zipcode $CRLF.
"Telephone: " .$attributes.telephone $CRLF.
"Fax: " .$attributes.fax $CRLF.
"How is your comapny associated with VAM: " .$attributes.associated $CRLF ;


?>		 