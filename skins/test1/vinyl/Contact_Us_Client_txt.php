<?php
$CRLF = Chr(13) & Chr(10);
echo "Vinyl Acetate Council Contact Us Form" .$CRLF $CRLF.
"The following submission was received from a contact us form:" .$CRLF $CRLF.
"Name:" $attributes.firstName $attributes.lastName .$CRLF.
"Email:" $attributes.contactEmail .$CRLF.
"Title:" $attributes.title .$CRLF.
"Company:" $attributes.company .$CRLF.
"Address:" $attributes.address .$CRLF.
"City:" $attributes.city .$CRLF.
"State:" $attributes.state .$CRLF.
"Zip Code:" $attributes.zipcode .$CRLF.
"Country:" $attributes.country .$CRLF.
"Telephone:" $attributes.telephone .$CRLF.
"Fax:" $attributes.fax .$CRLF.
"Participate in Future Web Briefings:" $attributes.webbriefings .$CRLF.
"Notify me when new information is posted:" $attributes.newinfo .$CRLF.
"Add me to Canadian news distribution list:" $attributes.canadiannews .$CRLF.
"Comments or Suggestions:" $attributes.comments .$CRLF;


?>		 
 