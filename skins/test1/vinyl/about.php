<?php 
$current = 'about';
include('navigation.php');?>
	<!--- Page text begins here --->
<div class="center_content_pages">
        
        
        <div class="left_content">
        
        	<div class="left_block">
           	<h2>About Us</h2>
                <img src="images/pic.jpg" alt="" title="" border="0" class="pic" />
            </div>
            
            <div class="left_block">
            	<h2>What is Vinyl Acetate? </h2>
                <p>
<strong>Vinyl acetate or vinyl acetate monomer (VAM)</strong>  is a versatile and economically important building block chemical used to manufacture polymers that have with a wide variety of industrial and commercial applications.
<br /><br />
                </p>
            </div>
            
            
            <div class="block_wide">
                <h2>What is the Vinyl Acetate Council? </h2>
                
                <p>
The <strong>Vinyl Acetate Council (VAC)</strong> represents the North American manufactures and major processors of vinyl acetate monomer. VAC is dedicated to promoting the safe and continued use of vinyl acetate monomer (VAM) and VAM-based products.
<br /><br />
VAC was formed in 1994 as a not-for-profit (501(c)(6)) tax exempt corporation, in the District of Columbia. VAC was previously known as the Vinyl Acetate Toxicology Group (VATG).
<br /><br />
The VAC has a close working relationship with its European counterparts through the CEFIC Acetyls Sector Group.

                </p><br><br>
            
                <div class="mission" style="width:628px;; padding-top:25px;">   
                 <h2>Mission</h2>
                   <p>
                    The Mission of the VAC is to:<br/><br>
                       <ul>
                       <li>Collect and disseminate health, safety, and environmental information relevant to vinyl acetate.</li></br>
                       <li>Assess the need for additional information and sponsor studies accordingly.</li></br>
                       <li>Promote product stewardship with members and downstream users of vinyl acetate.</li></br>
                      <li>Monitor and participate in regulatory and other government proceedings with the potential to impact the safe and continued use of vinyl acetate.</li>
                        </span></ul>
                       </p>
                    </div>
			       
        </div>
        </div>
        
            <div class="right_block">
            	<h2>Quick Facts</h2>
                
                <div class="fact">
			<span style="color: white;">
		<p>
				<ul><li>Chemical Name: Vinyl Acetate</li>
				<li>Synonyms: Vinyl Ester Acetic Acid, Ethenyl Ester, Vinyl Acetate Monomer, VAM, Ethenyl Acetate</li>
				<li>CAS Number: 108-05-4</li>
				<li>Molecular formula: CH3COOCH=CH2</li>
				<li>Molecular weight: 86.09 g/mol</li>
				<li>Density: 0.934 g/cm3</li>
				<li>Boiling point: 72.7°C</li>
				<li>Melting point: -93°C</li>
				</ul>
			
			</p></span>
		 </div> 
                 <br/>
                 <h2>Products</h2>
                <p>
                VAM based polymers are used in the production of:
                <br/><br/>
                    <div id="font"><ul>                                                                                            
                         <li>plastics</li>
                         <li>films</li>
                         <li>lacquers</li>
                         <li>laminating adhesives</li>
                         <li>elastomers</li>
                         <li>inks</li>
                         <li>water-based emulsion paints</li>
                         <li>adhesives</li>
                         <li>finishing and impregnation materials</li>
                         <li>paper coatings</li>
                         <li>floor tiling</li>
                         <li>safety glass</li>
                         <li>building construction</li>
                         <li>acrylic fibers</li>
                         <li>glue</li>
                         <li>cosmetics and personal care products</li>
                         <li>textile finishing and non-wovens</li>
                    </ul>
                    </div>
                </p> <br/><br/>   
                    
  	
            </div>
            
<div class="wide_content" style="padding-top:25px;">
 <p><H2>Membership</H2>
                    <P ALIGN=JUSTIFY>Membership in the VAC is open to any manufacturer and processor/user of VAM. North American                      manufacturers aare eligible to be Full Members while all others are eligible to be Associate Members.  Current                      members of the Vinyl Acetate Council include:<br><br>
                    <img src="images/celance.JPG" alt="" title="" border="0" class="logo"width="110px" height="40px"/>
                   <img src="images/dow.jpg" alt="" title="" border="0" class="logo"/>
                   <img src="images/lyondell.JPG" alt="" title="" border="0" class="logo"/>
                   <img src="images/wacker.JPG" alt="" title="" border="0" class="logo" width="120px" height="30px"/>	
                     </p><br><br>
             
                <div class="Acetaldehyde" style="padding-top:25px;">                
                <h2>Acetaldehyde Working Group</h2>
                <p>
                  Acetaldehyde is a significant metabolite of vinyl acetate. Given that the biologic properties of acetaldehyde tend to                  drive VAM’s human health risk assessment, the VAC formed the Acetaldehyde Working Group (AWG).
                </p> </br></br>
                <p>
                Membership in the AWG is open to all members of the VAC as well as companies and/or other organizations involved in the                manufacture and/or emissions of acetaldehyde.  In addition to the VAC,  the following are members of the AWG: Eastman                Chemical Company and the American Forest and Paper Association (www.afandpa.org).
                </p><br><br>
                <p>
                One of the VAC's primary projects is to develop a pharmacokinetic model to further the risk assessment on vinyl acetate.                Once completed, the risk assessment model should be of immediate use to EPA as they address vinyl acetate as a hazardous                 air pollutant. The VAC organized a Workshop with EPA to review the pharmacokinetics risk assessment model. The VAC                routinely submits technical comments to research and regulatory agencies throughout the world, most notably the                 Environmental Protection Agency, International Agency for Research on Cancer and California Proposition 65 Office.
                </p>     
                </div>
            </div>
</div>
	<!--- Page text ends here --->



<?php include('footer.php');?>

