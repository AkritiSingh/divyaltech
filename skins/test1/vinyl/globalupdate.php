<?php
$current = 'global';
include('navigation.php');?>
	
	<!--- Page text begins here --->
<div class="center_content_pages">
      
        <div class="left_content" style="width:48%;">
        
        	<div class="left_block" style="padding: 20px 20px 10px 15px;width: 97%;">
				<h2>Global Update</h2>
				<p>Click on the links below for an update on how vinyl acetate is being addressed by these organizations. VAC routinely monitors developments regarding vinyl acetate and will post updates as they become available.
				<p>
				
				<a href="vam.cfm">Canadian Chemicals Management Plan &#151; "Challenge"</a>
				<p>
				<a href="global_eu.php">European Union (EU)</a>
				<p>
				<a href="global_oecd.php">Organisation for Economic Co-operation and Development (OECD)</a>
				<p>
				<a href="global_usepa.php">United States Environmental Protection Agency (US EPA)</a>

			</div>	
		</div>
		<div class="right_block" style="float: right;padding: 55px 20px 10px 0;width: 48%;">
			<b style="color: #b52025;">North America</b><br>
			<b style="color: #b52025;">Europe</b><br>
			<p>The Vinyl Acetate Council coordinates activities with the 
            <a href="http://www.petrochemistry.net/acetyls.html" target="">European Acetyls Sector Group</a>, which represents the European producers of vinyl acetate.
			<br>
			<b style="color: #b52025;">Asia</b><br>
			<b style="color: #b52025;">Other</b><br>
		</div>
	<!--- Page text ends here --->
	
    	</div>
		<?php include('footer.php');?>
	</div>
</div>
</body>
</html>
