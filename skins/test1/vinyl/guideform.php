<!---NOTE: for each paragraph, please use <p> and closing tag </p> there is a set height for each paragraph--->
<?php session_start();

if(isset($_POST['VAM_guide_form_submit'])){
	// code for check server side validation
	if(empty($_SESSION['captcha_code'] ) || strcasecmp($_SESSION['captcha_code'], $_POST['captcha_code']) != 0){  
		$msg="<span style='color:red'>The Validation code does not match!</span>";// Captcha verification is incorrect.		
	}else{// Captcha verification is Correct. Final Code Execute here!	
		$form_values=array('firstname'=>$_POST['firstName'],
							'lastname'=>$_POST['lastName'],
							'email'=>$_POST['contactEmail'],
							'company'=>$_POST['company'],
							'address'=>$_POST['address'],
							'city'=>$_POST['city'],
							'state'=>$_POST['state'],
							'zipcode'=>$_POST['zipcode'],
							'telephone'=>$_POST['telephone'],
							'fax'=>$_POST['fax'],
							'associated'=>$_POST['associated'],
							
						);	
		header("Location: guide_form_confirmation.php?" . http_build_query($form_values, null, '&'));
	}
}

?>
<?php $formcode = "guideform"; ?>
<?php $errorfields = ""; ?>

<!--- Define Fields --->
<?php $fieldlist = array("firstName","lastName","contactEmail","company","address","city","state","zipcode","telephone","fax","associated","encryptedString","captchaResponse","captchaChallenge");
$form=array();
?>
<?php
foreach($fieldlist as $value)
{
	$form[$value]="";
}
?>
<!--<cfloop list="#request.fieldList#" index="fieldname">
	<cfparam name="form.#fieldname#" default="">
</cfloop>-->

<!--- Process inputs if form has been submitted --->
<?php /*<cfset request.newform = application.toolsfactory.getform(request.formcode) />*/?>

<?php /*<cfif isdefined("form.VAM_guide_form_submit")>
	<!--- Form Validation --->
	
	<cfif len(form.contactemail) and (refindnocase("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.(([a-z]{2,3})|(aero|coop|info|museum|name))$", "#form.contactEmail#") eq 0)>
		<cfset structinsert(request.errors, "emailValidation", "The email address you entered does not appear to be a valid email address.") />
		<cfset request.errorfields = listappend(request.errorfields, "contactEmail") />
	</cfif>
	
	<cfset request.formresult = request.newform.processauthorizeonly(form) />
	
	<cfif structkeyexists(request.formresult, "errors") and isstruct(request.formresult.errors) and structkeyexists(request.formresult.errors, "missingFields")>
		<cfset structinsert(request.errors, "missingFields", "The required fields highlighted below must be completed to submit this form.") />
		<cfset request.errorfields = listappend(request.errorfields, request.formresult.errors.missingfields) />
	</cfif>
	
	<cfif structkeyexists(request.formresult, "errors") and isstruct(request.formresult.errors) and structkeyexists(request.formresult.errors, "captchaFailed")>
		<cfset structinsert(request.errors, "captchaFailed", "The security text you entered is not valid.") />
		<cfset request.errorfields = listappend(request.errorfields, request.formresult.errors.captchaFailed) />
	</cfif>
	
    <!--- <cfdump var="#request#" /> --->
    
	<cfif structisempty(request.errors)>
		<cfset request.formresult = request.newform.process(form) />
		<cfif request.formresult.result eq true>
			<cflocation url="guide_form_confirmation.cfm?formCode=#request.formCode#&id=#request.formResult.submissionID#" addtoken="false" />	
		<cfelseif structkeyexists(request.formresult, "errors") and isstruct(request.formresult.errors) and structkeyexists(request.formresult.errors, "paymentError")>
			<cfset structinsert(request.errors, "paymentError", request.formresult.errors.paymenterror) />
		<cfelse>
			<cfdump var="#request.formResult#">
			<cfset structinsert(request.errors, "unknownError", "There was an error processing your submission") />
		</cfif>
	</cfif>
</cfif>
*/
?>

<?php include('navigation.php');?>

<IMG SRC="images/hdr-vas.jpg" WIDTH=382 HEIGHT=63 ALT="Safe Handling Guide">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
		<!--- Page text begins here --->

<h3>New Vinyl Acetate Safe Handling Guide - April 2010</h3>

The Vinyl Acetate Council's VAM Safe Handling Guide is intended for use by companies that handle and/or process vinyl acetate monomer (VAM) and is to be used in conjunction with safety data sheets provided by your supplier. To request a copy, please complete the fields below and help us to better understand your involvement in the VAM industry.<br><br> 
	
 	
		<noscript>
			<div>
				<span style="float: left; margin-right: 0.3em;color: red; font-weight: bold;"></span>
				This page requires Javascript to function properly.  Please enable Javascript in your web browser and reload the page.
			</div>
		</noscript>
  
 
 		<!--<cfif not structisempty(request.errors)>
			<div>
				<span style="float: left; margin-right: 0.8em;color: red; font-weight: bold;">
				The following error(s) occurred while processing your request:
				</span><br />
				<ul>
					<cfloop list="#structKeyList(request.errors)#" index="thisErrorCode">
						<li><span style="color: red;">#request.errors[thisErrorCode]#</span></li>
					</cfloop>
				</ul>				
			</div>
		</cfif>-->

		<form method="post" id="newform" name="newform" onSubmit="return validateform(this);">
<blockquote>
	<table border=0 cellpadding=1 cellspacing=0 bgcolor="#461101" width=450><tr><td>
		<table border=0 cellpadding=2 cellspacing=0 bgcolor="#ffffff"><tr><td>
		<table border=0 cellpadding=4 cellspacing=0 bgcolor="#DDDAD5"><tr>
			<td>&nbsp;</td>
			<td><span style="color: red; font-weight: bold; font-weight: bold;">* = required</span></td>
		</tr><tr>			
			<td><?php /*<cfif listfindnocase(request.errorfields, "firstName")> span style="color: red; font-weight: bold;"</cfif>*/?>First Name: <span style="color: red; font-weight: bold;">*</span></td>
			<td><input name="firstName" id="firstName" type="text" size="25" maxlength="50" value="<?php echo $form["firstName"]; ?>" /></td>
		</tr><tr>			
			<td><!--<cfif listfindnocase(request.errorfields, "lastName")> span style="color: red; font-weight: bold;"</cfif>-->Last Name: <span style="color: red; font-weight: bold;">*</span></td>
			<td><input name="lastName" id="lastName" type="text" size="25" maxlength="50" value="<?php echo $form["lastName"]; ?>" /> </td>
		</tr><tr>
			<td><!--<cfif listfindnocase(request.errorfields, "contactEmail")> span style="color: red; font-weight: bold;"</cfif>-->E-mail: <span style="color: red; font-weight: bold;">*</span></td>  
			<td><input name="contactEmail" id="contactEmail" type="text" size="25" maxlength="30" value="<?php echo $form["contactEmail"]; ?>" /></td>
		</tr><tr>
			<td>Company:</td> 
			<td><input name="company" id="company" type="text" size="25" maxlength="50" value="<?php echo $form["company"]; ?>" /></td>			
		</tr><tr>
			<td>Street Address:</td> 
			<td><input name="address" id="address" type="text" size="25" maxlength="50" value="<?php echo $form["address"]; ?>" /></td>
		</tr><tr>
			<td>City:</td>    
			<td><input name="city" id="city" type="text" size="20" maxlength="20" value="<?php echo $form["city"]; ?>" /></td> 
		</tr><tr>
			<td width=45%>State:</td> 
			<td><input name="state" id="state" type="text" size="3" maxlength="2" value="<?php echo $form["state"]; ?>" /></td>  
		</tr><tr>
			<td>Zip Code: </td>
			<td><input name="zipcode" id="zipcode" type="text" size="10" maxlength="10" value="<?php echo $form["zipcode"]; ?>" /></td>
		</tr><tr>
			<td>Telephone:</td>
			<td><input name="telephone" id="telephone" type="text" size="20" maxlength="20" value="<?php echo $form["telephone"]; ?>" /></td>
		</tr><tr>
			<td>Fax: </td>
			<td><input name="fax" id="fax" type="text" size="20" maxlength="20" value="<?php echo $form["fax"]; ?>" /></td>
		</tr><tr>
			<td colspan=2>In which way is your company associated with vinyl acetate monomer (VAM)? (check all that apply)<br>
				<blockquote><table cellpadding=2 cellspacing=2><tr>
					<td>1)</td>
					<td>Producer</td>
					<td><input name="associated[]" type="checkbox" value="Producer" ><?php /*<cfif listfindnocase(form.associated,"Producer")>*/?>checked<?php /*</cfif>*/?></td>
				</tr><tr>
					<td>2)</td>
					<td>Distributor</td>
					<td><input name="associated[]" type="checkbox" value="Distributor"><?php /* <cfif listfindnocase(form.associated,"Distributor")>*/?>checked<?php /*</cfif>*/?></td>
				</tr><tr>
					<td>3)</td>
					<td>Downstream User</td>
					<td><input name="associated[]" type="checkbox" value="Downstream User"><?php /* <cfif listfindnocase(form.associated,"Downstream User")>*/?>checked<?php /*</cfif>*/?></td>
				</tr></table></blockquote></td>
				
		</tr>
		<?php /*<tr>
			<td>&nbsp;</td>
			<td>
				<cfset newcaptcha = request.newform.getcaptcha() />
					<input type="hidden" name="captchaChallenge" value="<cfoutput>#newCaptcha.encryptedString#</cfoutput>">
					<img src="<cfoutput>#newCaptcha.imagePath#</cfoutput>"></td>
		</tr><tr>
			<td><!--<cfif listfindnocase(request.errorfields, "captchaResponse")> span style="color: red; font-weight: bold;"</cfif>--><strong>Enter the text above: <span style="color: red; font-weight: bold;">*</span></strong></td> 
			<td><input name="captchaResponse" type="text" size="10" value="" /></td>
		</tr>*/?>
		<?php if(isset($msg)){?>
				<tr>
				  <td colspan="2" align="center" valign="top"><?php echo $msg;?></td>
				</tr>
				<?php } ?>			
				<tr>
				  <td> Validation code:</td>
				  <td><img src="captcha.php?rand=<?php echo rand();?>" id='captchaimg'></td>
				</tr>
				<tr>
					<td>Enter the code above here :</td>					
					<td><input id="captcha_code" name="captcha_code" type="text"><br>
						Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh.</td>
				</tr>
		
		<tr>
			<td align=center colspan=2><input type="submit" class="headlines" name="VAM_guide_form_submit" value="Submit Request" /></td>
		</tr></table></td>
		</tr></table></td>
		</tr></table>
	</blockquote>
		</form>
		</cfoutput>


	<!--- Page text ends here --->

</td></tr></table></td>
<script>
	
	var ck_name = /^[A-Za-z]{3,20}$/;
	var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; 	
	function validateform(form){
		var name = form.firstName.value;
		var email = form.contactEmail.value;
		var lastname = form.lastName.value;
		var errors = [];
			
		 if (!ck_name.test(name)) {
		  errors[errors.length] = "You valid Name .";		  
		 }
		 if (!ck_email.test(email)) {
		  errors[errors.length] = "You must enter a valid email address.";		  
		 }		
		 if (!ck_name.test(lastname)) {
		  errors[errors.length] = "You valid Last name .";
		 }			 		 
		 if (errors.length > 0) {			
		  reportErrors(errors);
		  return false;
		 }	
		return true;
		 }
		function reportErrors(errors){
		 var msg = "Please Enter Valide Data...\n";
		 for (var i = 0; i<errors.length; i++) {
		 var numError = i + 1;
		  msg += "\n" + numError + ". " + errors[i];
		}
		 alert(msg);
	}			
	</script>
	<script type='text/javascript'>
	function refreshCaptcha(){
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
	</script>

<?php include('footer.php');?>