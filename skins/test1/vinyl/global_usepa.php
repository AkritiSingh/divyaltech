<?php include('navigation.php');?>

<IMG SRC="images/hdr-gu.jpg" WIDTH=382 HEIGHT=63 ALT="Global Update">

	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 
	
	<!--- Page text begins here --->




<h3>Global Update</h3>

<h4>United States Environmental Protection Agency</h4>

<i>Integrated Risk Information System (IRIS)</i>

<blockquote>
EPA's assessment of vinyl acetate as presented in its Integrated Risk Information System was last updated in September 1990. The Agency's <a href="http://www.epa.gov/iris/subst/0512.htm" target="_blank">current vinyl acetate IRIS profile</a> is available online at <a href="http://www.epa.gov/iris/index.html" target="_blank">EPA's IRIS website</a>.
<p>
In a <a href="iris_fr_020503.pdf" target="_blank">February 5, 2003, <i>Federal Register</i> notice</a>, EPA announced its intention to update its assessment for vinyl acetate.  As of EPA's <a href="iris_agenda2005_70FR10616.pdf" target="_blank">March 4, 2005, <i>Federal Register</i> notice</a>, vinyl acetate remains an "assessment in progress."
<p>
According to EPA's <a href="http://cfpub.epa.gov/iristrac/index.cfm" target="_blank">IRIS Chemical Assessment Tracking System</a> (last updated for vinyl acetate on October 14, 2005), the Agency expects the first draft of the vinyl acetate IRIS profile available for public review in February 2009; its posting on the EPA IRIS site is expected in July 2009.
<p>
VAC has been working with EPA to ensure that the updated IRIS file is a scientifically defensible assessment of the most up-to-date information on vinyl acetate.
</blockquote>
<P>
<i>Toxic Release Inventory</i>
<p>
<blockquote>
Under the Emergency Planning and Community Right-to-Know Act of 1986 (EPCRA) and expanded by the Pollution Prevention Act of 1990, manufacturers, processors and users that meet EPA's specified regulatory thresholds are required to submit an annual report of emissions from their facilities that are released into the environment or otherwise managed as waste.
<p>
Reported air emissions have reduced over time: 
<p>
<center><img src="/images/airemissions_graph.jpg" width=482 height=279></center>
</blockquote>
<p>
<i>Acute Exposure Guideline Levels	</i>
<p>
<blockquote>
Vinyl acetate is one of 371 chemicals EPA identified in a <a href="aegl_fr_053102.pdf" target="_blank">May 31, 2002 <i>Federal Register</i></a> notice as priority chemicals for Acute Exposure Guideline Level (AEGL) development.
<p>
At its December 2004 meeting, the National Advisory Committee for Acute Exposure Guideline Levels for Hazardous Substances (NAC/AEGL) reviewed the Technical Support Document for vinyl acetate and recommended the following AEGL values for vinyl acetate:
<p align="right">
Source: <a href="http://www.epa.gov/oppt/aegl/rest143.htm" target="_blank">http://www.epa.gov/oppt/aegl/rest143.htm</a></p>
<p>
<center>
<table border=1 cellpadding=2 cellspacing=2 width=70%>
<tr><td valign="top" colspan=6 bgcolor="00CC66"><center><b>Vinyl acetate &nbsp; &nbsp; 108-05-4 &nbsp; &nbsp; (Proposed)</b></center></td>
</tr>
<tr><td valign="top" colspan=6 bgcolor="00CC66"><center><b>(ppm) </b></center></td>
</tr>
<tr><td valign="top" width=17%>&nbsp;</td>
<td valign="top" width=17%>10 min</td>
<td valign="top" width=17%>30 min</td>
<td valign="top" width=17%>60 min</td>
<td valign="top" width=17%>4 hr</td>
<td valign="top" width=17%>8 hr</td>
</tr>
<tr><td valign="top" width=17%><b>AEGL 1</b></td>
<td valign="top" width=17%>6.7</td>
<td valign="top" width=17%>6.7</td>
<td valign="top" width=17%>6.7</td>
<td valign="top" width=17%>6.7</td>
<td valign="top" width=17%>6.7</td>
</tr>
<tr><td valign="top" width=17%><b>AEGL 2</b></td>
<td valign="top" width=17%>230</td>
<td valign="top" width=17%>230</td>
<td valign="top" width=17%>180</td>
<td valign="top" width=17%>110</td>
<td valign="top" width=17%>75</td>
</tr>
<tr><td valign="top" width=17%><b>AEGL 3</b></td>
<td valign="top" width=17%>760</td>
<td valign="top" width=17%>760</td>
<td valign="top" width=17%>610</td>
<td valign="top" width=17%>380</td>
<td valign="top" width=17%>250</td>
</tr>
</table></center>
<p align="right">Level of Distinct Odor Awareness (LOA) = 0.25 ppm</p>
<p>
VAC expects that final AEGL values for vinyl acetate will be published in 2006.
<p>
AEGLs are intended to describe the risk to humans resulting from once-in-a-lifetime, or rare, exposure to airborne chemicals.  They are developed to help both national and local authorities, as well as private companies, deal with emergencies involving spills, or other catastrophic exposures.  Additional information on AEGLs can be found at the <a href="http://www.epa.gov/oppt/aegl/" target="_blank">EPA AEGL website</a>.

</blockquote>


	<!--- Page text ends here --->

</td></tr></table></td>

<?php include('footer.php');?>
