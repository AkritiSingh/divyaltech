<cfparam name="form.username" default="" />
<cfparam name="form.password" default="" />
<cfparam name="URL.destURL" default="/members/index.cfm" />
<cfparam name="URL.destQuery" default="" />

<cfset request.errorMessage = "" />

<cfif len(form.username) AND len(form.password)>
	<cfif application.loginService.validateLogin(form.username, form.password)>
		<cflocation url="#URL.destURL##iif(len(URL.destQuery), DE("?#URL.destQuery#"), "")#" addtoken="false" />
	<cfelse>
		<cfset request.errorMessage = "Login Failed" />
	</cfif>
</cfif> 

<!--- BEGIN Site Header --->
<cfset pagetitle = "Member Login">
<cfset whichsection = "MO">

<cfinclude template="/navigation.cfm">
	<table cellpadding=0 cellspacing=13 border=0 width=100%><tr>
	<TD> 

<!--- END Site Header --->

<h3>Member Login</h3>

<cfif len(request.errorMessage)>
	<div class="ui-widget" style="margin-bottom:2em;">
		<div style="padding: 1em;" class="ui-state-error ui-corner-all"> 
			<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span> 
			<cfoutput>#request.errorMessage#</cfoutput>
		</div>
	</div>
</cfif>

<section>
	<form method="post">
	
		<span class="input">
			<label class="input text" for="username">Username</label>
			<input class="input text" style="width:100px" name="username" id="username" type="text" /><br /><br />		
		</span>
		
		<div class="clear"></div>
		
		<span class="input">
			<label class="input text" for="password">Password</label>
			<input class="input text" style="width:100px" name="password" id="password" type="password" /><br /><br />	
		</span>
	
		<div class="clear"></div>
		
		<span class="input" style="margin:10px 0px;">
			<input class="input button" id="loginButton" type="submit" value="Log In" />
		</span>
		
		<div class="clear"></div>
		
	</form>
</section>

<!--- <p><a href="retrievePassword.cfm">Forgot your username or password?</a></p> --->

<script type="text/javascript">
	$(document).ready(function() {
		$('#loginButton').button();
		$('#loginButton').css('text-decoration', 'none');
		$('#loginButton').css('color', '#444444');		
	});
</script>

	<!--- Page text ends here --->

</td></tr></table></td>

<cfinclude template="../footer.cfm">

