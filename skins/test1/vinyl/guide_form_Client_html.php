<cfoutput>
	<table width="100%" cellpadding="5" cellspacing="0" style="font-family:Verdana,Arial,Helvetica;font-size: 10pt;"><tr>
		<td colspan="2">
			<h3>VAM Safe Handling Guide Order Form</h3>
			<p>The following submission was received from a VAM Safe Handling Guide order form:</p>
		</td>
	</tr><tr>
		<td valign="top"><strong>Name:</strong></td>
		<td valign="top"><?php echo $attributes.firstName $attributes.lastName ?></td>
	</tr><tr>
		<td valign="top"><strong>Email:</strong></td>
		<td valign="top"><?php echo $attributes.contactEmail ?></td>
	</tr><tr>
		<td valign="top"><strong>Company:</strong></td>
		<td valign="top"><?php echo $attributes.company ?></td>		
	</tr><tr>
		<td valign="top"><strong>Address:</strong></td>
		<td valign="top"><?php echo $attributes.address ?></td>
	</tr><tr>
		<td valign="top"><strong>City:</strong></td>
		<td valign="top"><?php echo $attributes.city ?></td>
	</tr><tr>
		<td valign="top"><strong>State:</strong></td>
		<td valign="top"><?php echo $attributes.state ?></td>
	</tr><tr>
		<td valign="top"><strong>Zip Code:</strong></td>
		<td valign="top"><?php echo $attributes.zipcode ?></td>		
	</tr><tr>
		<td valign="top"><strong>Telephone:</strong></td>
		<td valign="top"><?php echo $attributes.telephone ?></td>
	</tr><tr>
		<td valign="top"><strong>Fax:</strong></td>
		<td valign="top"><?php echo $attributes.fax ?></td>	
	</tr><tr>
		<td valign="top"><strong>How is your company associated with VAM:</strong></td>
		<td valign="top"><?php echo $attributes.associated ?></td>

	</tr>		
	</table>
</cfoutput>