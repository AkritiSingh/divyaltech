<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Our Team </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third last">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third last">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="http://placehold.it/340x230.jpg" alt="" title="">
                    </div>
                    <h4> Jennifer Nicole </h4>
                    <h6> Mauris congue </h6>
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/team-social/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
