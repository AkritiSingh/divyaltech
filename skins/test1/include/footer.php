<footer id="footer">
    	<div class="container">
        
			<div class="column one-fourth" style="margin-right:106px"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="index.php" title=""> Our Services </a> </h3>
                    <ul>
                        <li> <a href="services.php" title=""> Web Application Development </a> </li>
                        <li> <a href="services.php" title=""> Mobile Appliacation Development </a> </li>
                        <li> <a href="services.php" title=""> PHP Development</a> </li>
                        <li> <a href="services.php" title=""> SEO &amp; Internet Marketing </a> </li>
                        <li> <a href="services.php" title=""> ColdFusion Development </a> </li>        
                        <li> <a href="services.php" title=""> WordPress Development </a> </li>
                    </ul>
                </aside>   
			</div> 
        	<div class="column one-fourth" style="margin-right:100px">
                <aside class="widget tweetbox"> 
                    <h3 class="widgettitle"> <a href="aboutus.php" title=""> About us </a> </h3>
					<p><strong>Divyal Technology</strong> is a small but group of experienced developers. We’re a powerful network of marketers and developers who endeavour to discover, investigate, build and implement you IT solutions that help your biz succeed online... <a href="aboutus.php" style="color: rgb(107, 142, 35); font-size: 18px;">Read more</a></p>            
                </aside>   
            </div>  
            
            <div class="column one-fourth last">
            	<aside class="widget">
                    <h3 class="widgettitle"> <a href="contactus.php" title=""> Contact Info </a> </h3>
                	<p><i class="icon-home"></i> D/61 Chouhan Town Bhilai, Chhattisgarh, India </p>
                    <p><span class="icon-phone"> </span> Phone : (+91) 8359825066  </p>
                    <p> <span class="icon-envelope-alt"> </span> Email : <a href="mailto:info@divyaltech.com"> info@divyaltech.com </a></p> 
                    <p><a href="skype:manoj.patle3"><span style="vertical-align:middle;"> <img src="images/sociable/skype-add.png" alt="coldfusion divyal technology"> </span>  &nbsp;manoj.patle3</a></p>
                   
                    <ul class="social-icons">
                        <li>
                            <a href="https://www.facebook.com/Divyal-Technology-572881792864790/" target="_blank" title="facebook"> 
                                <img src="images/sociable/hover/facebook.png" alt="divyaltech" title="facebook">
                                <img src="images/sociable/facebook.png" alt="divyaltech" title="facebook">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/u/0/108727414415824265010" target="_blank" title="google+"> 
                                <img src="images/sociable/hover/google.png" alt="divyal technology" title="google+">
                                <img src="images/sociable/google.png" alt="divyal technology" title="google+">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/DivyalTech" target="_blank" title="twitter"> 
                                <img src="images/sociable/hover/twitter.png" alt="twitter divyaltech" title="twitter">
                                <img src="images/sociable/twitter.png" alt="twitter divyaltech" title="twitter">                        
                            </a>
                        </li>
                        <li>
                            <a href="https://dribbble.com/divyaltech" target="_blank" title="dribbble"> 
                                <img src="images/sociable/hover/dribble.png" alt="dribbble" title="dribbble">
                                <img src="images/sociable/dribble.png" alt="dribbble" title="dribbble">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title="" target="_blank"> 
                                <img src="images/sociable/hover/linkedin.png" alt="linkedin divyaltech" title="linkedin">
                                <img src="images/sociable/linkedin.png" alt="linkedin divyaltech" title="linkedin">                        
                            </a>
                        </li>
                    </ul>
                </aside>	
            </div>
            
        </div>
        
        <div class="copyright">        	
        	<div class="container">
                <p style="text-align:center"> Copyright &copy; 2015 Divyal Technology All Rights Reserved. <!--| <a href="" title=""> Design Themes </a--> </p>        	
            </div>
        </div>
    </footer>
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76011742-1', 'auto');
  ga('send', 'pageview');

</script>