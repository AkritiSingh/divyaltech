<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> About </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<div class="page-slider-container">
                <ul class="page-slider">
                    <li> <img src="http://placehold.it/1060x300.jpg" alt="" title=""> </li>
                    <li> <img src="http://placehold.it/1060x300.jpg" alt="" title=""> </li>
                    <li> <img src="http://placehold.it/1060x300.jpg" alt="" title=""> </li>
                </ul>                
            </div>
        
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> Why Choose us <span> </span> </h2> </div>                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set">
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Impeccable Customer Service </a> </h5>
                        <div class="toggle-content">
                            <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Communicates well and often </a> </h5>
                        <div class="toggle-content">
                            <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Honesty and integrity </a> </h5>
                        <div class="toggle-content">
                            <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                        </div>
                    </div>
                	<div class="toggle-frame">                    
                        <h5 class="toggle-accordion"> <a href="#" title=""> Not the Best Numbers but the Best Strategy </a> </h5>
                        <div class="toggle-content">
                            <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                        </div>
                    </div>
                </div> <!-- **Toggle Frame Set - End** -->                 
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Our Skills <span> </span> </h2> </div>
                
                <div class="progress progress-striped">
                	<div data-value="90" class="bar">
                    	<div class="bar-text"> HTML5 &amp; CSS3 <span> 90% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped ">
                	<div data-value="70" class="bar">
                    	<div class="bar-text"> Wordpress <span> 70% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="80" class="bar">
                    	<div class="bar-text"> PHP <span> 80% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="60" class="bar">
                    	<div class="bar-text"> Photoshop <span> 60% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="50" class="bar">
                    	<div class="bar-text"> jQuery <span> 50% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="75" class="bar">
                    	<div class="bar-text"> Web Design <span> 75% </span> </div>
                    </div>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Services <span> </span> </h2> </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-cogs"> </span> </div>
                    <h5> <a href="" title=""> Great Support </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-beaker"> </span> </div>
                    <h5> <a href="" title=""> Clean &amp; Simple </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magic"> </span> </div>
                    <h5> <a href="" title=""> Font Awesome icons </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magnet"> </span> </div>
                    <h5> <a href="" title=""> HTML5 &amp; CSS3 </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-dashboard"> </span> </div>
                    <h5> <a href="" title=""> Boxed &amp; Wide Versions </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5> <a href="" title=""> 7 Predefined Color Schemes </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Partners <span> </span> </h2> </div>
            
            <!-- **Partner Carousel Wrapper** -->
            <div class="partner-carousel-wrapper">
                <ul class="partner-carousel">
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="" title=""> </a> </li>
                </ul>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div><!-- **Partner Carousel Wrapper - End** -->
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>

<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
<script src="js/jquery.viewport.js"></script>
<script src="js/twitter/jquery.tweet.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/clickme.js"></script>
</body>
</html>
