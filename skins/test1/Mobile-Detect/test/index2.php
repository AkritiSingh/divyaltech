<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content=" Divyal Technology is a web development company and offer best solution for web and mobile application development, php, coldfusion, wordpress and HTML & CSS. We are located in Chhattisgarh, India ">
	<meta name="author" content="">
    <meta name="google-site-verification" content="DTPEKOcwPsuvzEPSSZQugE7EZuf0Zk5fWIFKiLnpXNw" />
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />  
    <link rel="stylesheet" href="css/layerslider.css" type="text/css">
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
    <![endif]-->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    	<!-- **Slider Section** -->
    	<section id="slider">
        
            <div id="layerslider-container-fw">    
                <div id="layerslider" style="width:100%; height:200px; margin:0px auto; ">
                
                <div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; transition3d:all;">      
                   
                      <img alt="website design in php" src="images/slider/coldfusion_banner.jpg" class="ls-bg">
                      <img alt="web app development" class="ls-s-1"  src="images/slider/Cold-Fusion-icon.png"  style="position:absolute; top:38px; left:798px; slidedirection:bottom;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                                         
                        <h1 class="ls-s-1" style="position:absolute; top:80px; left:-10px; slidedirection:left;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">ColdFusion </span> Development</h1>
                        
                        <p class="ls-s-1 text3" style="position:absolute; top:140px; left:25.5px; slidedirection:bottom;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;">We build Creative &amp; Effective Solution. <br>
                        			 5+ years experience in all CFML engines(ColdFusion, Railo and OpenBD) and in all major framework(ColdBox, Fusebox,CFWheels and more)..
                        </p>
                        
              </div>
                
                        
                    <div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; transition3d:all;">      
                      <img alt="website design in php" src="images/slider/sliderimages.jpg" class="ls-bg">
                      <img alt="web app development" class="ls-s-1"  src="images/slider/slider_webdevelopment.png"  style="position:absolute; top:79px; left:598px; slidedirection:top;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                     
                        <h1 class="ls-s-1" style="position:absolute; top:80px; left:-10px; slidedirection:top;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">Web </span> Application Development</h1>
                        
                        <h2 class="ls-s-1 text2" style="position:absolute; top:139px; left:25px; slidedirection:left;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;">Interactive Mobile </span> Application Development</h2>
                        
                        <div class="ls-s-1 button-text2"  style="position:absolute; top:190.00001525878906px; left: 165px; display:block; slidedirection:bottom;  durationin:4000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0">
                        <a style="" href="web-application.php"> View More </a>
                      
                      </div>  
                    </div>        
                                        
                    <!--<div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; timeshift:0; transition2d:all; ">
                    
                        <img alt="divyal technology development" src="images/slider/design.jpg" class="ls-bg">
                        <img alt="website development in coldfusion" class="ls-s-1"  src="images/slider/responsive-design.png"  style="position:absolute; top:-55px; left:18px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <h1 class="ls-s-1" style="position:absolute; top:70.00001525878906px; left:485px; slidedirection:top;  durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">Responsive</span> Web Design</h1>
                         <h2 class="ls-s-1 text2" style="position:absolute; top:130.00001525878906px; left:495px; slidedirection:right;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> We Provide a Creative Design Solution. </h2>
                         
                        <h4 class="ls-s-1" style="position:absolute; top:182.00001525878906px; left:500px; slidedirection:right;  durationin:2500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> With a Clean & Modern design </h4>
                       
                    </div>-->
                  
                    <div class="ls-layer"  style="slidedirection:right; slidedelay:4000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition3d:all; ">
                    
                        <img alt="divyaltech web development" src="images/slider/app_development.jpg" class="ls-bg">
                        <img alt="php web development" class="ls-s-1"  src="images/slider/iphone.png"  style="position:absolute; top:25px; left:0px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <p class="ls-s-1 text4" style="position:absolute; top:335px; padding: 0 5px; left:-40px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;"> iphone</span> Application Development</p>
                        <img alt="coldfusion web development" class="ls-s-1"  src="images/slider/Android_app.png"  style="position:absolute; top:25px; left:930px; slidedirection:right;   durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <p class="ls-s-1 text4" style="position:absolute; top:338px; padding: 0 5px; left:850px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;">Android</span> Application Development</p>
                       
                        <h1 class="ls-s-1" style="position:absolute; top:76px; padding: 0 5px; left:250px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;"> Mobile</span> Application Development</h1>
						
                        <h2 class="ls-s-1 text2" style="position:absolute; top:140.00001525878906px; left:230px; slidedirection:left;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> We make innovative Apps That solve your business problem.</h2>
                                                   
                    </div>
                </div>
            </div>
        </section><!-- **Slider Section - End** -->
        
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        	<div class="intro-text type1">
            	<!--h4> Welcome to our <span class="highlight"> Divyal Technology</span> - Software and Web Application Development Company</h4-->
                <h6 style="text-transform: capitalize; font-size: 16px;">A True Coldfusion Development Company Which Takes Care of All Your Online Needs!</h6>
            </div>
            
            <div class="hr-invisible-small"> </div>
			
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5><div style="text-align:center"><a href="services.php" title="">Web Development </a> </div></h5>
                    <div style="text-align:justify"><p> Divyal Technology is a chief web application development organization and we offer web solutions to our clients which are custom-made to achieve their respective goals, vision and mission. </p></div>
                </div>
            </div>
			
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-leaf"> </span> </div>
                    <h5> <div style="text-align:center"><a href="services.php" title="">Digital Marketing </a></div> </h5>
                    <div style="text-align:justify"><p> Digital marketing has approached business intelligence to discover the right strategy and we act as a proficient digital marketing agency and provide our clients better structural digital marketing platform </p></div>
                </div>
            </div>
            
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-laptop"> </span> </div>
                    <h5> <div style="text-align:center"><a href="services.php" title=""> Fully Responsive </a> </div></h5>
                    <div style="text-align:justify"><p>At Divyaltech, We take your business to next level by focusing on both desktop &amp; mobile traffic with responsive web. We produce Fully Responsive Websites with intelligent user interface and amazing user experience. </p></div>
                </div>
            </div>
			
			<div class="column one-fourth last">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-mobile-phone"> </span> </div>
                    <h5> <div style="text-align:center"><a href="services.php" title=""> Mobile App Development </a></div></h5>
                    <div style="text-align:justify"><p>Divyal Technology provide you PhoneGap Development services to build your app to by expertise PhoneGap Developer. </p></div>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>
            
            <div class="border-title"> <h2> Latest from Portfolio <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/add1.JPG" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/add1.JPG" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/add2.JPG" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								
								<a href="images/addictive.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet2.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/add5.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Addictive Polls</a> </h5>
                            <p> www.addictivepolls.com </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/econo.JPG" alt="wordpress development" title="">
                            <div class="image-overlay">
                                <a href="images/Econo.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/Econo3.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/mobile2.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab3.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab2.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
                                <a href="http://econorentals.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Econo Auto Rental </a> </h5>
                            <p>www.econorentals.com</p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="images/spritualbri1.png" alt="" title="">
                        <div class="image-overlay">
							<a href="images/spritual.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritual2.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
							
							<a href="images/spritual3.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritual4.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualContact.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualAdvertising.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualdesktop.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualipad.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>			
								<a href="http://spiritualbridge.org/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Spiritual Bridge </a> </h5>
                            <p> SPIRITUALBRIDGE.ORG </p>
                        </div>
                    </li>
                    <!--li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="website development in php" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="web app development" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li-->
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                    	<img src="images/harrow11.png" alt="" title="">
                        <div class="image-overlay">
                          <a href="images/harrowlegal.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                          </a>
						<a href="images/harrow2.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
						<a href="images/harrow3.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
		 
						<a href="images/harrow4.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
						<a href="images/harrow5.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
                            <a href="http://www.harrowlegal.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.harrowlegal.com/" title=""> Harrow Law Group P.A. </a> </h5>
                        <p> WWW.HARROWLEGAL.COM </p>
                    </div>
                    </li>
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!--**Portfolio Carousel Wrapper - End** -->
            
            <!-- <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> From the blog <span> </span> </h2> </div>
            
            <div class="column one-half">-->
                <!-- **Blog Entry** -->
               <!-- <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="divyal Technology website" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            <div class="categories"> <span class="icon-pushpin"> </span> <a href="" title=""> Business</a>, <a href="" title=""> Outdoors </a> </div>-->
                       <!-- </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article>--> <!-- **Blog Entry - End** -->
         <!--   </div>
            
            <div class="column one-half last">-->
                <!-- **Blog Entry** -->
          <!--      <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="web app development" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article>
				--> <!-- **Blog Entry - End** -->
            <!--</div> -->
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> FAQ's <span> </span> </h2> </div>
                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set faq">                  
                    <h5 class="toggle-accordion"> <a href="#" title="">Why we choose you/ why should we work with you?</a> </h5>
                    <div class="toggle-content">
                        <p> We are the team of very good experienced developers, designers. We take care of all your
                            requirements, your business need and provide best solutions. We follow the process of
                            understanding your issues and requirements first and then code.
                        </p>
                    </div>
                 
                    <h5 class="toggle-accordion"> <a href="#" title="">What is the quality of people you employ?</a> </h5>
                    <div class="toggle-content">
                        <p> Our all peoples are well educated and post graduated in Commuters and engineering. Well
                            experienced.
                            <ul>
                              <li>20+ Man-Years Experience in ColdFusion and web technologies.</li>
                              <li>Our people are flexible working at your working hours if your need.</li>
                              <li>All people have experience on off-shore projects with US/European clients.</li>
                              <li>All the projects QA are done by highly experienced Team Leads before deliver to you.</li>
                            </ul>
                        </p>
                    </div>
                   
                   <h5 class="toggle-accordion"> <a href="#" title=""> What’s you rates and payment terms?</a> </h5>
                    <div class="toggle-content">
                        <p>We follow different payment models, we work fixed price as well as hourly and monthly bases.</p><br>
                        <p><strong>Fixed Price- </strong>It’s based on project requirement and size of the project. So please                           write us for quote with your requirements.
                        </p>
                           <ul>
                              <li>We take 20% of project cost on start of project.</li>
                              <li>40% on first release.</li>
                              <li>40% on final release and confirmation.</li>
                           </ul>
                           
                     <p><strong>Hourly Rates-</strong>–Our hourly charges are between $10-$15 based on technical resource you
                      need and project complexity. Our Resource based charges are as below for your idea -
                     </p>
                           <ul>
                              <li>$15/ hour for 5+ year’s senior developer, Team Leads.</li>
                              <li>$13/ hour for 3+ year’s developers or designers.</li>
                              <li>$10/ hour for junior developers/designers.</li>
                           </ul>  
                           
                           <p>
                           <strong>Monthly Rates – </strong>Our monthly rates are starts from $1000 to $2000 per month depending                            on availability your need and resources experience level or project complexity.
                           </p>
                    </div>
               
                     <h5 class="toggle-accordion"> <a href="#" title="">How you provide support remotely if we 
                         need it instantly?</a> </h5>
                    <div class="toggle-content">
                        <p>We use skype for instant support and discussion. You can contact us via email or call in skype. Our
                           offices are open 15 hours a day.<br>
                           If you need support at off hours or on weekends you will have our phone numbers to call our
                           support team ask for instant support with details. Support team will engage a developer for you to
                           get your issue resolve or job done within half an hour.
                        </p>
                    </div>                     
                </div> <!-- **Toggle Frame Set - End** --> 
                
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Testimonials <span> </span> </h2> </div>
                
                <div class="testimonial">
                    <div class="author">
                        <img src="images/text2image.jpg" alt="divyal Technology website" title="">
                    </div>                       
                    <blockquote>
                    	<q> I tested Divyal Technologies and every time I had issues, I got almost immediate responses.  With a 12 hour time difference, I was truly skeptical.  I don’t know how it’s done, but I get responses within minutes, not hours, relieving my stress.I am so pleased with the work.


</q>
                        <cite> Marc L.<br>
                            Online Technologies </cite>
                    </blockquote>
                </div>
                <div class="testimonial">
                    <div class="author">
                        <img src="images/testimonial.jpg" alt="divyaltech website development" title="">
						
                    </div>                       
                    <blockquote>
                    	<q> Manoj has helped me with many web projects over several years. He has always been highly professional, communicative and able to deliver projects on time and on budget. Highly recommended. </q>
                        <cite> John Carratt<br>
						 SeeInside.co.uk
						</cite>
                    </blockquote>
                </div>
            </div>
            
            <!--<div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Partners <span> </span> </h2> </div>-->
            
            <!-- **Partner Carousel Wrapper** -->
            <!--<div class="partner-carousel-wrapper">
                <ul class="partner-carousel">
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="Divyaltech web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="seo and social media marketing" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="ecommerce website design" title=""> </a> </li>
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="wordpress web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="cold fusion website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="website development in codeigniter" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php and coldfusion web development" title=""> </a> </li>
                </ul>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div>--> <!-- **Partner Carousel Wrapper - End** -->
            
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

<!-- Layer Slider -->
<script src="js/jquery-easing-1.3.js"></script>
<script src="js/jquery-transit-modified.js"></script>
<script src="js/layerslider.transitions.js"></script>
<script src="js/layerslider.kreaturamedia.jquery.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery('#layerslider').layerSlider({
			skinsPath : 'layerslider-skins/',
			skin : 'fullwidthdark',
			thumbnailNavigation : 'hover',
			hoverPrevNext : false,
			responsive : false,
			responsiveUnder : 1120,
			sublayerContainer : 1060,
			width : '100%',
			height : '400px'
		});
	});		
</script>		



</body>
</html>
