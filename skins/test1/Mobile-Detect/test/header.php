<header id="header">
    
    	<!-- **Top Bar** -->
    	<!--div id="top-bar">
        	<div class="container">
            	<p class="phone-no"> (+91) 788-2225066 <a href="" title=""> info@divyaltech.com </a> </p>
                <ul class="social-icons">
                    <li>
                        <a href="https://www.facebook.com/Divyal-Technology-572881792864790/" title="facebook"> 
                            <img src="images/sociable/hover/facebook.png" alt="" title="facebook">
                            <img src="images/sociable/facebook.png" alt="" title="facebook">                        
                        </a>
                    </li>
                    <li>
                        <a href="" title="google+"> 
                            <img src="images/sociable/hover/google.png" alt="" title="google+">
                            <img src="images/sociable/google.png" alt="" title="google+">                        
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/DivyalTech" title="twitter"> 
                            <img src="images/sociable/hover/twitter.png" alt="" title="twitter">
                            <img src="images/sociable/twitter.png" alt="" title="twitter">                        
                        </a>
                    </li>
                    <li>
                        <a href="https://dribbble.com/divyaltech" title="dribbble"> 
                            <img src="images/sociable/hover/dribble.png" alt="" title="dribbble">
                            <img src="images/sociable/dribble.png" alt="" title="dribbble">                        
                        </a>
                    </li>
                    <li>
                        <a href="" title="linkedin"> 
                            <img src="images/sociable/hover/linkedin.png" alt="" title="linkedin">
                            <img src="images/sociable/linkedin.png" alt="" title="linkedin">                        
                        </a>
                    </li>
                </ul>
            </div>
        </div--><!-- **Top Bar - End** -->
        
        <div class="container">
        	<!-- **Logo - End** -->
            <div id="logo">
            	<a href="index.php" title=""><img src="images/logo1.png" alt="Divyal Technology"></a>
            </div><!-- **Logo - End** -->
            
            <!-- **Navigation** -->
            <nav id="main-menu">
            	<ul>
					<li <?php if (ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Index'){?>class="current_page_item"<?php }?> >
					 <a href="index.php" title="">  Home </a> <span> </span>                     
						<!-- <ul>
							<li <?php if (ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Index'){?>class="current_page_item" <?php }?>> 
							<a href="index.php" title=""> Home Example 1 </a> </li>
							<li>
							 <a href="index-v2.php" title=""> Home Example 2 </a> </li>
							<li >
							 <a href="index-v3.php" title=""> Home Example 3 </a> </li>
							<li >
							 <a href="index-v4.php" title=""> Home Example 4 </a> </li>
						</ul> -->                           
                    </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'],PATHINFO_FILENAME))=='About'){?>class="current_page_item"<?php }?>>
					 <a href="aboutus.php" title=""> About </a> <span> </span> </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Side-navigation'){ ?>class="current_page_item"<?php }?>>
					 <a href="services.php" title=""> Services </a> <span> </span> 
                    	<!--<ul>
                            <?php /*?><li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Side-navigation'){ ?>class="current_page_item"<?php }?>><?php */?>
							 <a href="side-navigation.php" title=""> 100% Fully Responsive </a> </li>
                            <li>
							 <a href="features-layouts.php" title=""> Boxed &amp; Wide Versions </a> </li>
                            <li>
							 <a href="features-home.php" title=""> 4 Home Page Options </a> </li>
                            <li>
							 <a href="features-icons.php" title=""> Font Awesome Icons </a> </li>
                            <li>
							 <a href="features-valid.php" title=""> Valid HTML5 and CSS3 </a> </li>
                            <li>
							 <a href="features-contact.php" title=""> 2 Contact Layouts </a> </li>
                            <li>
							 <a href="features-blog.php" title=""> Blog Page Options </a> </li>
                            <li>
							 <a href="features-html.php" title=""> Tons of HTML Elements </a> </li>
                            <li>
							 <a href="features-portfolio.php" title=""> Different Portfolio Columns </a> </li>
                            <li>
							 <a href="features-sliders.php" title=""> 2 Premium Sliders </a> </li>
                            <li>
							 <a href="features-googlefonts.php" title=""> Google Web Fonts </a> </li>
                        </ul>-->
                    </li>

                   <!-- <li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Shortcodes-typography'){ ?>class="current_page_item"<?php// }?>>
					 <a href="shortcodes-typography.php" title=""> Pages </a> <span> </span> 
						<ul>
							<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Shortcodes-typography'){ ?>class="current_page_item"<?php// }?>>
							 <a href="shortcodes-typography.php" title=""> Shortcodes </a>                             
								<ul>
									<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Shortcodes-typography'){ ?>class="current_page_item"<?php// }?>>
									 <a href="shortcodes-typography.php" title=""> Typography </a> </li>
									<li>
									 <a href="shortcodes-pricing-tables.php" title=""> Pricing Tables </a> </li>
									<li>
									 <a href="shortcodes-progress-bars.php" title=""> Progress Bars </a> </li>
									<li>
									 <a href="shortcodes-tabs-toggles.php" title=""> Tabs &amp; Toggles </a> </li>
									<li>
									 <a href="shortcodes-buttons.php" title=""> Buttons </a> </li>
									<li>
									 <a href="shortcodes-dropcap.php" title=""> Dropcap </a> </li>
									<li>
									 <a href="shortcodes-fancyboxes.php" title=""> Fancy Boxes </a> </li>
									<li>
									 <a href="shortcodes-form-elements.php" title=""> Form elements </a> </li>
									<li>
									 <a href="shortcodes-highlighters.php" title=""> Highlighters </a> </li>
									<li>
									 <a href="shortcodes-lists.php" title=""> Unordered Lists </a> </li>
									<li>
									 <a href="shortcodes-ordered-lists.php" title=""> Ordered Lists </a> </li>
									<li>
									 <a href="shortcodes-quotes.php" title=""> Quotes </a> </li>
								</ul>
							</li>
							<li>
							 <a href="team.php" title=""> Team </a> </li>
							<li>
							 <a href="side-navigation.php" title=""> Side Navigation </a> </li>
							<li>
							 <a href="404.php" title=""> 404 </a> </li>
						</ul>    
                    </li>-->
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Portfolio-four-column'){ ?>class="current_page_item"<?php }?>> <a href="portfolio.php" title=""> Portfolio </a> <span> </span> 
                    	<!--<ul>
                        	<li <?php// if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Portfolio-four-column'){ ?>class="current_page_item"<?php //}?>> <a href="portfolio-four-column.php" title=""> Four Column </a>                             
                            	<ul>
                                	<li <?php// if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Portfolio-four-column'){ ?>class="current_page_item"<?php //}?>> <a href="portfolio-four-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="portfolio-four-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="portfolio-four-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="portfolio-three-column.php" title=""> Three Column </a>                             
                            	<ul>
                                	<li> <a href="portfolio-three-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="portfolio-three-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="portfolio-three-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="portfolio-two-column.php" title=""> Two Column </a>                             
                            	<ul>
                                	<li> <a href="portfolio-two-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="portfolio-two-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="portfolio-two-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="portfolio-one-column.php" title=""> One Column </a>                             
                            	<ul>
                                	<li> <a href="portfolio-one-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="portfolio-one-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="portfolio-one-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="portfolio-single.php" title=""> Portfolio Single </a>                             
                            	<ul>
                                	<li> <a href="portfolio-single.php" title=""> Layout 1 </a> </li>
                                    <li> <a href="portfolio-single2.php" title=""> Layout 2 </a> </li>
                                </ul>                            
                            </li>
                        </ul> -->
                    </li>
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Blog-three-column'){ ?>class="current_page_item"<?php }?>> <a href="our-clients.php" title=""> Our Clients </a> <span> </span>                     
                    	<!--<ul>
                        	<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Blog-three-column'){ ?>class="current_page_item"<?php// }?>> <a href="blog-three-column.php" title=""> Three Column </a>                             
                            	<ul>
                                	<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Blog-three-column'){ ?>class="current_page_item"<?php// }?>> <a href="blog-three-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="blog-three-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="blog-three-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="blog-two-column.php" title=""> Two Column </a>                             
                            	<ul>
                                	<li> <a href="blog-two-column.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="blog-two-column-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="blog-two-column-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="blog-fullwidth.php" title=""> Fullwidth image </a>                             
                            	<ul>
                                	<li> <a href="blog-fullwidth.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="blog-fullwidth-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="blog-fullwidth-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="blog-thumb.php" title=""> Thumb image </a>                             
                            	<ul>
                                	<li> <a href="blog-thumb.php" title=""> Without Sidebar </a> </li>
                                    <li> <a href="blog-thumb-with-left-sidebar.php" title=""> With Left Sidebar </a> </li>
                                    <li> <a href="blog-thumb-with-right-sidebar.php" title=""> With Right Sidebar </a> </li>
                                </ul>                            
                            </li>
                        	<li> <a href="blog-single.php" title=""> Blog Single </a> </li>
                        </ul>  -->                  
                    </li>
                    <!--<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Products'){ ?>class="current_page_item"<?php //}?>> <a href="products.php" title=""> Shop </a> <span> </span> 
                    
                    	<ul>
                        	<li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Products'){ ?>class="current_page_item"<?php //}?>> <a href="products.php" title=""> Product List </a> </li>
                        	<li> <a href="products2.php" title=""> Product List type 2 </a> </li>
                        	<li> <a href="product-detail.php" title=""> Product Detail </a> </li>
                        </ul>                    
                    
                    </li>-->
                    <li <?php if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Contact'){ ?>class="current_page_item"<?php }?>> <a href="contactus.php" title=""> Contact Us </a> <span> </span> 
                        <!--<ul>
                            <li <?php //if(ucfirst(pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME))=='Contact'){ ?>class="current_page_item"<?php// }?>> <a href="contact.php" title=""> Layout 1 </a> </li>
                            <li> <a href="contact2.php" title=""> Layout 2 </a> </li>
                        </ul>-->                            
                    </li>
                </ul>
            </nav><!-- **Navigation - End** -->      
        </div>
    	
    </header>