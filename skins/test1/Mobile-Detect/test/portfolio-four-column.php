<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />    
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Portfolio Four Column </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<div class="sorting-container">
            	<a href="#" title="" class="active-sort" data-filter=".all-sort"> All </a>
                <a href="#" title="" data-filter=".webdesign-sort"> PHP </a>
                <a href="#" title="" data-filter=".html-sort"> Cold Fusion </a>
                <a href="#" title="" data-filter=".woocommerce-sort"> Code Ignitor </a>
                <a href="#" title="" data-filter=".blog-sort"> Wordpress </a>
                <a href="#" title="" data-filter=".news-sort"> Mura </a>
                <a href="#" title="" data-filter=".photography-sort"> HTML </a>
            </div>
            
        	<div class="portfolio-container gallery">        
            	<div class="portfolio four-column all-sort html-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/glad1.png" alt="" title="">
                        <div class="image-overlay">
                            <a href="images/glad2.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/glad3.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/glad4.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/glad8.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/glad9.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
					  </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.glad.net/" title="">Gladiator Distribution	</a> </h5>
                        <p> WWW.GLAD.NET</p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort html-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/Scooter.png" alt="" title="">
						<div class="single">
                        <div class="image-overlay">
                          <a href="images/scootrlnk2.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
		     			  <a href="images/scootrlnk3.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/scootrlnk4.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/scootrlnk5.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/scootrlnk6.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/scootrlnk7.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/scootrlnk8.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a> 
						  <a href="images/scootrlnk9.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						 <a href="images/scootrlnk10.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a> 
						 <a href="images/scootrlnk11.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						 <a href="images/scootrlnk12.png" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a> 
						  
						   
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
						</div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.scooterlink.com/" title=""> Scooter Link </a> </h5>
                        <p> WWW.SCOOTERLINK.COM </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/spritualbri1.png" alt="" title="">
                        <div class="image-overlay">
                        <a href="images/spritualbri2.png" data-gal="prettyPhoto[news-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
              			<a href="images/spritualbri3.png" data-gal="prettyPhoto[news-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
						
			<a href="images/spritualbri4.png" data-gal="prettyPhoto[news-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
			<a href="images/spritualbri5.png" data-gal="prettyPhoto[news-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
			<a href="images/spritualbri6.png" data-gal="prettyPhoto[news-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>			
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://spiritualbridge.org/" title=""> Spiritual Bridge </a> </h5>
                        <p>SPIRITUALBRIDGE.ORG</p>
                    </div>
                </div>  
            	<div class="portfolio four-column all-sort blog-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/harrow11.png" alt="" title="">
                        <div class="image-overlay">
                          <a href="images/harrow22.png" data-gal="prettyPhoto[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                          </a>
						<a href="images/harrow33.png" data-gal="prettyPhoto[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
						<a href="images/harrow44.png" data-gal="prettyPhoto[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
		 
						<a href="images/harrow66.png" data-gal="prettyPhoto[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.harrowlegal.com/" title=""> Harrow Law Group P.A. </a> </h5>
                        <p> WWW.HARROWLEGAL.COM </p>
                    </div>
                </div>
            	
				<div class="portfolio four-column all-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/home.png" alt="" title="">
                        <div class="image-overlay">
                        <a href="images/home1.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						<a href="images/home2.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						<a href="images/home3.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						<a href="images/home4.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
					   <a href="images/home5.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
					   <a href="images/home6.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a> 
					   <a href="images/home7.png" data-gal="prettyPhoto[woocommerce-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a> 
					    <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.homefrontmagazine.com/" title="">Home Front Magazine </a> </h5>
                        <p> www.homefrontmagazine.com </p>
                    </div>
                </div>
            	
				
				<div class="portfolio four-column all-sort photography-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/andyb.png" alt="" title="">
                        <div class="image-overlay">
                      <a href="images/andyb1.png" data-gal="prettyPhoto[photography-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
					  <a href="images/andyb2.png" data-gal="prettyPhoto[photography-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
					  <a href="images/andyb3.png" data-gal="prettyPhoto[photography-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://andybaerselman.net/" title=""> ANDY BAERSELMAN </a> </h5>
                        <p> andybaerselman.net </p>
                    </div>
                </div> 
				
            	<?php /* <div class="portfolio four-column all-sort html-sort blog-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort blog-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
            	<div class="portfolio four-column all-sort news-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort news-sort woocommerce-sort photography-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
            	<div class="portfolio four-column all-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort photography-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
        	</div> <?php */?>    
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>   
            
            <div class="pagination"> 
            	<ul>
                    <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                	<li> <a href="" title=""> 1 </a> </li>
                    <li class="active-page"> 2 </li>
                    <li> <a href="" title=""> 3 </a> </li>
                    <li> <a href="" title=""> 4 </a> </li>
                    <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                </ul>
            </div>
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/isotope.js"></script>
<script src="js/jquery.smartresize.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
