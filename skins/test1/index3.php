<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title> Divyal Technologies </title>
	
	<meta name="description" content=" Divyal Technology is a web development company and offer best solution for web and mobile application development, php, coldfusion, wordpress and HTML & CSS. We are located in Chhattisgarh, India ">
	<meta name="author" content="">
    <meta name="google-site-verification" content="DTPEKOcwPsuvzEPSSZQugE7EZuf0Zk5fWIFKiLnpXNw" />
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />  
    <link rel="stylesheet" href="css/layerslider.css" type="text/css">
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
    <![endif]-->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css" media="all" >
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
 <!--service -->
    <!-- style css -->
    <link rel="stylesheet" href="css/slider/style1.css" media="all">
    <link rel="stylesheet" href="css/slider/style.1.css">
    <link rel="stylesheet" href="css/slider/font-awesome.min.css" media="all"> 
    <!-- bootstrap v3.3.6 css -->
    
	  <link rel="stylesheet" href="css/slider/bootstrap.min.css"  media="all">
     
    
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'> 
    <!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script--->
  
    
    
</head>




<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main" >
    	<!-- **Slider Section** -->
    	<section id="slider">
        
            <div id="layerslider-container-fw">    
                <div id="layerslider" style="width:100%; height:1000px; margin:0px auto; ">
                
                      <div class="ls-layer"  style="slidedirection:top; slidedelay:5000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0;">      
                   
                      <img alt="website design in php" src="images/slider/coldfusion_banner.jpg" class="ls-bg">
                      <img alt="web app development" class="ls-s-1"  src="images/slider/Cold-Fusion-icon.png"  style="position:absolute; top:38px; left:798px; slidedirection:bottom;   durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                                         
                        <h1 class="ls-s-1" style="position:absolute; top:80px; left:-10px; slidedirection:left;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">ColdFusion </span> Development</h1>
                        
                        <p class="ls-s-1 text3" style="position:absolute; top:140px; left:25.5px; slidedirection:bottom;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;">We build Creative &amp; Effective Solution. <br>
                        			 5+ years experience in all CFML engines(ColdFusion, Railo and OpenBD) and in all major framework(ColdBox, Fusebox,CFWheels and more)..
                        </p>
                        
                     </div>
                
                        
                     <div class="ls-layer"  style="slidedirection:top; slidedelay:5000; durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0;">      
                      <img alt="website design in php" src="images/slider/sliderimages.jpg" class="ls-bg">
                      <img alt="web app development" class="ls-s-1"  src="images/slider/slider_webdevelopment.png"  style="position:absolute; top:79px; left:598px; slidedirection:top;   durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                     
                        <h1 class="ls-s-1" style="position:absolute; top:80px; left:-10px; slidedirection:top;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">Web </span> Application Development</h1>
                        
                        <h2 class="ls-s-1 text2" style="position:absolute; top:139px; left:25px; slidedirection:left;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;">Interactive Mobile </span> Application Development</h2>
                        
                        <div class="ls-s-1 button-text2"  style="position:absolute; top:190.00001525878906px; left: 165px; display:block; slidedirection:bottom;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0">
                        <a style="" href="web-application.php"> View More </a>
                      </div>  
                    </div>        
                                        
                    <!--<div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; timeshift:0; transition2d:all; ">
                    
                        <img alt="divyal technology development" src="images/slider/design.jpg" class="ls-bg">
                        <img alt="website development in coldfusion" class="ls-s-1"  src="images/slider/responsive-design.png"  style="position:absolute; top:-55px; left:18px; slidedirection:left;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <h1 class="ls-s-1" style="position:absolute; top:70.00001525878906px; left:485px; slidedirection:top;  durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;">Responsive</span> Web Design</h1>
                         <h2 class="ls-s-1 text2" style="position:absolute; top:130.00001525878906px; left:495px; slidedirection:right;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> We Provide a Creative Design Solution. </h2>
                         
                        <h4 class="ls-s-1" style="position:absolute; top:182.00001525878906px; left:500px; slidedirection:right;  durationin:2500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> With a Clean & Modern design </h4>
                       
                    </div>-->
                  
                    <div class="ls-layer"  style="slidedirection:right; slidedelay:5000; durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; ">
                    
                        <img alt="divyaltech web development" src="images/slider/app_development.jpg" class="ls-bg">
                        <img alt="php web development" class="ls-s-1"  src="images/slider/iphone.png"  style="position:absolute; top:25px; left:0px; slidedirection:left;   durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <p class="ls-s-1 text4" style="position:absolute; top:335px; padding: 0 5px; left:-40px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;"> iphone</span> Application Development</p>
                        <img alt="coldfusion web development" class="ls-s-1"  src="images/slider/Android_app.png"  style="position:absolute; top:25px; left:930px; slidedirection:right;   durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">
                        <p class="ls-s-1 text4" style="position:absolute; top:338px; padding: 0 5px; left:850px; slidedirection:fade;  durationin:3500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:red;font-weight:bold;">Android</span> Application Development</p>
                       
                        <h1 class="ls-s-1" style="position:absolute; top:76px; padding: 0 5px; left:250px; slidedirection:fade;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"><span style="color:black;font-weight:bold;"> Mobile</span> Application Development</h1>
						
                        <h2 class="ls-s-1 text2" style="position:absolute; top:140.00001525878906px; left:230px; slidedirection:left;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> We make innovative Apps That solve your business problem.</h2>
                                                   
                    </div>
                </div>
            </div>
        </section><!-- **Slider Section - End** -->
              
        <!-- **Container** -->
        
       
     
        
        <!-- Start Service area -->
		<div class="services-area area-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
					        <h6 class="small-title">What we offer</h6>
						    <h3>Our Services</h3>
						    <p>Our consultants opt in to the projects they genuinely want to work on, committing wholeheartedly to delivering.</p>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="services-all">
                    	<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
						   <div class="single-services">
								<a class="service-images" href="#"><img src="img/service/cf1.png" alt="coldfusion development"></a>
								<div class="service-content">
									<h4><a href="#">COLDFUSION DEVELOPMENT</a></h4>
									<p align="justify">We have expert team of ColdFusion developers who dedicated work on your product..<a href="coldfusion-development.php">Read More</a></p>
								</div>
							</div>
						</div>
					    <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><img src="img/service/mobile.png" alt="mobile development"></a>
								<div class="service-content">
									<h4><a href="#">MOBILE APP DEVELOPMENT</a></h4>
									<p align="justify">Our web developers have wide involvement in site improvement and web designing..<a href="mobile-development.php">Read More</a></p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
							   <a class="service-images" href="#"><img src="img/service/java.png" alt="Java development"></a>
								<div class="service-content">
									<h4><a href="#">Java APP Development</a></h4>
									<p align="justify">Divyaltech provides dedicated services for developing web and mobile application development in Java tailored..<a href="java-development.php">Read More</p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><img src="img/service/php.png" alt="php development"></a>
								<div class="service-content">
									<h4><a href="#">PHP Web Development</a></h4>
									<p align="justify">Divyaltech is a leading PHP development company offering extensive scope of custom of PHP application..<a href="php-development.php">Read More</a></p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
								<a class="service-images" href="#"><img src="img/service/desktop.png" alt="desktop development"></a>
								<div class="service-content">
									<h4><a href="#">Desktop APP Development</a></h4>
									<p align="justify">Desktop stands for outwardly rich, exceptionally quick and responsive programming..<a href="desktop-application.php">Read More</a></p>
								</div>
							</div>
						</div>
						<!-- Start services -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-services">
                                <!-- <a class="service-images" href="#"><img src="img/service/setting.png" alt=" setting"></a> -->
                                <a class="service-images" href="#"><i class="fa fa-gear"></i></a>
                                 <div class="service-content">
									<h4><a href="#">Website Support and Maintenance</a></h4>
									<p align="justify">Our web developers have wide involvement in site improvement and web designing.<a href="web-application.php">Read More</a></p>
								</div>
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
        <!-- End Service area -->
        
        
        
        <!-- **Container** -->
        <div class="container">
        <div class="border-title"> <h2> Latest from Portfolio <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">
                    <!--li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/add1.JPG" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/add1.JPG" data-gal="prettyPhoto[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/add2.JPG" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								
								<a href="images/addictive.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet2.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/tablet.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/add5.png" data-gal="prettyPhoto[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Addictive Polls</a> </h5>
                            <p> www.addictivepolls.com </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/econo.JPG" alt="wordpress development" title="">
                            <div class="image-overlay">
                                <a href="images/Econo.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/Econo3.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/mobile2.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab3.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tab2.png" data-gal="prettyPhoto1[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
                                <a href="http://econorentals.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Econo Auto Rental </a> </h5>
                            <p>www.econorentals.com</p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="images/spritualbri1.png" alt="" title="">
                        <div class="image-overlay">
							<a href="images/spritual.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritual2.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
							
							<a href="images/spritual3.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritual4.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualContact.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualAdvertising.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualdesktop.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
							<a href="images/spritualipad.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>			
								<a href="http://spiritualbridge.org/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Spiritual Bridge </a> </h5>
                            <p> SPIRITUALBRIDGE.ORG </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="website development in php" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="web app development" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                    	<img src="images/harrow11.png" alt="" title="">
                        <div class="image-overlay">
                          <a href="images/harrowlegal.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                          </a>
						<a href="images/harrow2.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
						<a href="images/harrow3.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
		 
						<a href="images/harrow4.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
						<a href="images/harrow5.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span>                       </a>
                            <a href="http://www.harrowlegal.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.harrowlegal.com/" title=""> Harrow Law Group P.A. </a> </h5>
                        <p> WWW.HARROWLEGAL.COM </p>
                    </div>
                    </li-->
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/socialsense/socialsense1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                               
                                <a href="images/portfolio/socialsense/socialsense6.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="link"> <span class="icon-fullscreen"> </span> </a>								
								<a href="images/portfolio/socialsense/socialsense2.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense4.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense5.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/socialsense/socialsense1.png" data-gal="prettyPhoto4[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                               
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Social Sense</a> </h5>
                            <p> www.socialsense.outerdata.com </p>
                        </div>
                    </li>
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/elite-dealers/elite1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/portfolio/elite-dealers/elite2.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/elite-dealers/elite3.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>								
								<a href="images/portfolio/elite-dealers/elite4.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/elite-dealers/Responsive-elite.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/elite-dealers/elite1.png" data-gal="prettyPhoto5[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Elite Dealers</a> </h5>
                            <p> www.elite-dealers.com </p>
                        </div>
                    </li>
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/seaweedhealthnews/seasweet1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                                
								<a href="images/portfolio/seaweedhealthnews/seasweet2.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/seasweet4.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                  <a href="images/portfolio/seaweedhealthnews/seasweet6.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/Responsive-seaweed.png" data-gal="prettyPhoto6[]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/seaweedhealthnews/seasweet1.png" data-gal="prettyPhoto6[]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                              
                                							
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Sea Weed Health News</a> </h5>
                            <p> www.seaweedhealthnews.com </p>
                        </div>
                    </li>
                      <!--li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/pos/pos1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/portfolio/pos/pos2.png" data-gal="prettyPhoto7[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/pos/pos3.png" data-gal="prettyPhoto7[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								
								<a href="images/portfolio/pos/pos4.png" data-gal="prettyPhoto7[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>								
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> POS</a> </h5>   
                             <p>  </p>                        
                        </div>
                    </li>
                      <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="images/portfolio/ticket/ticket1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/portfolio/ticket/Screen Shot 2016-07-05 at 14.12.33.png" data-gal="prettyPhoto8[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/ticket/Screen Shot 2016-07-05 at 14.13.47.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								
								<a href="images/portfolio/ticket/ticket2.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
								<a href="images/portfolio/ticket/ticket3.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>								
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Ticket System</a> </h5>
                            <p>  </p>
                        </div>
                    </li -->
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!--**Portfolio Carousel Wrapper - End** -->
            
            <!-- <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> From the blog <span> </span> </h2> </div>
            
            <div class="column one-half">-->
                <!-- **Blog Entry** -->
               <!-- <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="divyal Technology website" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            <div class="categories"> <span class="icon-pushpin"> </span> <a href="" title=""> Business</a>, <a href="" title=""> Outdoors </a> </div>-->
                       <!-- </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article>--> <!-- **Blog Entry - End** -->
         <!--   </div>
            
            <div class="column one-half last">-->
                <!-- **Blog Entry** -->
          <!--      <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.php" title=""> <img src="http://placehold.it/340x230.jpg" alt="web app development" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.php" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.php" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.php" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article>
				--> <!-- **Blog Entry - End** -->
            <!--</div> -->
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            <div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="section-headline text-center">
								<h6 class="small-title">what client say</h6>
								<h3>Testimonial</h3>
								<p>Our consultants opt in to the projects they genuinely want to work on, committing wholeheartedly to delivering.</p>
							</div>
						</div>
					</div>
            
            <div class="column one-half">
                 <div class="testimonial">
                    <div class="author">
                        <img src="images/testimonial.jpg" alt="divyaltech website development" title="">
						
                    </div>                       
                    <blockquote align="justify">
                        <q> Manoj has helped me with many web projects over several years. He has always been highly professional, communicative and able to deliver projects on time and on budget. </q>
                        <div class="client-rating">
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class=".icon icon-star-empty"></i></a>
											<a href="#"><i class=".icon icon-star-empty"></i></a>
										</div> 
                        <cite> John Carratt<br>
						 SeeInside.co.uk
						</cite>
                    </blockquote>
                </div>
              
                
            </div>
            
            <div class="column one-half last">
               
                
                <div class="testimonial">
                    <div class="author">
                        <img src="images/text2image.jpg" alt="divyal Technology website" title="">
                    </div>                       
                    <blockquote align="justify">
                    	<q> I tested Divyal Technologies and every time I had issues, I got almost immediate responses.  With a 12 hour time difference, but I get responses within minutes, not hours, relieving my stress.
                         </q>
                         <div class="client-rating">
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class="icon icon-star"></i></a>
											<a href="#"><i class=".icon icon-star-empty"></i></a>
										</div> 
                         <cite>
                         
                             Marc L.<br>
                            Online Technologies </cite>
                    </blockquote> 
                    
                </div>
                
                
            </div>
               	       
                                    
               
              
          
            
            <!--<div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Our Partners <span> </span> </h2> </div>-->
            
            <!-- **Partner Carousel Wrapper** -->
            <!--<div class="partner-carousel-wrapper">
                <ul class="partner-carousel">
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="Divyaltech web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="seo and social media marketing" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="ecommerce website design" title=""> </a> </li>
                	<li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="wordpress web development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="cold fusion website development" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="website development in codeigniter" title=""> </a> </li>
                    <li> <a href="" title=""> <img src="http://placehold.it/200x40.jpg" alt="php and coldfusion web development" title=""> </a> </li>
                </ul>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div>--> <!-- **Partner Carousel Wrapper - End** -->
            
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
	<?php include ('include/footer.php')?>
    <!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

<!-- Layer Slider -->
<script src="js/jquery-easing-1.3.js"></script>
<script src="js/jquery-transit-modified.js"></script>
<script src="js/layerslider.transitions.js"></script>
<script src="js/layerslider.kreaturamedia.jquery.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery('#layerslider').layerSlider({
			skinsPath : 'layerslider-skins/',
			skin : 'fullwidthdark',
			thumbnailNavigation : 'hover',
			hoverPrevNext : false,
			responsive : false,
			responsiveUnder : 1120,
			sublayerContainer : 1060,
			width : '100%',
			height : '400px'
		});
	});		
</script>		



</body>
</html>

