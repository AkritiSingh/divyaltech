<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies</title>
	
	<meta name="description" content="Contact at Divyal Technology web development company to catch a professional insight into your business idea!">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <style>
						#gmap_canvas img {
							max-width: none!important;
							background: none!important
						}
					</style>
<!-- Copyright 2000, 2001, 2002, 2003 Macromedia, Inc. All rights reserved. -->
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1>Joomla</h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">       
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width"> 
			<div class="column two-third">  
             <div class="da-cantent">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong> Joomla Development</strong></span></h6>
                 <p>Joomla is a very extensible and easy to understand content management system. It is an exceptionally 
                   prominent innovation used to build sites.Joomla is developed utilizing PHP, Object Oriented 
                   Programming,software design patterns and MySQL (used for putting away the information). The primary elements 
                   of Joomla which makes it a popular content management system are:</p>
                   <ul style="margin-left:0px;">
                   <li>Open Source</li>
				   <li>Mobile ready</li>
				   <li>SEO friendly</li>
				   <li>Easy handling</li>
				   <li>Highly scalable</li>
				   <li>Reduced operational costs</li>
                   </ul>
                 
                     <p>Divyaltech is a detectable web development service provider,we give custom Joomla template development by                      executing its easy to use highlights, most recent form and extension of this award-winning content 
                      management system. We generally mean to assemble a site, which offers higher changes and this makes us the 
                      best Joomla service provider.
                   </p>
            </div>
            <br><br>
           <div class="type" style="margin-top:0px;">
              <br><h6><span style=" font-size: 27px;color: #464646;">Our Joomla developers provide services such as:</span> </h6>
                <h6><span style=" font-size: 18px;color: #464646;">Joomla Web Design & Development:</span></h6>
                  <p>The instinctive and easy administrator board makes Joomla a famous site development system for the 
                    individuals who need to upgrade content or interactive media content on their site every now and again.
                 </p> 
                <br><h6><span style=" font-size: 18px;color: #464646;">Joomla Extension Development:</span></h6>
                <p>
                Whether you to need to arrange the accessible Joomla expansions or need to build up another one from the scratch,
                our reality class developers can help you do it. We likewise encourage you to overhaul your Joomla site and get 
                 the advantages from the most recent discharge by keeping the functionalities in place.
                </p>
                   
                <br><h6><span style=" font-size: 18px;color: #464646;">Joomla Extension Development:</span></h6>
                <p>
                Whether you to need to arrange the accessible Joomla expansions or need to build up another one from the scratch,
                our reality class developers can help you do it. We likewise encourage you to overhaul your Joomla site and get 
                 the advantages from the most recent discharge by keeping the functionalities in place.
                </p>
                
               <br><h6><span style=" font-size: 18px;color: #464646;">Mobile Website Development:</span></h6>
                <p>
                 Your clients are searching for you on their mobile devices, would you say you are there? Divyaltech extends your                 site's compass to clients with a portable good form of your site. Our Joomla developers design the whole website                 page design and client interface around the mobile device.
                </p>
                
                
                <br><h6><span style=" font-size: 25px;color: #464646;">Joomla is utilized everywhere throughout the world to power Web destinations of all shapes and sizes: for example</span> </h6>
                  <ul>
             	    <li>Corporate Web sites or portals</li>
                     <li>Corporate intranets and extranets</li>
                     <li>Online magazines, newspapers, and publications</li>
                     <li>E-commerce and online reservations</li>
                     <li>Government applications</li>
                     <li>Small business Web sites</li>
                     <li>Non-profit and organizational Web sites</li>
                     <li>Community-based portals</li>
                     <li>Personal or family homepages</li>
                  </ul>
               </div>
           <br><br>
        
      </div> 
          <div class="rightside">
           <?php include 'technology.php';?>
           <img style="max-width:30%; margin-top:50px;margin-left:48px;" title="App Development" alt="App Development" src="images/joomla1.jpg">
             <div class="services" style="margin-top:65px;">
                <h6><span style="font-size: 27px;color: #464646;">
                 <strong>Advantages to choose joomla for web development:</strong></span></h6>
                  <ul>
                    <li>Easy to install</li>
                    <li>It provide usefull Plugins</li>
                    <li>Navigation Management</li>
                    <li>Good looking URLs</li>
                    <li>Advanced administration</li>
                    <li>Support & Updates</li>
                  </ul>
               </div>
               
            </div> 	
        </section><!-- **Primary Section** -->      
      </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
