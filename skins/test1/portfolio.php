<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title>Divyal Technologies | Portfolio</title>
	
	<meta name="description" content="Take a look at our portfolio which includes ecommerce website development we work with, which were produced for their global clients belonging to different industries">
	<meta name="author" content="">
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- **Favicon** -->
    <link rel="shortcut icon" type="image/png" href="images/favicon2.png"/>
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />    
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />    
    
    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />    
    <link href="responsive.css" rel="stylesheet" media="all" />    
    
    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    
    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
</head>

<body>

<!-- **Wrapper** -->
<div class="wrapper">

	<!-- **Header** -->
	<?php include ('include/header.php')?>
	<!-- **Header - End** -->
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Portfolio </h1>
                </div>
                <div class="main-phone-no">
                	<p> (+91) 788-2225066 <br> <a href="" title=""> info@divyaltech.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<div class="sorting-container">
            	<a href="#" title="" class="active-sort" data-filter=".all-sort"> All </a>
                <a href="#" title="" data-filter=".php-sort"> PHP </a>
                <a href="#" title="" data-filter=".coldfusion-sort"> Cold Fusion </a>                
                <a href="#" title="" data-filter=".wordpress-sort"> Wordpress </a>
                <a href="#" title="" data-filter=".mura-sort"> Mura </a>
                <a href="#" title="" data-filter=".html-sort"> HTML </a>
            </div>
            
        	<div class="portfolio-container gallery">
            	<div class="portfolio three-column all-sort html-sort coldfusion-sort mura-sort">  
            			<div class="portfolio-thumb">
                            <img src="images/portfolio/socialsense/socialsense1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                          							
								<a href="images/portfolio/socialsense/socialsense2.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense4.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"><span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/socialsense/socialsense5.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/socialsense/socialsense6.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                 <a href="images/portfolio/socialsense/responsive-social.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                 <a href="images/portfolio/socialsense/socialsense1.png" data-gal="prettyPhoto1[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="#" title=""> SocialSense</a> </h5>
                            <p> www.socialsense.com </p>
                        </div>   
                 </div>
                 
                 <div class="portfolio three-column all-sort html-sort php-sort">  
            			 <div class="portfolio-thumb">
                            <img src="images/portfolio/elite-dealers/elite1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/portfolio/elite-dealers/elite3.png" data-gal="prettyPhoto2[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/elite-dealers/elite2.png" data-gal="prettyPhoto2[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>								
								<a href="images/portfolio/elite-dealers/elite4.png" data-gal="prettyPhoto2[html-sort]" target="_blank" title="" class="zoom">  <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/elite-dealers/Responsive-elite.png" data-gal="prettyPhoto2[html-sort]" target="_blank" title="" class="zoom">  <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/elite-dealers/elite1.png" data-gal="prettyPhoto2[html-sort]" target="_blank" title="" class="zoom">  <span class="icon-fullscreen"> </span> </a>								
								<a href="http://www.elite-dealers.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="http://www.elite-dealers.com" title=""> Elite-Dealers</a> </h5>
                            <p> www.elite-dealers.com </p>
                        </div>  
                 </div>
                 
                 <div class="portfolio three-column all-sort html-sort wordpress-sort php-sort">  
            			<div class="portfolio-thumb">
                            <img src="images/portfolio/seaweedhealthnews/seasweet1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                
								<a href="images/portfolio/seaweedhealthnews/seasweet2.PNG" data-gal="prettyPhoto3[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/seasweet4.png" data-gal="prettyPhoto3[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/seaweedhealthnews/seasweet6.png" data-gal="prettyPhoto3[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/seaweedhealthnews/Responsive-seaweed.png" data-gal="prettyPhoto3[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/seaweedhealthnews/seasweet1.PNG" data-gal="prettyPhoto3[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>								 								<a href="http://www.seaweedhealthnews.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="http://www.seaweedhealthnews.com" title=""> SeaWeedHealthNews</a> </h5>
                            <p> www.seaweedhealthnews.com </p>
                        </div> 
                 </div>
                    
            	<div class="portfolio three-column all-sort html-sort coldfusion-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/portfolio/glad/gald1.png" alt="" title="">
                        <div class="image-overlay">
                            <a href="images/portfolio/glad/gald2.png" data-gal="prettyPhoto4[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/portfolio/glad/gald3.png" data-gal="prettyPhoto4[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/portfolio/glad/gald4.png" data-gal="prettyPhoto4[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/portfolio/glad/gald5.png" data-gal="prettyPhoto4[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
							<a href="images/portfolio/glad/gald6.png" data-gal="prettyPhoto4[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> 
							</a>
                            <a href="http://glad.net/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
					  </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.glad.net/" title="">Gladiator Distribution	</a> </h5>
                        <p> WWW.GLAD.NET</p>
                    </div>
                </div>
            	<div class="portfolio three-column all-sort html-sort coldfusion-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/portfolio/scooterlink/scooter1.png" alt="" title="">
						<div class="single">
                        <div class="image-overlay">
						  <a href="images/portfolio/scooterlink/scooter4.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/portfolio/scooterlink/scooter5.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/portfolio/scooterlink/scooter6.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
						  <a href="images/portfolio/scooterlink/scooter7.png" data-gal="prettyPhoto5[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>					 
                            <a href="http://www.scooterlink.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
						</div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.scooterlink.com/" title=""> Scooter Link </a> </h5>
                        <p> WWW.SCOOTERLINK.COM </p>
                    </div>
                </div>
                
            	<div class="portfolio three-column all-sort html-sort coldfusion-sort mura-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/portfolio/spritual/spritual1.png" alt="" title="">
                        <div class="image-overlay">
                        <a href="images/portfolio/spritual/spritual1.png" data-gal="prettyPhoto6[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
              			<a href="images/portfolio/spritual/spritual2.png" data-gal="prettyPhoto6[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
			<a href="images/portfolio/spritual/spritual4.png" data-gal="prettyPhoto6[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
			<a href="images/portfolio/spritual/spritual5.png" data-gal="prettyPhoto6[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>				
                            <a href="http://spiritualbridge.org/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://spiritualbridge.org/" title=""> Spiritual Bridge </a> </h5>
                        <p>SPIRITUALBRIDGE.ORG</p>
                    </div>
                </div>
                <div class="portfolio three-column all-sort php-sort html-sort">
					<div class="portfolio-thumb">
                            <img src="images/add1.JPG" alt="ecommerce website development" title="">
                            <div class="image-overlay">
                                <a href="images/add1.JPG" data-gal="prettyPhoto11[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/add2.JPG" data-gal="prettyPhoto11[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
								<a href="images/addictive.png" data-gal="prettyPhoto11[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tablet2.png" data-gal="prettyPhoto11[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/tablet.png" data-gal="prettyPhoto11[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/add5.png" data-gal="prettyPhoto11[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="http://www.addictivepolls.com" title=""> Addictive Polls</a> </h5>
                            <p> www.addictivepolls.com </p>
                        </div>
            	</div>
                <div class="portfolio three-column all-sort php-sort html-sort wordpress-sort">
					<div class="portfolio-thumb">
                            <img src="images/portfolio/econorentals/econo1.png" alt="wordpress development" title="">
                            <div class="image-overlay">
								<a href="images/portfolio/econorentals/Responsive-econorental.png" data-gal="prettyPhoto3[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>			
                                <a href="images/portfolio/econorentals/econo3.png" data-gal="prettyPhoto10[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
								<a href="images/portfolio/econorentals/econo4.png" data-gal="prettyPhoto10[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/econorentals/econo5.png" data-gal="prettyPhoto10[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								<a href="images/portfolio/econorentals/econo2.png" data-gal="prettyPhoto10[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
								
                                <a href="http://econorentals.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="http://www.econorentals.com" title=""> Econo Auto Rental </a> </h5>
                            <p>www.econorentals.com</p>
                        </div>
				</div>  
            	<div class="portfolio three-column all-sort wordpress-sort php-sort">
                	<div class="portfolio-thumb">
                    	<img src="images/harrow11.png" alt="" title="">
                        <div class="image-overlay">
                          <a href="images/portfolio/harrowlegal/harrowlegal.png" data-gal="prettyPhoto7[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>
						<a href="images/portfolio/harrowlegal/harrow2.png" data-gal="prettyPhoto7[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>
						<a href="images/portfolio/harrowlegal/harrow3.png" data-gal="prettyPhoto7[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>		 
						<a href="images/portfolio/harrowlegal/Responsive-harrowlegal.png" data-gal="prettyPhoto7[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>
						<a href="images/portfolio/harrowlegal/harrow1.png" data-gal="prettyPhoto7[blog-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span></a>
                            <a href="http://www.harrowlegal.com/" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="http://www.harrowlegal.com/" title=""> Harrow Law Group P.A. </a> </h5>
                        <p> WWW.HARROWLEGAL.COM </p>
                    </div>
                </div>
                
                
                <div class="portfolio three-column all-sort coldfusion-sort html-sort">
                	 <div class="portfolio-thumb">
                            <img src="images/portfolio/ticket/ticket1.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                     
                                <a href="images/portfolio/ticket/ticket3.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                                <a href="images/portfolio/ticket/ticket4.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                 <a href="images/portfolio/ticket/ticket5.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                                 <a href="images/portfolio/ticket/ticket6.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                 <a href="images/portfolio/ticket/responsive-ticket.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                 <a href="images/portfolio/ticket/ticket1.png" data-gal="prettyPhoto8[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>								
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> Ticket App Control Panel</a> </h5>
                            <p>&nbsp;</p>
                        </div>
                </div>
                
                
                <div class="portfolio three-column all-sort coldfusion-sort html-sort">
                	<div class="portfolio-thumb">
                            <img src="images/portfolio/pos/pos2.png" alt="ecommerce website development" title="">
                            <div class="image-overlay">                               
								<a href="images/portfolio/pos/pos4.png" data-gal="prettyPhoto9[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                                <a href="images/portfolio/pos/pos5.png" data-gal="prettyPhoto9[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                                <a href="images/portfolio/pos/pos3.png" data-gal="prettyPhoto9[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="images/portfolio/pos/pos6.png" data-gal="prettyPhoto9[html-sort]" target="_blank" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>	
                                 <a href="images/portfolio/pos/pos2.png" data-gal="prettyPhoto9[html-sort]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>							
								<a href="#" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.php" title=""> POS</a> </h5>   
                               <p>&nbsp;</p>                       
                        </div>
                </div>	  
            </div>
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>   
            
            <!--div class="pagination"> 
            	<ul>
                    <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                	<li class="active-page"> 1</li>                
                    <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                </ul>
            </div-->
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <?php include ('include/footer.php')?>
	<!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/isotope.js"></script>
<script src="js/jquery.smartresize.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>
<script src="js/clickme.js"></script>
</body>
</html>
